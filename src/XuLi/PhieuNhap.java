/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class PhieuNhap extends Phieu{
    private String malo;
    private ArrayList <CTPhieuNhap> chitiet;

    public String getMalo() {
        return malo;
    }

    public ArrayList<CTPhieuNhap> getChitiet() {
        return chitiet;
    }

    public void setMalo(String malo) {
        this.malo = malo;
    }

    public void setChitiet(ArrayList<CTPhieuNhap> chitiet) {
        this.chitiet = chitiet;
    }
    
}
