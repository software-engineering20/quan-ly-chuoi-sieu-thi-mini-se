/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class YCNoiBo extends YeuCau{
    private String manvxacnhan;
    private String manvhoanthanh;

    public String getManvxacnhan() {
        return manvxacnhan;
    }

    public String getManvhoanthanh() {
        return manvhoanthanh;
    }

    public void setManvxacnhan(String manvxacnhan) {
        this.manvxacnhan = manvxacnhan;
    }

    public void setManvhoanthanh(String manvhoanthanh) {
        this.manvhoanthanh = manvhoanthanh;
    }

    public String getManguonchuyenNB() {
        return super.getManguonchuyen(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getManguonycNB() {
        return super.getManguonyc(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getManhanvienYCNB() {
        return super.getManhanvien(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMayeucauNB() {
        return super.getMayeucau(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getSomnhatYCNB() {
        return super.getSomnhat(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getTrenhatYCNB() {
        return super.getTrenhat(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManguonchuyenNB(String manguonchuyen) {
        super.setManguonchuyen(manguonchuyen); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManguonycNB(String manguonyc) {
        super.setManguonyc(manguonyc); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManhanvienYCNB(String manhanvien) {
        super.setManhanvien(manhanvien); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMayeucauNB(String mayeucau) {
        super.setMayeucau(mayeucau); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSomnhatYCNB(Date somnhat) {
        super.setSomnhat(somnhat); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTrenhatYCNB(Date trenhat) {
        super.setTrenhat(trenhat); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
