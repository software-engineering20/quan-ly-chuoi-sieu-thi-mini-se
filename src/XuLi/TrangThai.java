/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class TrangThai {
    private String matrangthai;
    private String tentrangthai;

    public TrangThai() {
    }

    public TrangThai(String matrangthai, String tentrangthai) {
        this.matrangthai = matrangthai;
        this.tentrangthai = tentrangthai;
    }

    public String getMatrangthai() {
        return matrangthai;
    }

    public String getTentrangthai() {
        return tentrangthai;
    }

    public void setMatrangthai(String matrangthai) {
        this.matrangthai = matrangthai;
    }

    public void setTentrangthai(String tentrangthai) {
        this.tentrangthai = tentrangthai;
    }
    
}
