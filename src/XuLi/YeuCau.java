/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class YeuCau {
    private String mayeucau;
    private String manhanvien;
    private String manguonyc;
    private String manguonchuyen;
    private Date somnhat;
    private Date trenhat;

    public String getMayeucau() {
        return mayeucau;
    }

    public String getManhanvien() {
        return manhanvien;
    }

    public String getManguonyc() {
        return manguonyc;
    }

    public String getManguonchuyen() {
        return manguonchuyen;
    }

    public Date getSomnhat() {
        return somnhat;
    }

    public Date getTrenhat() {
        return trenhat;
    }

    public void setMayeucau(String mayeucau) {
        this.mayeucau = mayeucau;
    }

    public void setManhanvien(String manhanvien) {
        this.manhanvien = manhanvien;
    }

    public void setManguonyc(String manguonyc) {
        this.manguonyc = manguonyc;
    }

    public void setManguonchuyen(String manguonchuyen) {
        this.manguonchuyen = manguonchuyen;
    }

    public void setSomnhat(Date somnhat) {
        this.somnhat = somnhat;
    }

    public void setTrenhat(Date trenhat) {
        this.trenhat = trenhat;
    }
    
}
