/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author admin
 */
public class DoiTac extends TacDong{
    private Date nghoptac;
    private Date hanhd;
    private String email;
    private TrangThai trangthai;
    private ArrayList <String> sdt;

    public Date getNghoptac() {
        return nghoptac;
    }

    public Date getHanhd() {
        return hanhd;
    }

    public String getEmail() {
        return email;
    }

    public TrangThai getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(TrangThai trangthai) {
        this.trangthai = trangthai;
    }

    public ArrayList<String> getSdt() {
        return sdt;
    }

    public void setNghoptac(Date nghoptac) {
        this.nghoptac = nghoptac;
    }

    public void setHanhd(Date hanhd) {
        this.hanhd = hanhd;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSdt(ArrayList<String> sdt) {
        this.sdt = sdt;
    }

    public String getMaDT() {
        return super.getMa(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getTenDT() {
        return super.getTen(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getDiachiDT() {
        return super.getDiachi(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMaDT(String ma) {
        super.setMa(ma); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTenDT(String ten) {
        super.setTen(ten); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setDiachiDT(String diachi) {
        super.setDiachi(diachi); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

}
