/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.Date;

/**
 *
 * @author admin
 */
public class Phieu {
    private String maphieu;
    private String manv;
    private Date ngayphieu;

    public String getMaphieu() {
        return maphieu;
    }

    public String getManv() {
        return manv;
    }

    public Date getNgayphieu() {
        return ngayphieu;
    }

    public void setMaphieu(String maphieu) {
        this.maphieu = maphieu;
    }

    public void setManv(String manv) {
        this.manv = manv;
    }

    public void setNgayphieu(Date ngayphieu) {
        this.ngayphieu = ngayphieu;
    }
    
}
