/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.sql.*;
import java.time.LocalDateTime;

/**
 *
 * @author mac
 */
public class ChiTietThuChiCNSP {
    private String MaCN;
    private LocalDateTime Tu;
    private LocalDateTime Den;
    private long TongThu;
    private long TongChi;
    private long LoiNhuan;
    private float PhanTramLoiNhuan;

    public ChiTietThuChiCNSP(String MaCN, LocalDateTime Tu, LocalDateTime Den, long TongThu, long TongChi, long LoiNhuan, float PhanTramLoiNhuan) {
        this.MaCN = MaCN;
        this.Tu = Tu;
        this.Den = Den;
        this.TongThu = TongThu;
        this.TongChi = TongChi;
        this.LoiNhuan = LoiNhuan;
        this.PhanTramLoiNhuan = PhanTramLoiNhuan;
    }

    public String getMaCN() {
        return MaCN;
    }

    public LocalDateTime getTu() {
        return Tu;
    }

    public LocalDateTime getDen() {
        return Den;
    }

    public long getTongThu() {
        return TongThu;
    }

    public long getTongChi() {
        return TongChi;
    }

    public long getLoiNhuan() {
        return LoiNhuan;
    }

    public float getPhanTramLoiNhuan() {
        return PhanTramLoiNhuan;
    }

    
    
}
