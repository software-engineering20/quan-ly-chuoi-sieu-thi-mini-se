/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.Date;

/**
 *
 * @author admin
 */
public class KhuyenMai {
    private String makhuyenmai;
    private String tenkhuyenmai;
    private Date ngaybd;
    private Date ngaykt;

    public String getMakhuyenmai() {
        return makhuyenmai;
    }

    public String getTenkhuyenmai() {
        return tenkhuyenmai;
    }

    public Date getNgaybd() {
        return ngaybd;
    }

    public Date getNgaykt() {
        return ngaykt;
    }

    public void setMakhuyenmai(String makhuyenmai) {
        this.makhuyenmai = makhuyenmai;
    }

    public void setTenkhuyenmai(String tenkhuyenmai) {
        this.tenkhuyenmai = tenkhuyenmai;
    }

    public void setNgaybd(Date ngaybd) {
        this.ngaybd = ngaybd;
    }

    public void setNgaykt(Date ngaykt) {
        this.ngaykt = ngaykt;
    }
    
}
