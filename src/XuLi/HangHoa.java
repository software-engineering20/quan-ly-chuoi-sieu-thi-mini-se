/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class HangHoa {
    private String mahanghoa;
    private String tenhanghoa;
    private String nuocsx;
    private TrangThai trangthai;

    public String getMahanghoa() {
        return mahanghoa;
    }

    public String getTenhanghoa() {
        return tenhanghoa;
    }

    public String getNuocsx() {
        return nuocsx;
    }
    public TrangThai getTrangthai() {
        return trangthai;
    }


    public void setMahanghoa(String mahanghoa) {
        this.mahanghoa = mahanghoa;
    }

    public void setTenhanghoa(String tenhanghoa) {
        this.tenhanghoa = tenhanghoa;
    }

    public void setNuocsx(String nuocsx) {
        this.nuocsx = nuocsx;
    }

    public void setTrangthai(TrangThai matrangthai) {
        this.trangthai = matrangthai;
    }

    
    
    
}
