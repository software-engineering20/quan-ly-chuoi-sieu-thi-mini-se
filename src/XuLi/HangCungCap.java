/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class HangCungCap {
    private String madoitac;
    private String mahh;
    private TrangThai trangThai;

    public String getMadoitac() {
        return madoitac;
    }

    public String getMahh() {
        return mahh;
    }

    public TrangThai getTrangThai() {
        return trangThai;
    }

    public void setMadoitac(String madoitac) {
        this.madoitac = madoitac;
    }

    public void setMahh(String mahh) {
        this.mahh = mahh;
    }

    public void setTrangThai(TrangThai trangThai) {
        this.trangThai = trangThai;
    }
    
}
