/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class BiDong {
    private String ma;
    private String diachi;
    private ArrayList <String> sdt;

    public BiDong() {
    }

    public BiDong(String ma, String diachi, ArrayList<String> sdt) {
        this.ma = ma;
        this.diachi = diachi;
        this.sdt = sdt;
    }

    public String getMa() {
        return ma;
    }

    public String getDiachi() {
        return diachi;
    }

    public ArrayList<String> getSdt() {
        return sdt;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public void setSdt(ArrayList<String> sdt) {
        this.sdt = sdt;
    }
    
}
