/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class CT1DongHD {
    private String mahd;
    private String mahh;
    private String malohang;
    private String tenhh;
    private SanPham sanpham;
    private int soluong;
    private int gia;
    private int tongtien;
    private int giamgia;
    private int thanhtien;

    public CT1DongHD() {
    }

    public CT1DongHD(String mahd, String mahh, String malohang, String tenhh, SanPham sanpham, int soluong, int gia, int tongtien, int giamgia, int thanhtien) {
        this.mahd = mahd;
        this.mahh = mahh;
        this.malohang = malohang;
        this.tenhh = tenhh;
        this.sanpham = sanpham;
        this.soluong = soluong;
        this.gia = gia;
        this.tongtien = tongtien;
        this.giamgia = giamgia;
        this.thanhtien = thanhtien;
    }

    public String getMahd() {
        return mahd;
    }

    public String getMahh() {
        return mahh;
    }

    public String getMalohang() {
        return malohang;
    }

    public String getTenhh() {
        return tenhh;
    }

    public SanPham getSanpham() {
        return sanpham;
    }

    public int getSoluong() {
        return soluong;
    }

    public int getGia() {
        return gia;
    }

    public int getTongtien() {
        return tongtien;
    }

    public int getGiamgia() {
        return giamgia;
    }

    public int getThanhtien() {
        return thanhtien;
    }

    public void setMahd(String mahd) {
        this.mahd = mahd;
    }

    public void setMahh(String mahh) {
        this.mahh = mahh;
    }

    public void setMalohang(String malohang) {
        this.malohang = malohang;
    }

    public void setTenhh(String tenhh) {
        this.tenhh = tenhh;
    }

    public void setSanpham(SanPham sanpham) {
        this.sanpham = sanpham;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public void setGia(int gia) {
        this.gia = gia;
    }

    public void setTongtien(int tongtien) {
        this.tongtien = tongtien;
    }

    public void setGiamgia(int giamgia) {
        this.giamgia = giamgia;
    }

    public void setThanhtien(int thanhtien) {
        this.thanhtien = thanhtien;
    }
    
}
