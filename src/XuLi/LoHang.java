/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class LoHang {
    private String malohang;
    private String tenlohang;
    private String madoitac;
    private String makho;

    public String getMalohang() {
        return malohang;
    }

    public String getTenlohang() {
        return tenlohang;
    }

    public String getMadoitac() {
        return madoitac;
    }

    public String getMakho() {
        return makho;
    }

    public void setMalohang(String malohang) {
        this.malohang = malohang;
    }

    public void setTenlohang(String tenlohang) {
        this.tenlohang = tenlohang;
    }

    public void setMadoitac(String madoitac) {
        this.madoitac = madoitac;
    }

    public void setMakho(String makho) {
        this.makho = makho;
    }
    
}
