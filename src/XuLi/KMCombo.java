/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.Date;

/**
 *
 * @author admin
 */
public class KMCombo extends KMSanPham{
    private int giacombo;

    public int getGiacombo() {
        return giacombo;
    }

    public void setGiacombo(int giacombo) {
        this.giacombo = giacombo;
    }

    public String getMahhKMCom() {
        return super.getMahh(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMakhuyenmaiCom() {
        return super.getMakhuyenmai(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMalhKMCom() {
        return super.getMalh(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getNgaybdKMCom() {
        return super.getNgaybd(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getNgayktKMCom() {
        return super.getNgaykt(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getTenkhuyenmaiCom() {
        return super.getTenkhuyenmai(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMahhKMCom(String mahh) {
        super.setMahh(mahh); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMakhuyenmaiCom(String makhuyenmai) {
        super.setMakhuyenmai(makhuyenmai); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMalhKMCom(String malh) {
        super.setMalh(malh); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setNgaybdKMCom(Date ngaybd) {
        super.setNgaybd(ngaybd); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setNgayktKMCom(Date ngaykt) {
        super.setNgaykt(ngaykt); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTenkhuyenmaiCom(String tenkhuyenmai) {
        super.setTenkhuyenmai(tenkhuyenmai); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
