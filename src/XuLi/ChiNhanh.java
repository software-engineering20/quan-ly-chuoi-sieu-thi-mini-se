/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class ChiNhanh extends BiDong{
    private String tenchinhanh;
    private Kho khotructhuoc;
    private TrangThai trangThai;

    public ChiNhanh() {
    }

    public String getTenchinhanh() {
        return tenchinhanh;
    }

    public Kho getKhotructhuoc() {
        return khotructhuoc;
    }

    public TrangThai getTrangThai() {
        return trangThai;
    }

    public void setTenchinhanh(String tenchinhanh) {
        this.tenchinhanh = tenchinhanh;
    }

    public void setKhotructhuoc(Kho khotructhuoc) {
        this.khotructhuoc = khotructhuoc;
    }

    public void setTrangThai(TrangThai trangThai) {
        this.trangThai = trangThai;
    }
    public String getDiachiCN() {
        return super.getDiachi(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMaCN() {
        return super.getMa(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public ArrayList<String> getSdtCN() {
        return super.getSdt(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setDiachiCN(String diachi) {
        super.setDiachi(diachi); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMaCN(String ma) {
        super.setMa(ma); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSdtCN(ArrayList<String> sdt) {
        super.setSdt(sdt); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
}
