/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.time.LocalDateTime;

/**
 *
 * @author mac
 */
public class ChiTietNVKH {
    private String MaNV;
    private LocalDateTime Tu;
    private LocalDateTime Den;
    private long TongThu;

    public ChiTietNVKH(String MaNV, LocalDateTime Tu, LocalDateTime Den, long TongThu) {
        this.MaNV = MaNV;
        this.Tu = Tu;
        this.Den = Den;
        this.TongThu = TongThu;
    }

    public ChiTietNVKH() {
        
    }

    
    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaCN) {
        this.MaNV = MaNV;
    }

    public LocalDateTime getTu() {
        return Tu;
    }

    public void setTu(LocalDateTime Tu) {
        this.Tu = Tu;
    }

    public LocalDateTime getDen() {
        return Den;
    }

    public void setDen(LocalDateTime Den) {
        this.Den = Den;
    }

    public long getTongThu() {
        return TongThu;
    }

    public void setTongThu(long TongThu) {
        this.TongThu = TongThu;
    }
    
}
