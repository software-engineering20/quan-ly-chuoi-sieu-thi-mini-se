/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author admin
 */
public class HoaDon {
    private String mahoadon;
    private String manhanvien;
    private String makhachhang;
    private Date ngayHD;
    private long gia;
    private String tenKH;
    private String tenNV;

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }
    

    public String getTenKH() {
        return tenKH;
    }

    public String getTenNV() {
        return tenNV;
    }

    public String getMahoadon() {
        return mahoadon;
    }

    public void setNgayHD(Date ngayHD) {
        this.ngayHD = ngayHD;
    }

    public Date getNgayHD() {
        return ngayHD;
    }

    public String getManhanvien() {
        return manhanvien;
    }

    public String getMakhachhang() {
        return makhachhang;
    }

    public long getGia() {
        return gia;
    }

    public void setMahoadon(String mahoadon) {
        this.mahoadon = mahoadon;
    }

    public void setManhanvien(String manhanvien) {
        this.manhanvien = manhanvien;
    }

    public void setMakhachhang(String makhachhang) {
        this.makhachhang = makhachhang;
    }

    public void setGia(long gia) {
        this.gia = gia;
    }

}
