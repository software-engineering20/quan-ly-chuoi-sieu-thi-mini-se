/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class CTPhieuNhap extends CTPhieuLQHH{
    private int gianhap;
    private int giadukien;

    public int getGianhap() {
        return gianhap;
    }

    public int getGiadukien() {
        return giadukien;
    }

    public void setGianhap(int gianhap) {
        this.gianhap = gianhap;
    }

    public void setGiadukien(int giadukien) {
        this.giadukien = giadukien;
    }
    
}
