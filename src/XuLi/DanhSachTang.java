/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author Admin
 */
public class DanhSachTang {
    private String maKM;
    private String mahhtang;
    private String malotang;
    private int soluongtang;

    public String getMaKM() {
        return maKM;
    }

    public String getMahhtang() {
        return mahhtang;
    }

    public String getMalotang() {
        return malotang;
    }

    public int getSoluongtang() {
        return soluongtang;
    }

    public void setMaKM(String maKM) {
        this.maKM = maKM;
    }

    public void setMahhtang(String mahhtang) {
        this.mahhtang = mahhtang;
    }

    public void setMalotang(String malotang) {
        this.malotang = malotang;
    }

    public void setSoluongtang(int soluongtang) {
        this.soluongtang = soluongtang;
    }
    
}
