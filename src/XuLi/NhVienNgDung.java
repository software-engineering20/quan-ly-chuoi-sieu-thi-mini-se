/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author admin
 */
public class NhVienNgDung extends ConNguoi{
    private String username;
    private String password;
    private ArrayList <ChucVu> chucvu;
    private String machinhanh;
    private TrangThai trangthai;

    public NhVienNgDung() {
    }

    public NhVienNgDung(String MaNV, String TenNV,  String username, String password, ArrayList<ChucVu> chucvu, String machinhanh, TrangThai trangthai) {
        this.username = username;
        this.password = password;
        this.chucvu = chucvu;
        this.machinhanh = machinhanh;
        this.trangthai = trangthai;
    }

    public TrangThai getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(TrangThai trangthai) {
        this.trangthai = trangthai;
    }
    

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<ChucVu> getChucvu() {
        return chucvu;
    }

    public String getMachinhanh() {
        return machinhanh;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setChucvu(ArrayList<ChucVu> chucvu) {
        this.chucvu = chucvu;
    }

    public void setMachinhanh(String machinhanh) {
        this.machinhanh = machinhanh;
    }

    public String getMaNV() {
        return super.getMa(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getDiachiNV() {
        return super.getDiachi(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getNgsinhNV() {
        return super.getNgsinh(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public ArrayList<String> getSdtNV() {
        return super.getSdt(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getTenNV() {
        return super.getTen(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    public void setDiachiNV(String diachi) {
        super.setDiachi(diachi); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMaNV(String ma) {
        super.setMa(ma); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setNgsinhNV(Date ngsinh) {
        super.setNgsinh(ngsinh); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSdtNV(ArrayList<String> sdt) {
        super.setSdt(sdt); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTenNV(String ten) {
        super.setTen(ten); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
