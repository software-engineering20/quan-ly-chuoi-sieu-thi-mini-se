/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author admin
 */
public class ConNguoi extends TacDong{
    private Date ngsinh;
    private ArrayList <String> sdt;

    public Date getNgsinh() {
        return ngsinh;
    }

    public ArrayList<String> getSdt() {
        return sdt;
    }

    public void setNgsinh(Date ngsinh) {
        this.ngsinh = ngsinh;
    }

    public void setSdt(ArrayList<String> sdt) {
        this.sdt = sdt;
    }
    
}
