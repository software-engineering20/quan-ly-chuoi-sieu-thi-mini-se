/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class CTYeuCau {
    private String mayeucau;
    private float soluong;

    public String getMayeucau() {
        return mayeucau;
    }

    public float getSoluong() {
        return soluong;
    }

    public void setMayeucau(String mayeucau) {
        this.mayeucau = mayeucau;
    }

    public void setSoluong(float soluong) {
        this.soluong = soluong;
    }
    
}
