/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author admin
 */
public class YCDatHang extends YeuCau{
    private ArrayList <CTDatHang> chitiet;

    public ArrayList<CTDatHang> getChitiet() {
        return chitiet;
    }

    public void setChitiet(ArrayList<CTDatHang> chitiet) {
        this.chitiet = chitiet;
    }

    public String getManguonchuyenYCDH() {
        return super.getManguonchuyen(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getManguonycDH() {
        return super.getManguonyc(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getManhanvienYCDH() {
        return super.getManhanvien(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMayeucauDH() {
        return super.getMayeucau(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getSomnhatYCDH() {
        return super.getSomnhat(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getTrenhatYCDH() {
        return super.getTrenhat(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManguonchuyenYCDH(String manguonchuyen) {
        super.setManguonchuyen(manguonchuyen); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManguonycDH(String manguonyc) {
        super.setManguonyc(manguonyc); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setManhanvienYCDH(String manhanvien) {
        super.setManhanvien(manhanvien); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMayeucauDH(String mayeucau) {
        super.setMayeucau(mayeucau); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSomnhatYCDH(Date somnhat) {
        super.setSomnhat(somnhat); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTrenhatYCDH(Date trenhat) {
        super.setTrenhat(trenhat); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
