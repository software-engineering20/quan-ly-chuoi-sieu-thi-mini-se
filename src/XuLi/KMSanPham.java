/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class KMSanPham extends KhuyenMai{
    private String mahh;
    private String malh;

    public String getMahh() {
        return mahh;
    }

    public String getMalh() {
        return malh;
    }

    public void setMahh(String mahh) {
        this.mahh = mahh;
    }

    public void setMalh(String malh) {
        this.malh = malh;
    }
    
}
