/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author admin
 */
public class KhachHang extends ConNguoi{
    private int diem;
    private String matichdiem;
    
    //getter
    public String getMaKH() {
        return super.getMa(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getDiachiKH() {
        return super.getDiachi(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public Date getNgsinhKH() {
        return super.getNgsinh(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public ArrayList<String> getSdtKH() {
        return super.getSdt(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getTenKH() {
        return super.getTen(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public int getDiem() {
        return diem;
    }

    //setter
    public void setDiachiKH(String diachi) {
        super.setDiachi(diachi); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMaKH(String ma) {
        super.setMa(ma); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setNgsinhKH(Date ngsinh) {
        super.setNgsinh(ngsinh); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSdtKH(ArrayList<String> sdt) {
        super.setSdt(sdt); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setTenKH(String ten) {
        super.setTen(ten); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMatichdiem() {
        return matichdiem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }

    public void setMatichdiem(String matichdiem) {
        this.matichdiem = matichdiem;
    }
    
}
