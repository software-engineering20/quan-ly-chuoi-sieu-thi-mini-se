/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class GiamGia extends KMSanPham{
    private float phantramgg;
    private float giagiam;

    public float getPhantramgg() {
        return phantramgg;
    }

    public float getGiagiam() {
        return giagiam;
    }

    public void setPhantramgg(float phantramgg) {
        this.phantramgg = phantramgg;
    }

    public void setGiagiam(float giagiam) {
        this.giagiam = giagiam;
    }
    
}
