/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class MucTichDiem {
    private String mamuctd;
    private String tenmctd;
    private int diemtoithieu;
    private float chietkhau;

    public String getMamuctd() {
        return mamuctd;
    }

    public String getTenmctd() {
        return tenmctd;
    }

    public int getDiemtoithieu() {
        return diemtoithieu;
    }

    public float getChietkhau() {
        return chietkhau;
    }

    public void setMamuctd(String mamuctd) {
        this.mamuctd = mamuctd;
    }

    public void setTenmctd(String tenmctd) {
        this.tenmctd = tenmctd;
    }

    public void setDiemtoithieu(int diemtoithieu) {
        this.diemtoithieu = diemtoithieu;
    }

    public void setChietkhau(float chietkhau) {
        this.chietkhau = chietkhau;
    }
    
}
