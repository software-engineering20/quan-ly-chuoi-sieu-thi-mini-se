/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

/**
 *
 * @author admin
 */
public class CTPhieu {
    private String maphieu;
    private Float soluong;

    public String getMaphieu() {
        return maphieu;
    }

    public Float getSoluong() {
        return soluong;
    }

    public void setMaphieu(String maphieu) {
        this.maphieu = maphieu;
    }

    public void setSoluong(Float soluong) {
        this.soluong = soluong;
    }
    
}
