/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Kho extends BiDong{
    private ChiNhanh chinhanhql;
    private TrangThai trangthai;
    private ArrayList <SanPham> dssanpham;
    private ArrayList <LoHang> dslohang;

    public Kho() {
    }
    
    public ArrayList <SanPham> TimKiemSanPham(){
        return new ArrayList<>();
    }
    public ArrayList <LoHang> TimKiemLoHang(){
        return new ArrayList<>();
    }

    public ChiNhanh getChinhanhql() {
        return chinhanhql;
    }

    public TrangThai getTrangthai() {
        return trangthai;
    }

    

    public ArrayList<SanPham> getDssanpham() {
        return dssanpham;
    }

    public ArrayList<LoHang> getDslohang() {
        return dslohang;
    }

    public void setChinhanhql(ChiNhanh chinhanhql) {
        this.chinhanhql = chinhanhql;
    }

    public void setTrangthai(TrangThai trangthai) {
        this.trangthai = trangthai;
    }

    

    public void setDssanpham(ArrayList<SanPham> dssanpham) {
        this.dssanpham = dssanpham;
    }

    public void setDslohang(ArrayList<LoHang> dslohang) {
        this.dslohang = dslohang;
    }

    public String getDiachiKho() {
        return super.getDiachi(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public String getMaKho() {
        return super.getMa(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public ArrayList<String> getSdtKho() {
        return super.getSdt(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setDiachiKho(String diachi) {
        super.setDiachi(diachi); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setMaKho(String ma) {
        super.setMa(ma); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    public void setSdtKho(ArrayList<String> sdt) {
        super.setSdt(sdt); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
