/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package XuLi;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class SanPham {
    private String mahh;
    private String malohang;
    private int gianhap;
    private int giaban;
    private Date ngaynhap;
    private Date nsx;
    private Date hsd;
    private float slnhap;
    private float slkho;
    private float sldangban;
    private float sldaban;

    public String getMahh() {
        return mahh;
    }

    public String getMalohang() {
        return malohang;
    }

    public int getGianhap() {
        return gianhap;
    }

    public int getGiaban() {
        return giaban;
    }

    public Date getNgaynhap() {
        return ngaynhap;
    }

    public Date getNsx() {
        return nsx;
    }

    public Date getHsd() {
        return hsd;
    }

    public float getSlnhap() {
        return slnhap;
    }

    public float getSlkho() {
        return slkho;
    }

    public float getSldangban() {
        return sldangban;
    }

    public float getSldaban() {
        return sldaban;
    }

    public void setMahh(String mahh) {
        this.mahh = mahh;
    }

    public void setMalohang(String malohang) {
        this.malohang = malohang;
    }

    public void setGianhap(int gianhap) {
        this.gianhap = gianhap;
    }

    public void setGiaban(int giaban) {
        this.giaban = giaban;
    }

    public void setNgaynhap(Date ngaynhap) {
        this.ngaynhap = ngaynhap;
    }

    public void setNsx(Date nsx) {
        this.nsx = nsx;
    }

    public void setHsd(Date hsd) {
        this.hsd = hsd;
    }

    public void setSlnhap(float slnhap) {
        this.slnhap = slnhap;
    }

    public void setSlkho(float slkho) {
        this.slkho = slkho;
    }

    public void setSldangban(float sldangban) {
        this.sldangban = sldangban;
    }

    public void setSldaban(float sldaban) {
        this.sldaban = sldaban;
    }
    
}
