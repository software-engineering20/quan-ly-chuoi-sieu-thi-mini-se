    /*
     * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
     * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
     */
    package MiniSupermarket.DAL;

    import XuLi.HangCungCap;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.sql.Statement;
    /**
     *
     * @author ADMIN
     */
    public class HHCungCapDAL {
            public static boolean ThemCungCap(HangCungCap Cungc) throws SQLException{
            String sql=" Insert into DANHSACHCUNGCAP values (?,?,?)";
            PreparedStatement stCungCap = DALDuLieu.con.prepareStatement(sql);
            stCungCap.setString(1, Cungc.getMahh());
            stCungCap.setString(2, Cungc.getMadoitac());
            DALDuLieu.SoSanhDuLieuTT(Cungc.getTrangThai());
            stCungCap.setString(3, Cungc.getTrangThai().getMatrangthai());
            if(stCungCap.executeUpdate()>=1)
                return true;
            return false;
        }
            public static boolean SuaHCungCap(HangCungCap Cungc){
            boolean result = false;
            try {
                String sql = "UPDATE DANHSACHCUNGCAP "
                        + "SET MATT = ? "
                        + "WHERE MADOITAC = ? "
                        + "AND MAHH= ?";
                PreparedStatement stCungc = DALDuLieu.con.prepareStatement(sql);
                DALDuLieu.SoSanhDuLieuTT(Cungc.getTrangThai());
                stCungc.setString(1, Cungc.getTrangThai().getMatrangthai()); 
                stCungc.setNString(2, Cungc.getMadoitac());
                stCungc.setNString(3,Cungc.getMahh()); 
                if (stCungc.executeUpdate() >= 1){
                    result = true;
                }
                stCungc.close();
            } catch (Exception e ) {
                System.out.println(e);
            }
            return result;
     }    
        public static boolean DaCoCungCap(HangCungCap Cungc)throws SQLException{
          boolean flag = false;
          String sql ="SELECT * FROM DANHSACHCUNGCAP WHERE MADOITAC='"+Cungc.getMadoitac()+"' AND MAHH='"+Cungc.getMahh()+"'";
          Statement stCungCap =DALDuLieu.con.createStatement();
          ResultSet ds = stCungCap.executeQuery(sql);
          flag = ds.next();
          return flag;      
        }


    }