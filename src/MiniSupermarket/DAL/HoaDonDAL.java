/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import static MiniSupermarket.DAL.DALDuLieu.con;
import XuLi.HoaDon;
import XuLi.KhachHang;
import XuLi.NhVienNgDung;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

/**
 *
 * @author Admin
 */
public class HoaDonDAL {

    public static void themHoaDon(XuLi.HoaDon hd) {
        String url = "insert into HOADON values(?,?,?,?,?)";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, hd.getMahoadon());
            ps.setDate(2, (java.sql.Date) hd.getNgayHD());
            ps.setString(3, hd.getManhanvien());
            ps.setString(4, hd.getMakhachhang());
            ps.setLong(5, hd.getGia());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.print(e);;
        }
    }

    public static boolean xoaHD(String maHD) {
        String url = "delete from HOADON where MAHD='" + maHD + "'";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            return ps.executeUpdate() >= 0;
        } catch (SQLException e) {
            System.out.print(e);
        }
        return false;
    }

    public static ArrayList<XuLi.HoaDon> getDanhSachHD() {
        String url = "select * from HOADON";
        ArrayList<HoaDon> hdList = new ArrayList<>();
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                XuLi.HoaDon hd = new XuLi.HoaDon();
                hd.setMahoadon(rs.getString("MAHD"));
                hd.setManhanvien(rs.getString("MANV"));
                hd.setMakhachhang(rs.getString("MAKH"));
                hd.setGia(rs.getInt("TONGTIEN"));
                hd.setNgayHD(rs.getDate("NGAYHD"));
                hdList.add(hd);
            }
        } catch (SQLException e) {
            System.out.print(e);;
        }
        return hdList;
    }

    public static String timKiemTenKH(String maKH) {

        XuLi.KhachHang kh = new KhachHang();
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from KHACHHANG where MAKH = ?");
            prep.setString(1, maKH);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {

                kh.setTenKH(rs.getString(2));

                return kh.getTenKH();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String timKiemTenNV(String maNV) {

        XuLi.NhVienNgDung nv = new NhVienNgDung();
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from NHANVIEN where MANV = ?");
            prep.setString(1, maNV);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {

                nv.setTenNV(rs.getString(2));

                return nv.getTenNV();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public static XuLi.HoaDon timKiem(String maHD) {

        XuLi.HoaDon hd = new HoaDon();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("Select * from HOADON where MAHD like N'%" + maHD + "%'");
            if (rs.next()) {
                hd.setMahoadon(rs.getString(1));
                hd.setNgayHD(rs.getDate(2));
                hd.setManhanvien(rs.getString(3));
                hd.setMakhachhang(rs.getString(4));
                hd.setGia(rs.getInt(5));
                return hd;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
