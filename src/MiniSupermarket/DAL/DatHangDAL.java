/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.CTDatHang;
import XuLi.CTYCHangHoa;
import XuLi.YCDatHang;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class DatHangDAL {
      public static boolean ThemDH(YCDatHang dh){
        try{
            String sql="INSERT INTO DATHANG VALUES(?,?,?,?,?,?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, dh.getMayeucauDH());
            pr.setString(2, dh.getManhanvienYCDH());
            pr.setString(3, dh.getManguonycDH());
            pr.setString(4, dh.getManguonchuyenYCDH());
            pr.setDate(5,dh.getSomnhatYCDH());
            pr.setDate(6,dh.getTrenhatYCDH());
            if(pr.executeUpdate()>=1)
            {
                pr.close();
                return true;
            }
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.DatHangDAL.ThemYCHH()"+ex);
        }    
        return false;
    }
        public static boolean ThemCTDH(CTDatHang ctdh) throws SQLException{
        try{    
            String sql="INSERT INTO CTDATHANG VALUES(?,?,?,?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, ctdh.getMayeucau());
            pr.setString(2, ctdh.getMahh());
            pr.setFloat(3, ctdh.getSoluong());
            pr.setInt(4,ctdh.getGiadenghi());
            if(pr.executeUpdate()>=1)
            {
                pr.close();
                return true;
            }
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.DatHangDAL.ThemCTYCHH()" + ex);
        }    
        return false;
    }
    public static ArrayList<YCDatHang> LayDuLieuDH() throws SQLException {
        ArrayList <YCDatHang> dsdh=new ArrayList<YCDatHang>();
        String sql="SELECT * FROM DATHANG ";
        Statement stdh = DALDuLieu.con.createStatement();
        ResultSet rsdh = stdh.executeQuery(sql);
        while(rsdh.next()){
            YCDatHang dh=new YCDatHang();
            dh.setMayeucauDH(rsdh.getString("MADH"));
            dh.setManhanvienYCDH(rsdh.getString("MANV"));
            dh.setManguonchuyenYCDH(rsdh.getString("MADT"));
            dh.setSomnhatYCDH(rsdh.getDate("SOMNHAT"));
            dh.setTrenhatYCDH(rsdh.getDate("TRENHAT"));
            dsdh.add(dh);
        }
        stdh.close();
        rsdh.close();
        return dsdh;
    }
}
