/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

/**
 *
 * @author mac
 */
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import XuLi.ChiTietThuChiCNSP;
import XuLi.*;
import XuLi.QuanLy.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.*;

import java.sql.Connection;
import java.time.*;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author mac
 */
public class DALDuLieu {
    public static Connection con = null;
    public static boolean connectDB() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (Exception e) {
            System.out.println(e);
        }
        try {
            // DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
            String dbURL = "jdbc:sqlserver://localhost;databaseName=HETHONGSIEUTHI2;user=sa;password=handsome3S@";
            con = DriverManager.getConnection(dbURL);
            if (con != null) {
                System.out.println("connect successful!");
                return true;
            }
            else return false;
        } catch (SQLException ex) {
            System.err.println("Cannot connect database, " + ex);
            return false;
        } 
    }
    public static ArrayList<ChiNhanh> LayThongTinChiNhanh()  {
        String query = "SELECT * FROM (SELECT DISTINCT CHINHANH.MACN MACN, TENCN, CHINHANH.MATT MATT, MAKHO, CHINHANH.DIACHI DCCN, KHO.DIACHI DCKHO FROM CHINHANH LEFT JOIN KHO ON CHINHANH.MACN = KHO.MACN AND KHO.MATT = 'TT01') A, TRANGTHAI WHERE A.MATT = TRANGTHAI.MATT AND A.MATT = 'TT01'";
        ArrayList<ChiNhanh> DSChiNhanh = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {            
                ChiNhanh cn = new ChiNhanh();
                TrangThai tt = new TrangThai();
                Kho kho = new Kho();

                tt.setMatrangthai(rs.getString("MATT"));
                tt.setTentrangthai(rs.getString("TENTT"));
                kho.setMaKho(rs.getString("MAKHO"));
                kho.setDiachiKho(rs.getString("DCKHO"));

                cn.setMaCN(rs.getString("MACN"));
                cn.setTenchinhanh(rs.getString("TENCN"));
                cn.setDiachiCN(rs.getString("DCCN"));
                cn.setTrangThai(tt);
                kho.setChinhanhql(cn);
                cn.setKhotructhuoc(kho);
                kho.setChinhanhql(cn);
                LaySoDT(cn);
                DSChiNhanh.add(cn);
            }
            rs.close();
            st.close();
            QLChiNhanh.DSChiNhanh = DSChiNhanh;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.LayThongTinChiNhanh(): "+e);
        }
        
        return DSChiNhanh;
    }
    public static ArrayList<NhVienNgDung> LayThongTinNhanVien() {
        String queryNhanVien = "SELECT * FROM NHANVIEN WHERE MACN = '"+DangNhap.MACN+"' AND MATT = 'TT01'";
        ArrayList<NhVienNgDung> NhanVienList = new ArrayList<>();
        NhVienNgDung NhanVien = new NhVienNgDung();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(queryNhanVien);
            
            while (rs.next()) {
                NhanVien = new NhVienNgDung();
                NhanVien.setMaNV(rs.getString("MANV"));
                NhanVien.setTenNV(rs.getString("TENNV"));
                NhanVien.setNgsinhNV(rs.getDate("NGAYSINH"));
                NhanVien.setMachinhanh(rs.getString("MACN"));
                TrangThai tt = new TrangThai();
                tt.setMatrangthai(rs.getString("MATT"));
                NhanVien.setTrangthai(tt);
                NhanVien.setUsername(rs.getString("UN"));
                NhanVien.setPassword(rs.getString("PW"));
                LaySoDT(NhanVien);
                LayChucVu(NhanVien);
                NhanVienList.add(NhanVien);
            }
            rs.close();
        } catch (Exception e) {
            NhanVienList.add(NhanVien);
            System.out.println("Lay tt Nhan Vien" + e);
        }
        QLNhanVien.DSNhanVien = NhanVienList;
        return NhanVienList;
    }
    public static void LayThongTinNguoiDangNhap(String MANV) {
        String queryNhanVien = "SELECT * FROM NHANVIEN WHERE MANV = '"+MANV+"' AND MATT = 'TT01'";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(queryNhanVien);
            if (rs.next()) {  
                DangNhap.MA = rs.getString("MANV");
                DangNhap.TEN = rs.getString("TENNV");
                DangNhap.MACN = rs.getString("MACN");
                DangNhap.SDT = LaySoDT(DangNhap.MA);
                DangNhap.NGAYSINH = rs.getDate("NGAYSINH");
                DangNhap.ChucVu = LayChucVu(DangNhap.MA);
                DangNhap.MAKHO = LayMaKho(DangNhap.MACN);
                DangNhap.KHO = DALDuLieu.LayKho(DangNhap.MACN);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("Lay tt Nhan Vien" + e);
        }
    }
    public static Kho LayKho(String MACN) {
        String query = "SELECT * FROM KHO WHERE MACN = '"+MACN+"'";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            Kho kho = new Kho();
            if (rs.next()) {
                kho.setMaKho(rs.getString("MAKHO"));
                kho.setDiachiKho(rs.getNString("DIACHI"));
                ChiNhanh cn = new ChiNhanh();
                cn.setMaCN(rs.getString("MACN"));
                kho.setChinhanhql(cn);
                kho.setTrangthai(new KhoDAL().LayTTTT(new TrangThai(
                    rs.getString("MATT"), 
                    rs.getString("MATT")))
                );
            }
            return kho;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.LayKho(): "+e);
            return null;
        }
    }
    public static ArrayList<DoiTac> LayDuLieuDoiTac() throws SQLException {
        ArrayList<DoiTac> vDoiTac = new ArrayList<DoiTac>();
        String sql = "SELECT * FROM DOITAC, TRANGTHAI WHERE DOITAC.MATT= TRANGTHAI.MATT";
        Statement stDoiTac = DALDuLieu.con.createStatement();
        ResultSet ds = stDoiTac.executeQuery(sql);
        while (ds.next()) {
            DoiTac dt = new DoiTac();
            dt.setMaDT(ds.getString("MADOITAC"));
            dt.setTenDT(ds.getNString("TENCTYDOITAC"));
            dt.setDiachiDT(ds.getNString("DIACHI"));
            dt.setNghoptac(ds.getDate("NGAYHOPTAC"));
            dt.setHanhd(ds.getDate("HANHOPDONG"));
            dt.setEmail(ds.getString("EMAILDATHANG"));
            TrangThai tt = new TrangThai();
            tt.setMatrangthai(ds.getString("MATT"));
            tt.setTentrangthai(ds.getString("TENTT"));
            dt.setTrangthai(tt);
            vDoiTac.add(dt);
        
        }
        for (int i = 0;i < vDoiTac.size();i++){
            ResultSet dsSDT = stDoiTac.executeQuery("SELECT * "
                        + "FROM DOITAC_SDT "
                        + "WHERE MADOITAC = '" + vDoiTac.get(i).getMaDT()+ "'");
            ArrayList <String> sdt = new ArrayList<>();
            while (dsSDT.next()) {                        
                sdt.add(dsSDT.getString("SODT"));
            }
            vDoiTac.get(i).setSdt(sdt);
            dsSDT.close();
        }
        stDoiTac.close();
        ds.close();
        return vDoiTac;
    }
    public static TrangThai SoSanhDuLieuTT (TrangThai TT) throws SQLException {
        if (TT.getTentrangthai().contains("Đang bán") || TT.getTentrangthai().contains("Có"))
            TT.setTentrangthai("CO");
        if (TT.getTentrangthai().contains("Ngừng bán") || TT.getTentrangthai().contains("Không"))
            TT.setTentrangthai("KHONG");
        
        String sql = "SELECT MATT FROM TRANGTHAI WHERE TRANGTHAI.TENTT= '"+TT.getTentrangthai()+"'";
        Statement stCungc = DALDuLieu.con.createStatement();
        ResultSet ds = stCungc.executeQuery(sql);
        while (ds.next()) {
            TT.setMatrangthai(ds.getString("MATT"));
        }
        return TT;
    }
     
     public static ArrayList<HangCungCap> LayDuLieuCungCap() throws SQLException {
        ArrayList<HangCungCap> vCungCap = new ArrayList<HangCungCap>();
        String sql = "SELECT * FROM DANHSACHCUNGCAP,TRANGTHAI WHERE DANHSACHCUNGCAP.MATT= TRANGTHAI.MATT";
        Statement stCungc = DALDuLieu.con.createStatement();
        ResultSet ds = stCungc.executeQuery(sql);
        while (ds.next()) {
            HangCungCap cungc = new HangCungCap();
            cungc.setMadoitac(ds.getString("MADOITAC"));
            cungc.setMahh(ds.getString("MAHH"));
            TrangThai tt = new TrangThai();
            tt.setMatrangthai(ds.getString("MATT"));
            tt.setTentrangthai(ds.getString("TENTT"));
            cungc.setTrangThai(tt);
            vCungCap.add(cungc);
        }
        stCungc.close();
        ds.close();
        return vCungCap;
    }

public static ArrayList<HangHoa> LayDuLieuHangHoa() throws SQLException {
        ArrayList<HangHoa> vHH = new ArrayList<HangHoa>();
        String sql = "SELECT * FROM HANGHOA, TRANGTHAI WHERE HANGHOA.MATT= TRANGTHAI.MATT";
        Statement stHH = DALDuLieu.con.createStatement();
        ResultSet ds = stHH.executeQuery(sql);
        while (ds.next()) {
            HangHoa HH = new HangHoa();
            HH.setMahanghoa(ds.getString("MAHH"));
            HH.setTenhanghoa(ds.getNString("TENHANG"));
            HH.setNuocsx(ds.getNString("NUOCSX"));
            TrangThai tt = new TrangThai();
            tt.setMatrangthai(ds.getString("MATT"));
            tt.setTentrangthai(ds.getString("TENTT"));
            HH.setTrangthai(tt);
            vHH.add(HH);
        }
        stHH.close();
        ds.close();
        QLHangHoa.DSHangHoa = vHH;
        return vHH;
    }
public static ArrayList<SanPham> LayDuLieuSanpham() throws SQLException {
        ArrayList<SanPham> vSP = new ArrayList<SanPham>();
        String sql = "SELECT * FROM DSTHEOLO";
        Statement stSP = DALDuLieu.con.createStatement();
        ResultSet dsSP = stSP.executeQuery(sql);
        while (dsSP.next()) {
            SanPham SP = new SanPham();
            SP.setMalohang(dsSP.getString("MALO"));
            SP.setMahh(dsSP.getString("MAHH"));
            SP.setGianhap(dsSP.getInt("GIANHAP"));
            SP.setGiaban(dsSP.getInt("GIABAN"));
            SP.setNgaynhap(dsSP.getDate("NGAYNHAP"));
            SP.setNsx(dsSP.getDate("NSX"));
            SP.setHsd(dsSP.getDate("HSD"));
            SP.setSlnhap(dsSP.getFloat("SLNHAP"));
            SP.setSlkho(dsSP.getFloat("SLKHO"));
            SP.setSldangban(dsSP.getFloat("SLDANGBAN"));
            SP.setSldaban(dsSP.getFloat("SLDABAN"));
            vSP.add(SP);
        }
        stSP.close();
        dsSP.close();
        return vSP;
    }
    public static ArrayList<KhachHang> LayDuLieuKhachHang() throws SQLException {
        ArrayList<KhachHang> DSKhachHang = new ArrayList<>();
        Vector<KhachHang> kh = new KhachHangDAL().LayTTKH();
        for (KhachHang KH : kh) {
            DSKhachHang.add(KH);
        }
        QLKhachHang.DSKhachHang = DSKhachHang;
        return DSKhachHang;
    }
    public static ArrayList<YCHangHoa> LayDuLieuYCHH() throws SQLException{
        ArrayList<YCHangHoa> YCHH = new ArrayList<YCHangHoa>();
        String sql="SELECT * FROM YEUCAUHH";
        Statement stYC = DALDuLieu.con.createStatement();
        ResultSet dsYC = stYC.executeQuery(sql);
        while(dsYC.next()){
            YCHangHoa yc = new YCHangHoa();
            yc.setMayeucauNB(dsYC.getString("MAYC"));
            yc.setManguonchuyenNB(dsYC.getString("MAKHO"));
            yc.setManguonycNB(dsYC.getString("MACN"));
            yc.setManhanvienYCNB(dsYC.getString("MANV"));
            yc.setManvxacnhan(dsYC.getString("MANVXACNHAN"));
            yc.setManvhoanthanh(dsYC.getString("MANVHOANTHANH"));
            yc.setSomnhatYCNB(dsYC.getDate("SOMNHAT"));
            yc.setTrenhatYCNB(dsYC.getDate("TRENHAT"));
            YCHH.add(yc);
        }
        stYC.close();
        dsYC.close();
        return YCHH;
    }
        public static ArrayList<CTYCHangHoa> LayDuLieuCTYCHH(String mayc) throws SQLException{
        ArrayList<CTYCHangHoa> CTYC = new ArrayList<CTYCHangHoa>();
        String sql="SELECT * FROM CTYEUCAUHH WHERE MAYC= '"+ mayc + "'";
        Statement stYC = DALDuLieu.con.createStatement();
        ResultSet dsYC = stYC.executeQuery(sql);
        while(dsYC.next()){
            CTYCHangHoa ctyc = new CTYCHangHoa();
            ctyc.setMayeucau(dsYC.getString("MAYC"));
            ctyc.setMahh(dsYC.getString("MAHH"));
            ctyc.setSoluong(dsYC.getFloat("SOLUONG"));
            CTYC.add(ctyc);
        }
        stYC.close();
        dsYC.close();
        return CTYC;
    }
    public static ArrayList<PhieuXuat> LayDuLieuXuatHang() throws SQLException{
        ArrayList<PhieuXuat> dspx = new ArrayList<>();
        String sql = "SELECT * FROM XUATHANG ";
        Statement stxh = DALDuLieu.con.createStatement();
        ResultSet dsxh = stxh.executeQuery(sql);
        while(dsxh.next()){
            PhieuXuat px = new PhieuXuat();
            px.setMaphieu(dsxh.getString("MAXUAT"));
            px.setManv(dsxh.getString("MANV"));
            px.setNgayphieu(dsxh.getDate("NGAYXUAT"));
            dspx.add(px); 
        }
        stxh.close();
        dsxh.close();
        return dspx;
    }
    public static ArrayList<CTPhieuXuat> LayDuLieuCTXuatHang(String maxh) throws SQLException{
        ArrayList<CTPhieuXuat> ctpx = new ArrayList<>();
        String sql = "SELECT * FROM CTXUATHANG WHERE MAXUAT= '"+maxh+"'";
        Statement ctxh = DALDuLieu.con.createStatement();
        ResultSet dsctxh = ctxh.executeQuery(sql);
        while(dsctxh.next()){
            CTPhieuXuat ctx = new CTPhieuXuat();
            ctx.setMalh(dsctxh.getString("MALO"));
            ctx.setMahh(dsctxh.getString("MAHH"));
            ctx.setSoluong(dsctxh.getFloat("SOLUONG"));
            ctpx.add(ctx); 
        }
        ctxh.close();
        dsctxh.close();
        return ctpx;
    }
    public static ArrayList<PhieuNhap> LayDuLieuNH() throws SQLException{
        ArrayList<PhieuNhap> dsnh=new ArrayList<>();
        String sql="SELECT * FROM PHIEUNHAP,LOHANG WHERE PHIEUNHAP.MALO=LOHANG.MALO AND MAKHO ='"+DangNhap.KHO+"'";
        Statement stnh = DALDuLieu.con.createStatement();
        ResultSet rsnh = stnh.executeQuery(sql);
        while(rsnh.next()){
            PhieuNhap nh=new PhieuNhap();
            nh.setMaphieu(rsnh.getString("MANHAP"));
            nh.setMalo(rsnh.getString("MALO"));
            nh.setManv(rsnh.getString("MANV"));
            nh.setNgayphieu(rsnh.getDate("NGAYNHAP"));
            dsnh.add(nh);
        }
        stnh.close();
        rsnh.close();
        return dsnh;
    }
    public static ArrayList<CTPhieuNhap> LayDuLieuCTNH(String manhap) throws SQLException{
        ArrayList<CTPhieuNhap> ctpx = new ArrayList<>();
        String sql = "SELECT * FROM CTPHIEUNHAP WHERE MANHAP= '"+manhap+"'";
        Statement stctnh = DALDuLieu.con.createStatement();
        ResultSet rsctnh = stctnh.executeQuery(sql);
        while(rsctnh.next()){
            CTPhieuNhap ctnh = new CTPhieuNhap();
            ctnh.setMahh(rsctnh.getString("MAHH"));
            ctnh.setSoluong(rsctnh.getFloat("SOLUONG"));
            ctnh.setGianhap(rsctnh.getInt("GIANHAP"));
            ctnh.setGiadukien(rsctnh.getInt("GIADUKIEN"));   
            ctpx.add(ctnh); 
        }
        stctnh.close();
        rsctnh.close();
        return ctpx;
    }
    public static ArrayList<YCLuanChuyen> LayDuLieuLC() throws SQLException{
        ArrayList <YCLuanChuyen> dslc=new ArrayList<>();
        String sql="SELECT * FROM LUANCHUYEN";
        Statement stlc = DALDuLieu.con.createStatement();
        ResultSet rslc = stlc.executeQuery(sql);
        while(rslc.next()){
            YCLuanChuyen lc=new YCLuanChuyen();
            lc.setMayeucauNB(rslc.getString("MALC"));
            lc.setManhanvienYCNB(rslc.getString("MANV"));
            lc.setManguonycNB(rslc.getString("MAKHOYC"));
            lc.setManguonchuyenNB(rslc.getString("MAKHOCHUYEN"));
            lc.setManvxacnhan(rslc.getString("MANVXACNHAN"));
            lc.setManvhoanthanh(rslc.getString("MANVHOANTHANH"));
            lc.setSomnhatYCNB(rslc.getDate("SOMNHAT"));
            lc.setTrenhatYCNB(rslc.getDate("TRENHAT"));
            dslc.add(lc);
        }
        stlc.close();
        rslc.close();
        return dslc;
    }
    public static ArrayList<YCDatHang> LayDuLieuDH(String makho) throws SQLException {
        ArrayList <YCDatHang> dsdh=new ArrayList<YCDatHang>();
        String sql="SELECT * FROM DATHANG WHERE MAKHO= '"+makho+"'";
        Statement stdh = DALDuLieu.con.createStatement();
        ResultSet rsdh = stdh.executeQuery(sql);
        while(rsdh.next()){
            YCDatHang dh=new YCDatHang();
            dh.setMayeucauDH(rsdh.getString("MADH"));
            dh.setManhanvienYCDH(rsdh.getString("MANV"));
            dh.setManguonchuyenYCDH(rsdh.getString("MADT"));
            dh.setSomnhatYCDH(rsdh.getDate("SOMNHAT"));
            dh.setTrenhatYCDH(rsdh.getDate("TRENHAT"));
            dsdh.add(dh);
        }
        stdh.close();
        rsdh.close();
        return dsdh;
    }
    public static ArrayList<CTDatHang> LayDuLieuCTDH(String madh) throws SQLException {
        ArrayList <CTDatHang> dsctdh=new ArrayList<CTDatHang>();
        String sql="SELECT * FROM CTDATHANG WHERE MADH= '"+madh+"'";
        Statement stctdh = DALDuLieu.con.createStatement();
        ResultSet rsctdh = stctdh.executeQuery(sql);
        while(rsctdh.next()){
            CTDatHang ctdh=new CTDatHang();
            ctdh.setMayeucau(rsctdh.getString("MADH"));
            ctdh.setMahh(rsctdh.getString("MAHH"));
            ctdh.setSoluong(rsctdh.getFloat("SOLUONG"));
            ctdh.setGiadenghi(rsctdh.getInt("GIADENGHI"));
            dsctdh.add(ctdh);
        }
        stctdh.close();
        rsctdh.close();
        return dsctdh;
}
    private static void LaySoDT(ChiNhanh cn) throws SQLException{
        String query = "SELECT * FROM CHINHANH_SDT WHERE MACN = '" + cn.getMaCN() + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        ArrayList<String> sdt = new ArrayList<>();
        while (rs.next()) {            
            sdt.add(rs.getString("SDT"));
        }
        rs.close();
        cn.setSdtCN(sdt);
    }
    private static String LayMaKho(String MaCN) throws SQLException {
        String query = "SELECT * FROM KHO WHERE MACN = '" + MaCN + "' AND MATT = 'TT01'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        String MAKHO = "";
        if (rs.next())
            MAKHO = rs.getString("MAKHO");
        rs.close();
        return MAKHO;
    }
    private static void LaySoDT(NhVienNgDung cn) throws SQLException{
        String query = "SELECT * FROM NV_SDT WHERE MANV = '" + cn.getMaNV() + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        ArrayList<String> sdt = new ArrayList<>();
        while (rs.next()) {            
            sdt.add(rs.getString("SDT"));
        }
        rs.close();
        cn.setSdtNV(sdt);
    }
    private static ArrayList<String> LaySoDT(String MANV) throws SQLException{
        String query = "SELECT * FROM NV_SDT WHERE MANV = '" + MANV + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        ArrayList<String> sdt = new ArrayList<>();
        while (rs.next()) {            
            sdt.add(rs.getString("SDT"));
        }
        rs.close();
        return sdt;
    }
    private static void LayChucVu(NhVienNgDung cn) throws SQLException{
        String query = "SELECT * FROM DSCHUCVU, CHUCVU WHERE DSCHUCVU.MACV = CHUCVU.MACV AND MANV = '" + cn.getMaNV() + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        ArrayList<ChucVu> cv = new ArrayList<>();
        while (rs.next()) {            
            ChucVu chucvu = new ChucVu(rs.getString("MACV"), rs.getString("TENCV"));
            cv.add(chucvu);
        }
        rs.close();
        cn.setChucvu(cv);
    }
    private static ChucVu[] LayChucVu(String MANV) throws SQLException{
        String query = "SELECT * FROM DSCHUCVU, CHUCVU WHERE DSCHUCVU.MACV = CHUCVU.MACV AND MANV = '" + MANV + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        ArrayList<ChucVu> cv = new ArrayList<>();
        while (rs.next()) {            
            ChucVu chucvu = new ChucVu(rs.getString("MACV"), rs.getString("TENCV"));
            cv.add(chucvu);
        }
        rs.close();
        return  cv.toArray(new ChucVu[0]);
    }
    public static File ChonFileNhap() {
        JFileChooser chooser = new JFileChooser();
        int i = chooser.showOpenDialog(chooser);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                return file;
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.DALDuLieu.ChonFileXuat(): "+e); 
            }
        }
        return null;
    }
    public static File ChonFileXuat() {
        JFileChooser chooser = new JFileChooser();
        int i = chooser.showSaveDialog(chooser);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                boolean check = file.getPath().contains(".xls") || file.getPath().contains(".");
                File fileSave = new File(check ? file+"" : file + ".xls");
                fileSave.getParentFile().mkdirs();
                return fileSave;
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.DALDuLieu.ChonFileXuat(): "+e); 
            }
        }
        return null;
    }
    public static String XuatExcel(JTable table) {
        try {
            File file = ChonFileXuat();
            String excelFilePath = file.getPath();
            if (excelFilePath.endsWith("xlsx")) {
	           return XuatExcelXLSX(file, table);
	    } else if (excelFilePath.endsWith("xls")) {
	           return XuatExcelXLS(file, table);
	    } else {
	        return ("Bạn chọn sai file rồi!");
	    }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcel(): "+e);
            return "Lỗi khi xuất!";
        }
        
    }
    public static String XuatExcelXLSX(File file, JTable table) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Nhân viên");
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        Cell cell;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        int n = model.getColumnCount();
        int m = model.getRowCount();
        int soDong = 0;
        Row row = sheet.createRow(soDong);
        for (int i = 0; i < n; i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        for (int i = 0; i < m; i++) {
            soDong++;
            row = sheet.createRow(soDong);
            for (int j = 0; j < n; j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i,j).toString());
            }
        }
        try {
            OutputStream stream = new FileOutputStream(file);
            workbook.write(stream);
            stream.close();
            workbook.close();
            return "Xuất thành công! Địa chỉ file: "+file.getPath();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcelXLSX(): "+e);
        }
        return "";
    }
    public static String XuatExcelXLS(File file,  JTable table) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Nhân viên");
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        Cell cell;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        int n = model.getColumnCount();
        int m = model.getRowCount();
        int soDong = 0;
        Row row = sheet.createRow(soDong);
        for (int i = 0; i < n; i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        for (int i = 0; i < m; i++) {
            soDong++;
            row = sheet.createRow(soDong);
            for (int j = 0; j < n; j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i,j).toString());
            }
        }
        try {
            OutputStream stream = new FileOutputStream(file);
            workbook.write(stream);
            stream.close();
            workbook.close();
            return "Xuất thành công! Địa chỉ file: "+file.getPath();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcelXLS(): "+e);
        }
        return "";
    }
    private static Object getCellValue(Cell cell) {
        CellType ct = cell.getCellType();
        if (ct == CellType.STRING) 
            return cell.getStringCellValue();
        if (ct == CellType.BOOLEAN) 
            return cell.getBooleanCellValue();
        if (ct == CellType.NUMERIC) 
            return cell.getNumericCellValue();

        return null;
    }
    public static ArrayList<ArrayList<String>> NhapExcel() throws IOException {
        ArrayList<ArrayList<String>> NoiDung = new ArrayList<>();
        try {
            File file = ChonFileNhap();
            String excelFilePath = file.getPath();
            FileInputStream inputstream = new FileInputStream(file);
            if (excelFilePath.endsWith("xlsx") || excelFilePath.endsWith("xls")) {
                Workbook workbook = getWorkbook(inputstream,file.getPath());
                Sheet firstSheet = workbook.getSheetAt(0);
                Iterator<Row> iterator = firstSheet.iterator();
                Row nextRow = iterator.next();
                while (iterator.hasNext()) {
                    nextRow = iterator.next();
                    Iterator<Cell> cellIterator = nextRow.cellIterator();
                    ArrayList<String> NoiDung1Dong = new ArrayList<>();
                    while (cellIterator.hasNext()) {
                        Cell nextCell = cellIterator.next();
                        int columnIndex = nextCell.getColumnIndex();
                        NoiDung1Dong.add(getCellValue(nextCell).toString());
                        System.out.println(getCellValue(nextCell).toString());
                    }
                    NoiDung.add(NoiDung1Dong);
                }
                workbook.close();
                return NoiDung;
            }
	    else {
	        return null;
	    }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.NhapExcel(): "+e);
            return null;
        }
        
    }
	
    private static Workbook getWorkbook(FileInputStream inputstream,String excelFilePath)
            throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputstream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputstream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }
    public static void main(String[] args) {
        try {
            NhapExcel();
        } catch (Exception e) {
        }

    }

    static void close() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

