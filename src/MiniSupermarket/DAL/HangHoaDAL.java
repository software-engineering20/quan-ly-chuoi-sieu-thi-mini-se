/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.HangHoa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author ADMIN
 */
public class HangHoaDAL {
    public static boolean ThemHangHoa(HangHoa HH) throws SQLException{    
        String sql=" Insert into HANGHOA values (?,?,?,?)";
        PreparedStatement stHH= DALDuLieu.con.prepareStatement(sql);
        stHH.setString(1, HH.getMahanghoa());
        stHH.setString(2, HH.getTenhanghoa());
        stHH.setNString(3, HH.getNuocsx());
        DALDuLieu.SoSanhDuLieuTT( HH.getTrangthai());
        stHH.setString(4, HH.getTrangthai().getMatrangthai());
        if(stHH.executeUpdate()>=1)
        {   
            stHH.close();
            return true;
        }
        stHH.close();
        return false;
    }
    public static HangHoa TimKiemHH(String mahh) throws SQLException{
            String sql="SELECT * FROM HANGHOA WHERE MAHH='"+mahh+"'";
            HangHoa hh= new HangHoa();
            Statement sthh = DALDuLieu.con.createStatement();
            ResultSet dshh = sthh.executeQuery(sql);
            dshh.next();
            hh.setMahanghoa(dshh.getString("MAHH"));
            hh.setTenhanghoa(dshh.getNString("TENHANG"));
            hh.setNuocsx(dshh.getNString("NUOCSX"));
            sthh.close();
            dshh.close();
            return hh;
  }
    public static boolean SuaHangHoa(HangHoa HH){
        boolean result = false;
        try {
            String sql = "UPDATE HANGHOA  "
                    + "SET TENHANG = ?, "
                    + " NUOCSX = ?, "
                    + " MATT=? "
                    + "WHERE MAHH = ? ";
            PreparedStatement stHH = DALDuLieu.con.prepareStatement(sql);
            DALDuLieu.SoSanhDuLieuTT(HH.getTrangthai());            
            stHH.setNString(1, HH.getTenhanghoa());
            stHH.setNString(2,HH.getNuocsx());
            stHH.setString(3, HH.getTrangthai().getMatrangthai());
            stHH.setString(4,HH.getMahanghoa());
            if (stHH.executeUpdate() >= 1){
                result = true;
            }
            stHH.close();
        } catch (Exception e ) {
            System.out.println(e);
        }
        return result;
 }   
    public static boolean DaCoHangHoa(String ma){
        boolean flag = false;
        try{        
            String sql ="SELECT * FROM HANGHOA WHERE MAHH = '"+ma+"'";
            Statement stHH =DALDuLieu.con.createStatement();
            ResultSet ds = stHH.executeQuery(sql);
            flag = ds.next();
            stHH.close();
      } catch (SQLException ex) {
            System.out.println("Da co HH DAL "+ex);
        } 
        return flag; 
    }
           
}
