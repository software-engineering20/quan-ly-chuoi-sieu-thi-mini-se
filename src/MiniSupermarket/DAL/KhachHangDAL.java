/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.KhachHang;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author admin
 */
public class KhachHangDAL {

    public Vector <KhachHang> LayTTKH() throws SQLException{
        Vector <KhachHang> dskh = new Vector<>();
            try {
                String sql = "SELECT * "
                        + "FROM KHACHHANG";
                Statement st = DALDuLieu.con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {                
                    KhachHang kh = new KhachHang();
                    kh.setMaKH(rs.getString("MAKH"));
                    kh.setTenKH(rs.getNString("TENKH"));
                    kh.setNgsinhKH(rs.getDate("NGAYSINH"));
                    kh.setDiachiKH(rs.getNString("DIACHI"));
                    kh.setDiem(rs.getInt("DIEM"));
                    dskh.add(kh);
                }
                rs.close();
                for (int i = 0;i < dskh.size();i++){
                    ResultSet rs0 = st.executeQuery("SELECT * "
                            + "FROM KHACHHANG_SDT "
                            + "WHERE MAKH = '" + dskh.get(i).getMaKH() + "'");
                    ArrayList <String> sdt = new ArrayList<>();
                    while (rs0.next()) {                        
                        sdt.add(rs0.getString("SDT"));
                    }
                    dskh.get(i).setSdtKH(sdt);
                    rs0.close();
                }
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.KhachHangDAL.LayTTKH() " + e);
            }
        return dskh;
    }
    public Vector <KhachHang> TimKH(String string){
        Vector <KhachHang> kh = new Vector<KhachHang>();
        KhachHang khachHang ;
        Vector <String> strings = new Vector<>();
        try {
            String sql;
            sql = "SELECT * "
                    + "FROM KHACHHANG_SDT "
                    + "WHERE MAKH = '" + string + "' "
                    + "OR SDT = '" + string + "' ";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()){
                khachHang = new KhachHang();
                if (!strings.contains(rs.getString("MAKH")))
                    strings.add(rs.getString("MAKH"));
                khachHang.setMaKH(string);
            }
            rs.close();
            if (strings.isEmpty()){
                strings.add(string);
            }
            Vector <String> strings1 = new Vector<>();
            for (int i = 0; i < strings.size(); i++){
                sql = "SELECT * "
                        + "FROM KHACHHANG "
                        + "WHERE MAKH = '" + strings.get(i) + "' "
                        + "OR TENKH LIKE N'%" + strings.get(i) + "%'";
                ResultSet rs0 = st.executeQuery(sql);
                while (rs0.next()) {
                    khachHang = new KhachHang();
                    khachHang.setMaKH(rs0.getString("MAKH"));
                    khachHang.setTenKH(rs0.getNString("TENKH"));
                    khachHang.setNgsinhKH(rs0.getDate("NGAYSINH"));
                    khachHang.setDiachiKH(rs0.getNString("DIACHI"));
                    khachHang.setDiem(rs0.getInt("DIEM"));
                    kh.add(khachHang);
                    strings1.add(khachHang.getMaKH());
                }
                rs0.close();
            }
            for (int i = 0; i < strings1.size(); i++){
                sql = "SELECT * "
                    + "FROM KHACHHANG_SDT "
                    + "WHERE MAKH = '" + strings1.get(i) + "' ";
                ResultSet rs1 = st.executeQuery(sql);
                ArrayList <String> sdt = new ArrayList<>();
                while (rs1.next()) { 
                    sdt.add(rs1.getString("SDT"));
                }
                kh.get(i).setSdtKH(sdt);
                rs1.close();            
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.TimKH() " + e);
        }
        return kh;
    }
    public KhachHang DSSDTKhachHang(String maKH){
        KhachHang kh = new KhachHang();
        try {
            String sql = "SELECT * "
                    + "FROM KHACHHANG "
                    + "WHERE MAKH = '" + maKH + "' ";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                kh.setMaKH(rs.getString("MAKH"));
                kh.setTenKH(rs.getNString("TENKH"));
                sql = "SELECT * "
                            + "FROM KHACHHANG_SDT "
                            + "WHERE MAKH = '" + maKH + "' ";
                ResultSet rs0 = DALDuLieu.con.createStatement().executeQuery(sql);
                ArrayList <String> sdt = new ArrayList<>();
                while (rs0.next()) {                        
                    sdt.add(rs0.getString("SDT"));
                }
                kh.setSdtKH(sdt);
                rs0.close();
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.DSSDTKhachHang() " + e);
        }
        return kh;
    }
    public boolean ThemKH(KhachHang khkh){
        boolean result = false;
        try {
            String sql = "INSERT INTO KHACHHANG (MAKH, TENKH, NGAYSINH, DIACHI, DIEM) "
                    + "VALUES (?, ?, ?, ?, ?)";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, khkh.getMaKH());
            pre.setString(2, khkh.getTenKH());
            pre.setDate(3, (Date) khkh.getNgsinhKH());
            pre.setString(4, khkh.getDiachiKH());
            pre.setInt(5, khkh.getDiem());
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();///
            for (int i = 0; i<khkh.getSdtKH().size(); i++){
                ThemSDT(khkh, i);
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.ThemKH() " + e);
        }
        return result;
    }
    public boolean ThemSDT(KhachHang khkh, int i){
        boolean result = false;
        try {
            String sql = "INSERT INTO KHACHHANG_SDT VALUES (?, ?)";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, khkh.getMaKH());
            pre.setString(2, khkh.getSdtKH().get(i));
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.ThemSDT() " + e);
        }
        return result;
    }
    public boolean XoaSDT(KhachHang kh){
        boolean result = false;
        try {
            String sql = "DELETE KHACHHANG_SDT "
                    + "WHERE SDT = ? "
                    + "AND MAKH = ? ";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, kh.getSdtKH().get(0));
            pre.setString(2, kh.getMaKH());
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.XoaSDT() " + e);
        }
        return result;
    }
    public boolean KTMaKH(String MaKH){
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM KHACHHANG "
                    + "WHERE MAKH = '" + MaKH + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            result = rs.next();
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.KTMaKH() " + e);
        }
        return result;
    }
    public boolean KTSDT(KhachHang kh, int i){
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM KHACHHANG_SDT "
                    + "WHERE MAKH = '" + kh.getMaKH() + "' "
                    + "AND SDT = '" + kh.getSdtKH().get(i) + "' ";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            result = rs.next();
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.KTSDT() " + e);
        }
        return result;
    }
    public boolean KTKH(String MaKH){
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM KHACHHANG "
                    + "WHERE MAKH = '" + MaKH + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            result = rs.next();
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.KTKH() " + e);
        }
        return result;
    }
    public boolean HopLeSDT(String s){ 
        Pattern p = Pattern.compile("([+]{1}[8]{1}[4]{1}[0-9]{9})|([0]{1}[0-9]{9})");
        Matcher m = p.matcher(s);
        return (m.matches());
    }
    public boolean SuaTTKH(KhachHang kh){
        boolean result = false;
        try {
            String sql = "UPDATE KHACHHANG "
                    + "SET TENKH = ?, "
                    + "NGAYSINH = ?, "
                    + "DIACHI = ?, "
                    + "DIEM = ? "
                    + "WHERE MAKH = ?";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setNString(1, kh.getTenKH());
            pre.setDate(2, (Date) kh.getNgsinhKH());
            pre.setString(3, kh.getDiachiKH());
            pre.setInt(4, kh.getDiem());
            pre.setString(5, kh.getMaKH());
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.SuaTTKH() " + e);
        }
        return result;
    }
   
    public int SDT(String makh){
        int k = 0;
        try {
            String sql = "SELECT * FROM KHACHHANG_SDT WHERE MAKH = '" + makh + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while (rs.next()){
                k++;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhachHangDAL.SDT() " + e);
        }
        return k;
    }

}
