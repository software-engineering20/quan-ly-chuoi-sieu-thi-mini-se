/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.ChucVu;
import XuLi.QuanLy.DangNhap;
import XuLi.QuanLy.QLNhanVien;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import javax.sql.*;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author mac
 */
public class NhanVienDAL {
    public static String ThemNhanVien(ArrayList<String> ThongTinDaXuLy) {
        String queryCheckMaNV = "SELECT * FROM NHANVIEN WHERE MANV = '"+"NV"+(QLNhanVien.DSNhanVien.size() + 1) +"' OR UN = '"+ThongTinDaXuLy.get(4)+"'";
        String queryNhanVien = "INSERT INTO NHANVIEN (MANV, TENNV, NGAYSINH,MATT, UN, PW, MACN)\n" + "VALUES (?, ?, ?, ?, ?, CONVERT(VARCHAR(32), HashBytes('MD5', ?), 2), ?)";
        String querySDT = "INSERT INTO NV_SDT (MANV, SDT)\n" + "VALUES (?, ?)";
        try {
            
            SimpleDateFormat formatThuong = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatSQL = new SimpleDateFormat("MM/dd/yyyy");
            java.util.Date NgSinh = formatThuong.parse(ThongTinDaXuLy.get(3));
            int age = NgSinh.getYear() - LocalDate.now().getYear() + 1900;
            if (NgSinh.getMonth() < LocalDate.now().getMonthValue()) {
                age -= 1;
            }
            else {
                if (NgSinh.getMonth() == LocalDate.now().getMonthValue()) {
                    if (NgSinh.getDate() > LocalDate.now().getMonthValue()) {
                        age -= 1;
                    }
                }
            }
            if (age > -18) 
                return "Chưa đủ tuổi";
            else if (!DALDuLieu.con.createStatement().executeQuery(queryCheckMaNV).next()) {
                PreparedStatement prest = DALDuLieu.con.prepareStatement(queryNhanVien);
                PreparedStatement prest1 = DALDuLieu.con.prepareStatement(querySDT);

                prest.setString(1, "NV"+(QLNhanVien.DSNhanVien.size()+1)+"");
                prest.setString(2, ThongTinDaXuLy.get(1).toString());
                prest.setString(3, formatSQL.format(formatThuong.parse(ThongTinDaXuLy.get(3).toString())));
                prest.setString(4, "TT01");
                prest.setString(5, ThongTinDaXuLy.get(4).toString());
                prest.setString(6, ThongTinDaXuLy.get(5).toString());
                prest.setString(7, DangNhap.MACN);

                prest1.setString(1, "NV"+(QLNhanVien.DSNhanVien.size() + 1));
                prest1.setString(2, ThongTinDaXuLy.get(2).toString());
                if(prest.executeUpdate() >= 1 && prest1.executeUpdate() >= 1) {
                    prest.close();
                    prest1.close();
                    return "Thêm thành công!";
                }
                else {
                    prest.close();
                    prest1.close();
                    return "Lỗi thêm vào Cơ sở dữ liệu!";
                }
            }
            else {
                return "Mã đã tồn tại!";
            }
        } catch (Exception e) {
            System.out.println(e);
            return "Lỗi thêm vào Cơ sở dữ liệu";
        }
    }
    public static String ThemNhieuSoDienThoai(String MANV, ArrayList<String> SDT) {
        try {
            String querySDT = "INSERT INTO NV_SDT (MANV, SDT) " + "VALUES ";
            boolean checkAdd = false;
            for (int i = 0; i < SDT.size() - 1; i++) 
                if (KiemTraSDT(MANV, SDT.get(i))) {
                    querySDT += "('"+MANV+"','"+SDT.get(i)+"'),";                
                    checkAdd = true;
                }
            if (KiemTraSDT(MANV, SDT.get(SDT.size() - 1))) {
                    querySDT += "('"+MANV+"','"+SDT.get(SDT.size() - 1)+"')";
                    checkAdd = true;
            }
            Statement st = DALDuLieu.con.createStatement();
            if (checkAdd) {
                int rs = st.executeUpdate(querySDT);
                if (rs > 0) 
                    return "Thêm số điện thoại thành công!"; 
                else
                    return "Thêm số điện thoại thành công!";
            }
            else 
                return "";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.ThemNhieuSoDienThoai(): "+e);
            return "Thêm số điện thoại không thành công!";
        }
    }
    public static String CapNhatNhanVien(ArrayList<String> ThongTinDaXuLy) {
        String queryUpdate = "UPDATE NHANVIEN SET TENNV = ?, NGAYSINH = ? WHERE MANV = '" + ThongTinDaXuLy.get(0) + "'";
        try {
            PreparedStatement prest = DALDuLieu.con.prepareStatement(queryUpdate);

            SimpleDateFormat formatThuong = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatSQL = new SimpleDateFormat("MM/dd/yyyy");
            prest.setString(1, ThongTinDaXuLy.get(1).toString());
            prest.setString(2, formatSQL.format(formatThuong.parse(ThongTinDaXuLy.get(3).toString())));
            if(prest.executeUpdate() >= 1) {
                prest.close();
                return "Sửa thành công!";
            }
            else {
                prest.close();
                return "Lỗi sửa nhân viên!";
            }
        } catch (Exception e) {
            System.out.println(e);
            return "Lỗi sửa nhân viên!";
        }
    }
    public static ArrayList<ChucVu> DSChucVu() {
        ArrayList<ChucVu> DSChucVu = new ArrayList<>();
        DALDuLieu.connectDB();
        try {
            String query = "SELECT * FROM CHUCVU";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {                
                DSChucVu.add(new ChucVu(rs.getString("MACV"), rs.getString("TENCV")));
            }
            st.close();
            rs.close();
            DALDuLieu.con.close();
            return DSChucVu;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.DSChucVu(): " + e);
        }
        return DSChucVu;
    }
    public static String XoaQuyen(String maNV, String machucvu) {
        DALDuLieu.connectDB();
        if (!machucvu.equals("") || machucvu != null) {
            try {
                String s = "DELETE FROM DSCHUCVU WHERE MANV = '"+maNV+"' AND MACV = '"+"'";
                Statement st = DALDuLieu.con.createStatement();
                int rs = st.executeUpdate(s);
                if (rs > 0) {
                    st.close();
                    DALDuLieu.con.close();
                    return "Xóa quyền thành công!";
                }
                else {
                    st.close();
                    DALDuLieu.con.close();
                    return "Xóa quyền "+machucvu+" không thành công";
                }
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.NhanVienDAL.XoaQuyen(): " + e);
                return "Xóa quyền không thành công!";
            }
        }
        return "Không có gì để xóa";
    }

    public static String ThemQuyen(String maNV, String machucvu) {
        DALDuLieu.connectDB();
        try {
            String check = "SELECT * FROM DSCHUCVU WHERE MANV = '"+maNV+"' AND MACV = '"+machucvu+"'";
            if (!DALDuLieu.con.createStatement().executeQuery(check).next()) {
                String s = "INSERT INTO DSCHUCVU(MANV, MACV) VALUES('"+maNV+"', '"+machucvu+"')";
                Statement st = DALDuLieu.con.createStatement();
                int rs = st.executeUpdate(s);
                if (rs > 0) {
                    st.close();
                    DALDuLieu.con.close();
                    return "Thêm quyền thành công!";
                }
                else {
                    st.close();
                    DALDuLieu.con.close();
                    return "Thêm quyền "+machucvu+" không thành công";
                }
            }
            return "Đã tồn tại!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.XoaQuyen(): " + e);
            return "Thêm quyền không thành công!";
        }
    }
    public static boolean KiemTraSDT(String MANV,String SDT) {
        DALDuLieu.connectDB();
        try { 
            String queryCheckSDT = "SELECT * FROM NV_SDT WHERE SDT = '"+SDT+"' AND MANV = '"+MANV+"'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(queryCheckSDT);
            if (rs.next()) {
                st.close();
                rs.close();
                DALDuLieu.con.close();
                return false;
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.KiemTraSDT(): "+e);
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        Object ob = "2002/11/30";
        Date date = Date.valueOf(ob.toString());
        System.out.println(date.toString());
    }

    public static String XoaSDT(String maNV, String SDT) {
        DALDuLieu.connectDB();
        try {
            String queryXoaSDT = "DELETE FROM NV_SDT WHERE MANV = '" + maNV + "' AND SDT = '" + SDT +"'"; 
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(queryXoaSDT);
            st.close();
            if (rs > 0) {
                DALDuLieu.con.close();
                return "Xóa thành công!";
            }
            else {
                DALDuLieu.con.close();
                return "Chưa xóa gì!";
            }
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.XoaSDT(): "+e);
            return "Xóa không thành công!";
        }
    }

    public static boolean NghiViec(String maNV) {
        try {
            DALDuLieu.connectDB();
            String queryNghiViec = "UPDATE NHANVIEN SET MATT = 'TT02' WHERE MANV = '" + maNV + "' ";
            for (int i = 0; i < DangNhap.ChucVu.length; i ++) {
                if (DangNhap.ChucVu[i].getTenchucvu().toLowerCase().contains("quản lý")) {
                   String queryDSCV = "SELECT * FROM DSCHUCVU, CHUCVU WHERE MANV = '" + maNV + "' AND DSCHUCVU.MACV = CHUCVU.MACV AND CHUCVU.TENCV = N'QUẢN LÝ'"; 
                   Statement st = DALDuLieu.con.createStatement(); 
                   if (st.executeQuery(queryDSCV).next()) {
                       return false;
                   }
                   break;
                }
            }
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(queryNghiViec);
            st.close();
            DALDuLieu.con.close();
            if (rs > 0) {
                return true;
            }
            else return false;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.NhanVienDAL.NghiViec(): "+e);
            return false;
        }
    }

  
}
