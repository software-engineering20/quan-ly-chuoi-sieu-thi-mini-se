/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.CTYCHangHoa;
import XuLi.HangCungCap;
import XuLi.YCHangHoa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author ADMIN
 */
public class YeuCauHHDAL {
    public static YCHangHoa TimYCHH(String mayc) throws SQLException{
        String sql="SELECT * FROM YEUCAUHH WHERE MAYC='"+mayc+"'";
        YCHangHoa ychh= new YCHangHoa();
        Statement sthh = DALDuLieu.con.createStatement();
        ResultSet dsYC = sthh.executeQuery(sql);
        dsYC.next();
        ychh.setManhanvienYCNB(dsYC.getString("MANV"));
        ychh.setManvxacnhan(dsYC.getString("MANVXACNHAN"));
        ychh.setManvhoanthanh(dsYC.getString("MANVHOANTHANH"));
        ychh.setSomnhatYCNB(dsYC.getDate("SOMNHAT"));
        ychh.setTrenhatYCNB(dsYC.getDate("TRENHAT"));
        sthh.close();
        dsYC.close();
        return ychh;
  }
    public static boolean ThemYCHH(YCHangHoa yc){
        try{
            String sql="INSERT INTO YEUCAUHH VALUES(?,?,?,?,?,?,?,?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, yc.getMayeucauNB());
            pr.setString(2, yc.getManguonchuyenNB());
            pr.setString(3, yc.getManguonycNB());
            pr.setString(4, yc.getManhanvienYCNB());
            pr.setString(5, yc.getManvxacnhan());
            pr.setString(6, yc.getManvhoanthanh());
            pr.setDate(7,yc.getSomnhatYCNB());
            pr.setDate(8,yc.getTrenhatYCNB());
            if(pr.executeUpdate()>=1)
            {
                pr.close();
                return true;
            }
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.YeuCauHHDAL.ThemYCHH()" +ex);
        }    
        return false;
    }
    public static boolean DaCoYCHH(String mayc){
        boolean flag = false;
        try{        
            String sql ="SELECT * FROM YEUCAUHH WHERE MAYC = '"+mayc+"'";
            Statement stYC =DALDuLieu.con.createStatement();
            ResultSet ds = stYC.executeQuery(sql);
            flag = ds.next();
            stYC.close();
      } catch (SQLException ex) {
            System.out.println(ex);
        } 
        return flag; 
    }
    public static boolean ThemCTYCHH(CTYCHangHoa ctyc) throws SQLException{
        try{    
            String sql="INSERT INTO CTYEUCAUHH VALUES(?,?,?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, ctyc.getMayeucau());
            pr.setString(2, ctyc.getMahh());
            pr.setFloat(3, ctyc.getSoluong());
            if(pr.executeUpdate()>=1)
            {
                pr.close();
                return true;
            }
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.YeuCauHHDAL.ThemCTYCHH()"+ ex);
        }    
        return false;
    }
    public static boolean XacNhanYCCN(YCHangHoa yc){
        boolean flag = false;
        try {
            String sql = "UPDATE YeuCauHH "
                    + "SET MANVHOANTHANH = ? "
                    + "WHERE MAYC = ? ";
            PreparedStatement stYC = DALDuLieu.con.prepareStatement(sql);
            stYC.setString(1, yc.getManvhoanthanh());
            stYC.setString(2,yc.getMayeucauNB()); 
            if (stYC.executeUpdate() >= 1){
                flag = true;
            }
            stYC.close();
        } catch (SQLException e ) {
            System.out.println(e);
        }
        return flag;
 }
        public static boolean XacNhanYCKho(YCHangHoa yc){
        boolean flag = false;
        try {
            String sql = " UPDATE YeuCauHH "
                    + " SET MANVXACNHAN = ? "
                    + " WHERE MAYC = ? ";
            PreparedStatement stYC = DALDuLieu.con.prepareStatement(sql);
            stYC.setString(1,yc.getManvxacnhan()); 
            stYC.setString(2,yc.getMayeucauNB()); 
            if (stYC.executeUpdate() >= 1){
                flag = true;
            }
            stYC.close();
        } catch (SQLException e ) {
            System.out.println(e);
        }
        return flag;
 }
}
