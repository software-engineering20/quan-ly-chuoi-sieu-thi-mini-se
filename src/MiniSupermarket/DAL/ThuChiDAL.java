/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.ChiTietThuChiCNSP;
import XuLi.ChiNhanh;
import XuLi.ChiTietNVKH;
import XuLi.HangHoa;
import XuLi.KhachHang;
import XuLi.NhVienNgDung;
import XuLi.QuanLy.DangNhap;
import XuLi.QuanLy.QLChiNhanh;
import XuLi.QuanLy.QLHangHoa;
import XuLi.QuanLy.QLKhachHang;
import XuLi.QuanLy.QLNhanVien;
import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement;
import java.sql.*;
import java.sql.SQLException;
import javax.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author mac
 */
public class ThuChiDAL {
    //Thong ke chi nhanh
    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiChiNhanhTheoThoiGian(LocalDateTime BatDau, LocalDateTime KetThuc, ChiNhanh ChiNhanh, int kindTime) throws  SQLException{
        LocalDateTime[] NgayBDKT = {BatDau, KetThuc};
        DuaVeGioiHan(NgayBDKT);
        ArrayList<Long> TongThu = TongThuChiNhanhTheoThoiGian(ChiNhanh, NgayBDKT, kindTime);
        ArrayList<Long> TongChi = TongChiChiNhanhTheoThoiGian(ChiNhanh, NgayBDKT, kindTime);
        ArrayList<Long> LoiNhuan = LoiNhuan(TongThu, TongChi);
        ArrayList<Float> PhanTramLoiNhuan = PhanTramLoiNhuan(LoiNhuan, TongChi);
        ArrayList<ChiTietThuChiCNSP> ChiTietThuchi = new ArrayList<>();
        int n = TongThu.size();
        for (int i = 0; i < n; i++) {
            ChiTietThuChiCNSP ct = null;
            if (kindTime == 1) {
                ct = new ChiTietThuChiCNSP(ChiNhanh.getMaCN(), NgayBDKT[0], NgayBDKT[0], TongThu.get(i), TongChi.get(i), LoiNhuan.get(i), PhanTramLoiNhuan.get(i));
                NgayBDKT[0] = NgayBDKT[0].plusDays(1);
            }
            else if (kindTime == 2) {
                ct = new ChiTietThuChiCNSP(ChiNhanh.getMaCN(), NgayBDKT[0], NgayBDKT[0].plusMonths(1), TongThu.get(i), TongChi.get(i), LoiNhuan.get(i), PhanTramLoiNhuan.get(i));
                NgayBDKT[0] = NgayBDKT[0].plusMonths(1);
            }
            else if (kindTime == 3) {
                ct = new ChiTietThuChiCNSP(ChiNhanh.getMaCN(), NgayBDKT[0], NgayBDKT[0].plusYears(1), TongThu.get(i), TongChi.get(i), LoiNhuan.get(i), PhanTramLoiNhuan.get(i));
                NgayBDKT[0] = NgayBDKT[0].plusYears(1);
            }
            ChiTietThuchi.add(ct);
        }
        return ChiTietThuchi;
    }
    
    //Theo ngày
    public static ArrayList<Long> TongThuChiNhanhTheoThoiGian(ChiNhanh ChiNhanh, LocalDateTime[] NgBDKT, int kindTime) {
        ArrayList<Long> TongThu = new ArrayList<>();
        ArrayList<Long> TongHoaDon = TongHoaDonChiNhanhTheoThoiGian(ChiNhanh, NgBDKT, kindTime);
        ArrayList<Long> TongLuanChuyen = TongLuanChuyenChiNhanhTheoThoiGian(ChiNhanh, NgBDKT, kindTime);
        int i = 0;
        LocalDateTime NgBD = NgBDKT[0];
        while ((kindTime == 1 && !NgBD.minusDays(1).isEqual(NgBDKT[1])) || (kindTime == 2 && !NgBD.minusMonths(1).isEqual(NgBDKT[1])) || (kindTime == 3 && !NgBD.minusYears(1).isEqual(NgBDKT[1]))) {
            TongThu.add(TongHoaDon.get(i) + TongLuanChuyen.get(i));
            i++;
            if (kindTime == 1) NgBD = NgBD.plusDays(1);
            else if (kindTime == 2) NgBD = NgBD.plusMonths(1);
            else if (kindTime == 3) NgBD = NgBD.plusYears(1);
        }
        return TongThu;
    }
    public static ArrayList<Long> TongChiChiNhanhTheoThoiGian(ChiNhanh ChiNhanh, LocalDateTime[] NgBDKT, int kindTime) throws SQLException {
        
        ArrayList<Long> TongChi = new ArrayList<>();
        TongChi = TongChiPhieuNhapChiNhanhTheoThoiGian(ChiNhanh, NgBDKT, kindTime);
        return TongChi;
    }
    public static ArrayList<Long> TongHoaDonChiNhanhTheoThoiGian(ChiNhanh ChiNhanh, LocalDateTime[] NgayBDKT, int kindTime) {
        String query = null;
        if (kindTime == 1) query = "SELECT SUM(TONGTIEN) TONGTHU FROM NHANVIEN, HOADON WHERE NHANVIEN.MANV = HOADON.MANV AND NHANVIEN.MACN = '"+ChiNhanh.getMaCN()+"' AND NGAYHD = ?";
        else if (kindTime == 2) query = "SELECT SUM(TONGTIEN) TONGTHU FROM NHANVIEN, HOADON WHERE NHANVIEN.MANV = HOADON.MANV AND NHANVIEN.MACN = '"+ChiNhanh.getMaCN()+"' AND MONTH(NGAYHD) = ? AND YEAR(NGAYHD) = ?";
        else if (kindTime == 3) query = "SELECT SUM(TONGTIEN) TONGTHU FROM NHANVIEN, HOADON WHERE NHANVIEN.MANV = HOADON.MANV AND NHANVIEN.MACN = '"+ChiNhanh.getMaCN()+"' AND YEAR(NGAYHD) = ?";
        ArrayList<Long> TongHoaDon = new ArrayList<>();
        TheoThoiGian(TongHoaDon, query, "TONGTHU", NgayBDKT, kindTime);
        return TongHoaDon;
    }
    public static ArrayList<Long> TongLuanChuyenChiNhanhTheoThoiGian(ChiNhanh ChiNhanh, LocalDateTime[] NgayBDKT, int kindTime) {
        String query = null;
        if (kindTime == 1) query = "SELECT SUM(CTLUANCHUYEN.SOLUONG * DSTHEOLO.GIANHAP) TONGTHU FROM LUANCHUYEN, CTLUANCHUYEN, DSTHEOLO WHERE LUANCHUYEN.MALC = CTLUANCHUYEN.MALC AND LUANCHUYEN.MAKHOCHUYEN = '"+ChiNhanh.getKhotructhuoc().getMaKho()+"' AND CTLUANCHUYEN.MAHH = DSTHEOLO.MAHH AND CTLUANCHUYEN.MALO = DSTHEOLO.MALO AND MANVHOANTHANH IS NOT NULL AND SOMNHAT = ?";
        else if (kindTime == 2) query = "SELECT SUM(CTLUANCHUYEN.SOLUONG * DSTHEOLO.GIANHAP) TONGTHU FROM LUANCHUYEN, CTLUANCHUYEN, DSTHEOLO WHERE LUANCHUYEN.MALC = CTLUANCHUYEN.MALC AND LUANCHUYEN.MAKHOCHUYEN = '"+ChiNhanh.getKhotructhuoc().getMaKho()+"' AND CTLUANCHUYEN.MAHH = DSTHEOLO.MAHH AND CTLUANCHUYEN.MALO = DSTHEOLO.MALO AND MANVHOANTHANH IS NOT NULL AND MONTH(SOMNHAT) = ? AND YEAR(SOMNHAT) = ?";
        else if (kindTime == 3) query = "SELECT SUM(CTLUANCHUYEN.SOLUONG * DSTHEOLO.GIANHAP) TONGTHU FROM LUANCHUYEN, CTLUANCHUYEN, DSTHEOLO WHERE LUANCHUYEN.MALC = CTLUANCHUYEN.MALC AND LUANCHUYEN.MAKHOCHUYEN = '"+ChiNhanh.getKhotructhuoc().getMaKho()+"' AND CTLUANCHUYEN.MAHH = DSTHEOLO.MAHH AND CTLUANCHUYEN.MALO = DSTHEOLO.MALO AND MANVHOANTHANH IS NOT NULL AND YEAR(SOMNHAT) = ?";
        ArrayList<Long> TongLuanChuyen = new ArrayList<>();
        TheoThoiGian(TongLuanChuyen, query, "TONGTHU", NgayBDKT, kindTime);
        return TongLuanChuyen;
    }
    public static ArrayList<Long> TongChiPhieuNhapChiNhanhTheoThoiGian(ChiNhanh ChiNhanh, LocalDateTime[] NgayBDKT, int kindTime) {
        String query = null;
        if (kindTime == 1) query = "SELECT SUM(GIANHAP*SOLUONG) TONGCHI FROM PHIEUNHAP, CTPHIEUNHAP,LOHANG WHERE PHIEUNHAP.MANHAP = CTPHIEUNHAP.MANHAP AND PHIEUNHAP.MALO = LOHANG.MALO AND MAKHO = '" + ChiNhanh.getKhotructhuoc().getMaKho()+ "' AND NGAYNHAP = ? GROUP BY MAKHO";
        else if (kindTime == 2) query = "SELECT SUM(GIANHAP*SOLUONG) TONGCHI FROM PHIEUNHAP, CTPHIEUNHAP,LOHANG WHERE PHIEUNHAP.MANHAP = CTPHIEUNHAP.MANHAP AND PHIEUNHAP.MALO = LOHANG.MALO AND MAKHO = '" + ChiNhanh.getKhotructhuoc().getMaKho()+ "' AND MONTH(NGAYNHAP) = ? AND YEAR(NGAYNHAP) = ? GROUP BY MAKHO";
        else if (kindTime == 3) query = "SELECT SUM(GIANHAP*SOLUONG) TONGCHI FROM PHIEUNHAP, CTPHIEUNHAP,LOHANG WHERE PHIEUNHAP.MANHAP = CTPHIEUNHAP.MANHAP AND PHIEUNHAP.MALO = LOHANG.MALO AND MAKHO = '" + ChiNhanh.getKhotructhuoc().getMaKho()+ "' AND YEAR(NGAYNHAP) = ? GROUP BY MAKHO";
        ArrayList<Long> TongChi = new ArrayList<>();
        TheoThoiGian(TongChi, query, "TONGCHI", NgayBDKT, kindTime);
        return TongChi;
    }
    
    //rút ra
    public static void TheoThoiGian(ArrayList<Long> TongHoaDon, String query, String key,LocalDateTime[] NgayBDKT, int kindTime) {
        PreparedStatement prest = null;
        try {
            prest = DALDuLieu.con.prepareStatement(query);
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            int i = 0;
            LocalDateTime NgBD = NgayBDKT[0];
            while (kindTime == 1 && !NgBD.minusDays(1).isEqual(NgayBDKT[1])) {
                long result = 0;
                String NgayHD = NgBD.getMonthValue()+ "/" + NgBD.getDayOfMonth()+ "/"+NgBD.getYear();  
                prest.setString(1, NgayHD);
                ResultSet rsTongHoaDon = prest.executeQuery();            
                if (rsTongHoaDon.next()) {
                    result = rsTongHoaDon.getLong(key);
                }
                else 
                    result = 0;
                rsTongHoaDon.close();
                TongHoaDon.add(result);
                NgBD = NgBD.plusDays(1);
            }
            while (kindTime == 2 && !NgBD.minusMonths(1).isEqual(NgayBDKT[1])) {
                long result = 0;
                String Thang = NgBD.getMonthValue()+"";
                String Nam = NgBD.getYear()+"";
                prest.setString(1, Thang);
                prest.setString(2, Nam);
                ResultSet rsTongHoaDon = prest.executeQuery();            
                if (rsTongHoaDon.next()) {
                    result = rsTongHoaDon.getLong(key);
                }
                else 
                    result = 0;
                rsTongHoaDon.close();
                TongHoaDon.add(result);
                NgBD = NgBD.plusMonths(1);
            }
            while (kindTime == 3 && !NgBD.minusYears(1).isEqual(NgayBDKT[1])) {
                long result = 0;
                String Nam = NgBD.getYear()+"";
                prest.setString(1, Nam);
                ResultSet rsTongHoaDon = prest.executeQuery();            
                if (rsTongHoaDon.next()) {
                    result = rsTongHoaDon.getLong(key);
                }
                else 
                    result = 0;
                rsTongHoaDon.close();
                TongHoaDon.add(result);
                NgBD = NgBD.plusYears(1);
            }
            prest.close();
        }
        
        catch (Exception e) {
            System.out.println(e);
        }
    }
    //Thong ke cac chi nhanh
    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiCacChiNhanhDaSapXep(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        ArrayList<ChiTietThuChiCNSP> ThongKe = ThongKeThuChiCacChiNhanh(BatDau, KetThuc);
        int n = ThongKe.size();
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (ThongKe.get(i).getLoiNhuan() < ThongKe.get(j).getLoiNhuan()) {
                    ChiTietThuChiCNSP temp = ThongKe.get(i);
                    ThongKe.set(i, ThongKe.get(j));
                    ThongKe.set(j, temp);
                }
            }
        }
        return ThongKe;
    }
    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiCacChiNhanh(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        LocalDateTime[] NgayBDKT = {BatDau, KetThuc};
        ArrayList<ChiNhanh> CacChiNhanh = QLChiNhanh.DSChiNhanh;
        DuaVeGioiHan(NgayBDKT);
        ArrayList<Long> TongThu = TongThuCacChiNhanh(CacChiNhanh, NgayBDKT);
        ArrayList<Long> TongChi = TongChiCacChiNhanh(CacChiNhanh, NgayBDKT);
        ArrayList<Long> LoiNhuan = LoiNhuan(TongThu, TongChi);
        ArrayList<Float> PhanTramLoiNhuan = PhanTramLoiNhuan(LoiNhuan, TongChi);
        int n = CacChiNhanh.size();
        ArrayList<ChiTietThuChiCNSP> ChiTietThuchi = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ChiTietThuChiCNSP ct = new ChiTietThuChiCNSP(CacChiNhanh.get(i).getMaCN(), NgayBDKT[0], NgayBDKT[1], TongThu.get(i), TongChi.get(i), LoiNhuan.get(i), PhanTramLoiNhuan.get(i));
            ChiTietThuchi.add(ct);
        }
        return ChiTietThuchi;
    }
    
    private static void DuaVeGioiHan(LocalDateTime[] NgayBDKT) {
        int n = NgayBDKT[0].compareTo(NgayBDKT[1]);
        if (n > 0) {
            LocalDateTime temp = NgayBDKT[0];
            NgayBDKT[0] = NgayBDKT[1];
            NgayBDKT[1] = temp;
            n *= -1;
        }
//        if (n < -10) {
//            NgayBDKT[0] = NgayBDKT[1].minusDays(10);
//        }
    }
    
    public static ArrayList<Long> TongThuCacChiNhanh(ArrayList<ChiNhanh> CacChiNhanh, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongThu = new ArrayList<>();
        int n = CacChiNhanh.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatrihoadon;    
            long TongGiaTriHD = ThuChiDAL.TongHoaDonCacChiNhanh(CacChiNhanh.get(i), NgayBDKT);
            //tonggiatriluanchuyen
            long TongGiaTriLuanChuyen = ThuChiDAL.TongLuanChuyenCacChiNhanh(CacChiNhanh.get(i), NgayBDKT);
            //hoan thanh
            TongThu.add(TongGiaTriHD + TongGiaTriLuanChuyen);
        }
        return TongThu;
    }
    public static ArrayList<Long> TongChiCacChiNhanh(ArrayList<ChiNhanh> CacChiNhanh, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongChi = new ArrayList<>();
        int n = CacChiNhanh.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatriphieunhap;    
            long TongGiaTriPhieuNhap = ThuChiDAL.TongChiPhieuNhapCacChiNhanh(CacChiNhanh.get(i), NgayBDKT);
            //hoan thanh
            TongChi.add(TongGiaTriPhieuNhap);
        }
        return TongChi;
    }
    private static ArrayList<Long> LoiNhuan(ArrayList<Long> TongThu, ArrayList<Long> TongChi) {
        ArrayList<Long> LoiNhuan = new ArrayList<>();
        int n = TongThu.size();
        for (int i = 0; i < n; i++) {
            LoiNhuan.add(TongThu.get(i) - TongChi.get(i));
        }
        return LoiNhuan;
    }
    private static ArrayList<Float> PhanTramLoiNhuan(ArrayList<Long> LoiNhuan, ArrayList<Long> TongChi) {
        ArrayList<Float> PhanTramLoiNhuan = new ArrayList<>();
        int n = LoiNhuan.size();
        for (int i = 0; i < n; i++) {
            PhanTramLoiNhuan.add(((float) LoiNhuan.get(i) / (float) TongChi.get(i)) * 100.0f);
        }
        return PhanTramLoiNhuan;
    }
    public static long TongHoaDonCacChiNhanh(ChiNhanh cn, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYHD >= '" + NgayBDKT[0].getMonthValue()+ "/" + NgayBDKT[0].getDayOfMonth()+ "/"+NgayBDKT[0].getYear()+" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYHD <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() + " " +GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongHoaDon = DALDuLieu.con.createStatement();
        String queryTongHoaDon = "SELECT SUM(TONGTIEN) TONGTHU FROM NHANVIEN, HOADON WHERE NHANVIEN.MANV = HOADON.MANV AND NHANVIEN.MACN = '"+cn.getMaCN()+"' AND " + compareDate;
        long result = TongQuat(queryTongHoaDon, "TONGTHU"); 
        return result;
    }
    public static long TongLuanChuyenCacChiNhanh(ChiNhanh cn, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "SOMNHAT >= '" + NgayBDKT[0].getMonthValue() + "/" + NgayBDKT[0].getDayOfMonth() + "/"+NgayBDKT[0].getYear() +" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"SOMNHAT <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() +" "+GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        String queryTongLuanChuyen = "SELECT SUM(CTLUANCHUYEN.SOLUONG * DSTHEOLO.GIANHAP) TONGTHU FROM LUANCHUYEN, CTLUANCHUYEN, DSTHEOLO WHERE LUANCHUYEN.MALC = CTLUANCHUYEN.MALC AND LUANCHUYEN.MAKHOCHUYEN = '"+cn.getKhotructhuoc().getMaKho()+"' AND CTLUANCHUYEN.MAHH = DSTHEOLO.MAHH AND CTLUANCHUYEN.MALO = DSTHEOLO.MALO AND MANVHOANTHANH IS NOT NULL AND "+compareDate;
        long result = TongQuat(queryTongLuanChuyen, "TONGTHU");
        return result;
    }
    public static long TongChiPhieuNhapCacChiNhanh(ChiNhanh cn, LocalDateTime[] NgayBDKT) throws SQLException{
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYNHAP >= '" + NgayBDKT[0].getMonthValue() + "/" + NgayBDKT[0].getDayOfMonth() + "/"+NgayBDKT[0].getYear() +" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYNHAP <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() +" "+GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongChi = DALDuLieu.con.createStatement();
        String queryTongChi = "SELECT SUM(GIANHAP*SOLUONG) TONGCHI FROM PHIEUNHAP, CTPHIEUNHAP,LOHANG WHERE PHIEUNHAP.MANHAP = CTPHIEUNHAP.MANHAP AND PHIEUNHAP.MALO = LOHANG.MALO AND MAKHO = '" + cn.getKhotructhuoc().getMaKho()+ "' AND "+compareDate + " GROUP BY MAKHO";
        long result = TongQuat(queryTongChi, "TONGCHI");
        return result;
    }
    private static long TongQuat(String query, String key) {
        long result = 0;
        try {
            Statement stLuanChuyen = DALDuLieu.con.createStatement();
            ResultSet rsTongLuanChuyen = stLuanChuyen.executeQuery(query);
            if (rsTongLuanChuyen.next()) 
                result = rsTongLuanChuyen.getLong(key);
            else 
                result = 0;
            stLuanChuyen.close();
            rsTongLuanChuyen.close();
        } catch (Exception e) {
        }
        return result;
    }
    private static String[] ChinhLaiGio(LocalDateTime Ngay) {
        String hour = Ngay.getHour()+"";
        String minute = Ngay.getMinute()+"";
        String second = Ngay.getSecond()+"";
        if (hour.length() == 1) {
            hour = 0 + hour;
        }
        if (minute.length() == 1) {
            minute = 0 + minute;
        }
        if (second.length() == 1) {
            second = 0 + second;
        }
        return new String[]{hour, minute, second};
    }
    public static ArrayList<ChiTietNVKH> ThongKeThuChiNhanVienDaSapXep(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        ArrayList<ChiTietNVKH> ThongKe = ThongKeThuChiNhanVien(BatDau, KetThuc);
        int n = ThongKe.size();
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (ThongKe.get(i).getTongThu() < ThongKe.get(j).getTongThu()) {
                    ChiTietNVKH temp = ThongKe.get(i);
                    ThongKe.set(i, ThongKe.get(j));
                    ThongKe.set(j, temp);
                }
            }
        }
        return ThongKe;
    }
    public static ArrayList<ChiTietNVKH> ThongKeThuChiNhanVien(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        LocalDateTime[] NgayBDKT = {BatDau, KetThuc};
        ArrayList<NhVienNgDung> DSNhanVien = QLNhanVien.DSNhanVien;
        DuaVeGioiHan(NgayBDKT);
        ArrayList<Long> TongThu = TongThuNhanVien(DSNhanVien, NgayBDKT);
        int n = DSNhanVien.size();
        ArrayList<ChiTietNVKH> ChiTietNV = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ChiTietNVKH ct = new ChiTietNVKH(DSNhanVien.get(i).getMaNV(), BatDau, BatDau, TongThu.get(i));
            ChiTietNV.add(ct);
        }
        return ChiTietNV;
    }
    public static ArrayList<Long> TongThuNhanVien(ArrayList<NhVienNgDung> DSNhanVien, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongThu = new ArrayList<>();
        int n = DSNhanVien.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatrihoadon;    
            long TongGiaTriHD = ThuChiDAL.TongHoaDonNhanVien(DSNhanVien.get(i), NgayBDKT);
            //hoan thanh
            TongThu.add(TongGiaTriHD);
        }
        return TongThu;
    }
    public static long TongHoaDonNhanVien(NhVienNgDung cn, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYHD >= '" + NgayBDKT[0].getMonthValue()+ "/" + NgayBDKT[0].getDayOfMonth()+ "/"+NgayBDKT[0].getYear()+" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYHD <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() + " " +GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongHoaDon = DALDuLieu.con.createStatement();
        String queryTongHoaDon = "SELECT SUM(TONGTIEN) TONGTHU FROM NHANVIEN, HOADON WHERE NHANVIEN.MANV = HOADON.MANV AND NHANVIEN.MANV = '"+cn.getMaNV()+"' AND " + compareDate;
        long result = TongQuat(queryTongHoaDon, "TONGTHU"); 
        return result;
    }
    
    public static ArrayList<ChiTietNVKH> ThongKeThuChiKhachHangDaSapXep(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        ArrayList<ChiTietNVKH> ThongKe = ThongKeThuChiKhachHang(BatDau, KetThuc);
        int n = ThongKe.size();
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (ThongKe.get(i).getTongThu() < ThongKe.get(j).getTongThu()) {
                    ChiTietNVKH temp = ThongKe.get(i);
                    ThongKe.set(i, ThongKe.get(j));
                    ThongKe.set(j, temp);
                }
            }
        }
        return ThongKe;
    }
    public static ArrayList<ChiTietNVKH> ThongKeThuChiKhachHang(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        LocalDateTime[] NgayBDKT = {BatDau, KetThuc};
        ArrayList<KhachHang> DSKhachHang = QLKhachHang.DSKhachHang;
        DuaVeGioiHan(NgayBDKT);
        ArrayList<Long> TongThu = TongThuKhachHang(DSKhachHang, NgayBDKT);
        int n = DSKhachHang.size();
        ArrayList<ChiTietNVKH> ChiTietNV = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ChiTietNVKH ct = new ChiTietNVKH(DSKhachHang.get(i).getMaKH(), BatDau, BatDau, TongThu.get(i));
            ChiTietNV.add(ct);
        }
        return ChiTietNV;
    }
    public static ArrayList<Long> TongThuKhachHang(ArrayList<KhachHang> DSKhachHang, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongThu = new ArrayList<>();
        int n = DSKhachHang.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatrihoadon;    
            long TongGiaTriHD = ThuChiDAL.TongHoaDonKhachHang(DSKhachHang.get(i), NgayBDKT);
            //hoan thanh
            TongThu.add(TongGiaTriHD);
        }
        return TongThu;
    }
    public static long TongHoaDonKhachHang(KhachHang cn, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYHD >= '" + NgayBDKT[0].getMonthValue()+ "/" + NgayBDKT[0].getDayOfMonth()+ "/"+NgayBDKT[0].getYear()+" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYHD <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() + " " +GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongHoaDon = DALDuLieu.con.createStatement();
        String queryTongHoaDon = "SELECT SUM(TONGTIEN) TONGTHU FROM KHACHHANG, HOADON WHERE KHACHHANG.MAKH = HOADON.MAKH AND KHACHHANG.MAKH = '"+cn.getMaKH()+"' AND " + compareDate;
        long result = TongQuat(queryTongHoaDon, "TONGTHU"); 
        return result;
    }

    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiSanPhamDaSapXep(LocalDateTime NgayBD, LocalDateTime NgayKT) {
        try {
            ArrayList<ChiTietThuChiCNSP> ThongKe = ThongKeThuChiSanPham(NgayBD, NgayKT);
            int n = ThongKe.size();
            for (int i = 0; i < n - 1; i++) {
                for (int j = i + 1; j < n; j++) {
                    if (ThongKe.get(i).getLoiNhuan() < ThongKe.get(j).getLoiNhuan()) {
                        ChiTietThuChiCNSP temp = ThongKe.get(i);
                        ThongKe.set(i, ThongKe.get(j));
                        ThongKe.set(j, temp);
                    }
                }
            }
            return ThongKe;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ThuChiDAL.ThongKeThuChiSanPhamDaSapXep(): "+e);
        }
        return new ArrayList<ChiTietThuChiCNSP>();
    }
    
    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiSanPhamSX(LocalDateTime NgayBD, LocalDateTime NgayKT) {
        try {
            ArrayList<ChiTietThuChiCNSP> listChiTiet = ThongKeThuChiSanPham(NgayKT, NgayKT);
            int n = listChiTiet.size();
            for (int i = 0; i < n-1; i++) {
                for (int j = 1; j < n; j++) {
                    if (listChiTiet.get(i).getLoiNhuan() < listChiTiet.get(j).getLoiNhuan()) {
                        ChiTietThuChiCNSP temp = listChiTiet.get(i);
                        listChiTiet.set(i, listChiTiet.get(j));
                        listChiTiet.set(j, temp);
                    }
                }
            }
            return listChiTiet;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ThuChiDAL.ThongKeThuChiSanPhamSX(): "+e);
            return null;
        }
    }
    public static ArrayList<ChiTietThuChiCNSP> ThongKeThuChiSanPham(LocalDateTime BatDau, LocalDateTime KetThuc) throws  SQLException{
        LocalDateTime[] NgayBDKT = {BatDau, KetThuc};
        ArrayList<HangHoa> DSHangHoa = QLHangHoa.DSHangHoa;
        DuaVeGioiHan(NgayBDKT);
        ArrayList<Long> TongThu = TongThuSanPham(DSHangHoa, NgayBDKT);
        ArrayList<Long> TongChi = TongChiSanPham(DSHangHoa, NgayBDKT);
        ArrayList<Long> LoiNhuan = LoiNhuan(TongThu, TongChi);
        ArrayList<Float> PhanTramLoiNhuan = PhanTramLoiNhuan(LoiNhuan, TongChi);
        int n = DSHangHoa.size();
        ArrayList<ChiTietThuChiCNSP> ChiTietThuchi = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ChiTietThuChiCNSP ct = new ChiTietThuChiCNSP(DSHangHoa.get(i).getMahanghoa(), NgayBDKT[0], NgayBDKT[1], TongThu.get(i), TongChi.get(i), LoiNhuan.get(i), PhanTramLoiNhuan.get(i));
            ChiTietThuchi.add(ct);
        }
        return ChiTietThuchi;
    }
    public static ArrayList<Long> TongThuSanPham(ArrayList<HangHoa> DSHangHoa, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongThu = new ArrayList<>();
        int n = DSHangHoa.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatrihoadon;    
            long TongGiaTriHD = ThuChiDAL.TongHoaDonSanPham(DSHangHoa.get(i), NgayBDKT);
            //tonggiatriluanchuyen
            long TongGiaTriLuanChuyen = ThuChiDAL.TongLuanChuyenSanPham(DSHangHoa.get(i), NgayBDKT);
            //hoan thanh
            TongThu.add(TongGiaTriHD + TongGiaTriLuanChuyen);
        }
        return TongThu;
    }
    public static ArrayList<Long> TongChiSanPham(ArrayList<HangHoa> DSHangHoa, LocalDateTime[] NgayBDKT) throws SQLException {
        
        ArrayList<Long> TongChi = new ArrayList<>();
        int n = DSHangHoa.size();
        for (int i = 0; i < n; i++) {
            
            //tonggiatriphieunhap;    
            long TongGiaTriPhieuNhap = ThuChiDAL.TongChiPhieuNhapSanPham(DSHangHoa.get(i), NgayBDKT);
            //hoan thanh
            TongChi.add(TongGiaTriPhieuNhap);
        }
        return TongChi;
    }
    public static long TongHoaDonSanPham(HangHoa hh, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYHD >= '" + NgayBDKT[0].getMonthValue()+ "/" + NgayBDKT[0].getDayOfMonth()+ "/"+NgayBDKT[0].getYear()+" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYHD <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() + " " +GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongHoaDon = DALDuLieu.con.createStatement();
        String queryTongHoaDon = "SELECT SUM(CTHD.SOLONG * THANHTIEN) TONGTHU FROM NHANVIEN, HOADON, CTHD   WHERE NHANVIEN.MANV = HOADON.MANV AND HOADON.MAHD = CTHD.MAHD AND NHANVIEN.MACN = '"+DangNhap.MACN+"' AND MAHH = '"+hh.getMahanghoa()+"' AND " + compareDate;
        long result = TongQuat(queryTongHoaDon, "TONGTHU"); 
        return result;
    }
    public static long TongLuanChuyenSanPham(HangHoa hh, LocalDateTime[] NgayBDKT) throws SQLException {
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "SOMNHAT >= '" + NgayBDKT[0].getMonthValue() + "/" + NgayBDKT[0].getDayOfMonth() + "/"+NgayBDKT[0].getYear() +" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"SOMNHAT <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() +" "+GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        String queryTongLuanChuyen = "SELECT SUM(CTLUANCHUYEN.SOLUONG * DSTHEOLO.GIANHAP) TONGTHU FROM LUANCHUYEN, CTLUANCHUYEN, DSTHEOLO WHERE LUANCHUYEN.MALC = CTLUANCHUYEN.MALC AND LUANCHUYEN.MAKHOCHUYEN = '"+DangNhap.MAKHO+"' AND CTLUANCHUYEN.MAHH = DSTHEOLO.MAHH AND CTLUANCHUYEN.MAHH = '"+hh.getMahanghoa()+"'"+" AND CTLUANCHUYEN.MALO = DSTHEOLO.MALO AND MANVXACNHAN IS NOT NULL AND "+compareDate;
        long result = TongQuat(queryTongLuanChuyen, "TONGTHU");
        return result;
    }
    public static long TongChiPhieuNhapSanPham(HangHoa hh, LocalDateTime[] NgayBDKT) throws SQLException{
        String[] GioBD = ChinhLaiGio(NgayBDKT[0]);
        String[] GioKT = ChinhLaiGio(NgayBDKT[1]);
        String ngaybatdau = "NGAYNHAP >= '" + NgayBDKT[0].getMonthValue() + "/" + NgayBDKT[0].getDayOfMonth() + "/"+NgayBDKT[0].getYear() +" "+GioBD[0]+":"+GioBD[1]+":"+GioBD[2]+"'";
        String ngayketthuc = " AND "+"NGAYNHAP <= '" + NgayBDKT[1].getMonthValue() + "/" + NgayBDKT[1].getDayOfMonth() + "/"+NgayBDKT[1].getYear() +" "+GioKT[0]+":"+GioKT[1]+":"+GioKT[2]+"'";
        String compareDate = ngaybatdau + ngayketthuc;
        Statement stTongChi = DALDuLieu.con.createStatement();
        String queryTongChi = "SELECT SUM(GIANHAP*SOLUONG) TONGCHI FROM PHIEUNHAP, CTPHIEUNHAP,LOHANG WHERE PHIEUNHAP.MANHAP = CTPHIEUNHAP.MANHAP AND PHIEUNHAP.MALO = LOHANG.MALO AND MAKHO = '" + DangNhap.MAKHO+ "' AND CTPHIEUNHAP.MAHH ='"+hh.getMahanghoa()+"' AND "+compareDate + " GROUP BY MAKHO";
        long result = TongQuat(queryTongChi, "TONGCHI");
        return result;
    }
}
