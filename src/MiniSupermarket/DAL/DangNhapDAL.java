/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import java.sql.*;
import javax.sql.*;

/**
 *
 * @author mac
 */
public class DangNhapDAL {
    public static boolean DangNhap(String UN, String PW) {
        try {
            String query = "SELECT * FROM NHANVIEN WHERE UN = '"+ UN +"' AND PW = CONVERT(VARCHAR(32), HashBytes('MD5', '"+PW+"'), 2)  AND MATT = 'TT01'";
            Statement prest1 = DALDuLieu.con.createStatement();
            
            ResultSet rs0 = prest1.executeQuery(query);
            if (rs0.next()) {
                DALDuLieu.LayThongTinNguoiDangNhap(rs0.getString("MANV"));
                DALDuLieu.LayThongTinChiNhanh();
                rs0.close();
                return true;
            } 
            else {
                rs0.close();
                return false;
            }
        }
        catch(Exception e) {
            System.out.println("MiniSupermarket.DAL.DangNhapDAL.DangNhap(): "+e);
            return false;
        }
    }

    public static boolean KiemTraMatKhau(String text) {
        try {
            String query = "SELECT * FROM NHANVIEN WHERE MANV = ? AND PW = CONVERT(VARCHAR(32), HashBytes('MD5', ?), 2)";
            PreparedStatement prest = DALDuLieu.con.prepareStatement(query);
            prest.setString(1, XuLi.QuanLy.DangNhap.MA);
            prest.setString(2, text);
            ResultSet rs = prest.executeQuery();
            if (rs.next()) {
                rs.close();
                return true;
            } 
            else {
                rs.close();
                return false;
            }
        }
        catch(Exception e) {
            System.out.println("MiniSupermarket.DAL.DangNhapDAL.KTMK(): "+e);
            return false;
        }
    }

    public static boolean DoiMatKhau(String text1) {
        try {
            String update = "UPDATE NHANVIEN SET PW = CONVERT(VARCHAR(32), HashBytes('MD5', '"+text1+"'), 2) WHERE MANV = '"+XuLi.QuanLy.DangNhap.MA+"'";
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(update);
            if (rs > 0) 
                return true;
            return false;
        }
        catch(Exception e) {
            System.out.println("MiniSupermarket.DAL.DangNhapDAL.KTMK(): "+e);
            return false;
        }
    }
}
