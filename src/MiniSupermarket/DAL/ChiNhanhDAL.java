/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.ChucVu;
import XuLi.QuanLy.DangNhap;
import XuLi.QuanLy.QLChiNhanh;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author mac
 */
public class ChiNhanhDAL {
    public static String ThemChiNhanh(ArrayList<String> ThongTinDaXuLy) {
        String queryCheckMaCN = "SELECT * FROM CHINHANH WHERE MACN = '"+"CN"+(QLChiNhanh.DSChiNhanh.size() + 1)+"'";
        String queryChiNhanh = "INSERT INTO CHINHANH (MACN, TENCN, DIACHI,MATT)\n" + "VALUES (?, ?, ?, ?)";
        String querySDT = "INSERT INTO CHINHANH_SDT (MACN, SDT)\n" + "VALUES (?, ?)";
        try {
            if (!DALDuLieu.con.createStatement().executeQuery(queryCheckMaCN).next()) {
                PreparedStatement prest = DALDuLieu.con.prepareStatement(queryChiNhanh);
                PreparedStatement prest1 = DALDuLieu.con.prepareStatement(querySDT);

                prest.setString(1, "CN"+(QLChiNhanh.DSChiNhanh.size()+1));
                prest.setString(2, ThongTinDaXuLy.get(1).toString());
                prest.setString(3, ThongTinDaXuLy.get(2).toString());
                prest.setString(4, "TT01");

                prest1.setString(1, "CN"+(QLChiNhanh.DSChiNhanh.size()+1));
                prest1.setString(2, ThongTinDaXuLy.get(3).toString());
                if(prest.executeUpdate() >= 1 && prest1.executeUpdate() >= 1) {
                    prest.close();
                    prest1.close();
                    return "Thêm thành công!";
                }
                else {
                    prest.close();
                    prest1.close();
                    return "Lỗi thêm vào Cơ sở dữ liệu!";
                }
            }
            else {
                return "Mã đã tồn tại!";
            }
        } catch (Exception e) {
            System.out.println(e);
            return "Lỗi thêm vào Cơ sở dữ liệu";
        }
    }
    public static String ThemNhieuSoDienThoai(String MACN, ArrayList<String> SDT) {
        try {
            String querySDT = "INSERT INTO CHINHANH_SDT (MACN, SDT) " + "VALUES ";
            boolean checkAdd = false;
            for (int i = 0; i < SDT.size() - 1; i++) 
                if (KiemTraSDT(MACN, SDT.get(i))) {
                    querySDT += "('"+MACN+"','"+SDT.get(i)+"'),";                
                    checkAdd = true;
                }
            if (KiemTraSDT(MACN, SDT.get(SDT.size() - 1))) {
                    querySDT += "('"+MACN+"','"+SDT.get(SDT.size() - 1)+"')";
                    checkAdd = true;
            }
            Statement st = DALDuLieu.con.createStatement();
            if (checkAdd) {
                int rs = st.executeUpdate(querySDT);
                if (rs > 0) 
                    return "Thêm số điện thoại thành công!"; 
                else
                    return "Thêm số điện thoại thành công!";
            }
            else 
                return "";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ChiNhanhDAL.ThemNhieuSoDienThoai(): "+e);
            return "Thêm số điện thoại không thành công!";
        }
    }
    public static String CapNhatChiNhanh(ArrayList<String> ThongTinDaXuLy) {
        String queryUpdate = "UPDATE CHINHANH SET TENCN = ?, DIACHI = ? WHERE MACN = '" + ThongTinDaXuLy.get(0) + "'";
        try {
            PreparedStatement prest = DALDuLieu.con.prepareStatement(queryUpdate);
            prest.setString(1, ThongTinDaXuLy.get(1).toString());
            prest.setString(2, ThongTinDaXuLy.get(2).toString());
            if(prest.executeUpdate() >= 1) {
                prest.close();
                return "Sửa thành công!";
            }
            else {
                prest.close();
                return "Lỗi sửa nhân viên!";
            }
        } catch (Exception e) {
            System.out.println(e);
            return "Lỗi sửa nhân viên!";
        }
    }
    
    public static boolean KiemTraSDT(String MACN,String SDT) {
        try { 
            String queryCheckSDT = "SELECT * FROM CHINHANH_SDT WHERE SDT = '"+SDT+"' AND MACN = '"+MACN+"'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(queryCheckSDT);
            if (rs.next()) {
                st.close();
                rs.close();
                return false;
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ChiNhanhDAL.KiemTraSDT(): "+e);
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        Object ob = "2002/11/30";
        Date date = Date.valueOf(ob.toString());
        System.out.println(date.toString());
    }

    public static String XoaSDT(String maCN, String SDT) {
        try {
            String queryXoaSDT = "DELETE FROM CHINHANH_SDT WHERE MACN = '" + maCN + "' AND SDT = '" + SDT +"'"; 
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(queryXoaSDT);
            if (rs > 0) {
                
                return "Xóa thành công!";
            }
            else
                return "Chưa xóa gì!";
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ChiNhanhDAL.XoaSDT(): "+e);
            return "Xóa không thành công!";
        }
    }
    public static boolean DongCuaChiNhanh(String MACN) {
        try {
            String queryXoaSDT = "UPDATE CHINHANH SET MATT = 'TT02' WHERE MACN = '" + MACN + "'";
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(queryXoaSDT);
            if (rs > 0) {
                DongCuaKho(MACN);
                return true;
            }
            else
                return false;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ChiNhanhDAL.DongCuaChiNhanh(): "+e);
            return false;
        }
    }
    public static boolean DongCuaKho(String MACN) {
        try {
            String queryXoaSDT = "UPDATE KHO SET MATT = 'TT02' WHERE MACN = '" + MACN + "' AND MATT = 'TT01'";
            Statement st = DALDuLieu.con.createStatement();
            int rs = st.executeUpdate(queryXoaSDT);
            if (rs > 0) {
                return true;
            }
            else
                return false;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.ChiNhanhDAL.DongCuaKho(): "+e);
            return false;
        }
    }
}
