/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import MiniSupermarket.BLL.LuanChuyenBLL;
import XuLi.CTYCLuanChuyen;
import XuLi.YCLuanChuyen;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.sql.*;

/**
 *
 * @author mac
 */
public class LuanChuyenDAL {
    public static boolean YeuCauLuanChuyen(String MAYC, ArrayList<String> ThongTinYeuCau) {
        try {
            String query = "INSERT INTO LUANCHUYEN(MALC, MANV, MAKHOYC, MAKHOCHUYEN, SOMNHAT, TRENHAT) VALUES (?,?,?,?,?,?)";
            PreparedStatement prest = DALDuLieu.con.prepareStatement(query);
            SimpleDateFormat formatThuong = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatSQL = new SimpleDateFormat("MM/dd/yyyy");
            prest.setString(1, MAYC);
            prest.setString(2, XuLi.QuanLy.DangNhap.MA);
            prest.setString(3, XuLi.QuanLy.DangNhap.MAKHO);
            prest.setString(4, ThongTinYeuCau.get(0));
            prest.setString(5, formatSQL.format(formatThuong.parse(ThongTinYeuCau.get(1))));
            prest.setString(6, formatSQL.format(formatThuong.parse(ThongTinYeuCau.get(2))));
            if (prest.executeUpdate() > 0)
                return true;
            else return false;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.LuanChuyenDAL.YeuCauLuanChuyen(): "+e);
            return false;
        }
    }
    public static boolean ThemChiTietYeuCau(String MAYC, ArrayList<CTYCLuanChuyen> listCtyc) {
        try {
            boolean rs = true;
            String query = "INSERT INTO CTLUANCHUYEN(MALC, MAHH, SOLUONG) VALUES (?,?,?)";
            for (CTYCLuanChuyen ctyc : listCtyc) {
                PreparedStatement prest = DALDuLieu.con.prepareStatement(query);
                SimpleDateFormat formatThuong = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat formatSQL = new SimpleDateFormat("MM/dd/yyyy");
                prest.setString(1, MAYC);
                prest.setString(2, ctyc.getMahh());
                prest.setFloat(3, ctyc.getSoluong());
                if (prest.executeUpdate() <= 0)
                    return false;
            }
            return true;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.LuanChuyenDAL.ThemChiTietYeuCau(): "+e);
            return false;
        }
    }
    public static ArrayList<YCLuanChuyen> LayThongTinYCLC() {
        try {
            String query = "SELECT * FROM LUANCHUYEN";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(query);
            ArrayList<YCLuanChuyen> CacThongTin = new ArrayList<>();
            while (rs.next()) {
                YCLuanChuyen LuanChuyen = new YCLuanChuyen();
                LuanChuyen.setMayeucau(rs.getString("MALC"));
                LuanChuyen.setManguonchuyen(rs.getString("MAKHOCHUYEN"));
                LuanChuyen.setManguonyc(rs.getString("MAKHOYC"));
                LuanChuyen.setManhanvien(rs.getString("MANV"));
                LuanChuyen.setManvxacnhan(rs.getString("MANVXACNHAN"));
                LuanChuyen.setManvhoanthanh(rs.getString("MANVHOANTHANH"));
                LuanChuyen.setSomnhat(rs.getDate("SOMNHAT"));
                LuanChuyen.setTrenhat(rs.getDate("TRENHAT"));
                LuanChuyen.setChitiet(LayThongTinCTYCLC(LuanChuyen.getMayeucau()));
                CacThongTin.add(LuanChuyen);
            }
            return CacThongTin;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.LuanChuyenDAL.LayThongTinYCLC(): "+e);
            return null;
        }
        
        
    }
    public static ArrayList<CTYCLuanChuyen> LayThongTinCTYCLC(String MAYC) {
        try {
            String query = "SELECT * FROM CTLUANCHUYEN WHERE MALC ='"+MAYC+"'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(query);
            ArrayList<CTYCLuanChuyen> CacThongTin = new ArrayList<>();
            while (rs.next()) {
                CTYCLuanChuyen CTLuanChuyen = new CTYCLuanChuyen();
                CTLuanChuyen.setMahh(rs.getString("MAHH"));
                CTLuanChuyen.setMayeucau(rs.getString("MALC"));
                CTLuanChuyen.setSoluong(rs.getFloat("SOLUONG"));
                CacThongTin.add(CTLuanChuyen);
            }
            return CacThongTin;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.LuanChuyenDAL.LayThongTinCTYCLC(): "+e);
            return null;
        }
        
        
    }
    public static void main(String[] args) throws SQLException {
        DALDuLieu.connectDB();
        ArrayList<YCLuanChuyen> yc =  LayThongTinYCLC();
        DALDuLieu.con.close();
    }
}
