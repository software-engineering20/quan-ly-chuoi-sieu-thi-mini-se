/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.CTPhieu;
import XuLi.CTPhieuXuat;
import XuLi.SanPham;
import java.util.ArrayList;
import javax.sql.*;
import java.sql.*;
import java.sql.Date;
/**
 *
 * @author ADMIN
 */
public class SanPhamDAL {
    public static ArrayList<SanPham> TimSanPham(CTPhieuXuat px) throws SQLException{
        ArrayList<SanPham> arrsp = new ArrayList<>();
        String sql="SELECT * FROM DSTHEOLO WHERE MALO='"+px.getMalh()
                   + "' AND MAHH='"+px.getMahh()+"'";
        Statement stsp = DALDuLieu.con.createStatement();
        ResultSet dssp = stsp.executeQuery(sql);
        
        
        while(dssp.next()){
            SanPham sp= new SanPham();
            sp.setNsx(dssp.getDate("NSX"));
            sp.setHsd(dssp.getDate("HSD"));
            sp.setGianhap(dssp.getInt("GIANHAP"));
            arrsp.add(sp);
        }
        stsp.close();
        dssp.close();
        return arrsp;
   }
    public static SanPham LaySanPham(SanPham sp){
        String sql="SELECT TOP 1 * FROM DSTHEOLO WHERE MAHH='"+ sp.getMahh() +"'"
                   + " AND HSD > GETDATE() AND SLKHO>0"
                   + "ORDER BY HSD";
        try{
            Statement stsp = DALDuLieu.con.createStatement();
            ResultSet dssp = stsp.executeQuery(sql);
            
            while(dssp.next()){
                sp.setMalohang(dssp.getString("MALO"));
                sp.setSlkho(dssp.getFloat("SLKHO"));
            }
            stsp.close();
            dssp.close();
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.SanPhamDAL.LaySanPham()"+ex);
        }
        return sp;
   }
    public static boolean SuaSanPham(SanPham sp){
        boolean flag = false;
        String sql = "UPDATE DSTHEOLO " // :D ý chua sua nay ak :V
                    + "SET SLKHO = ? "
                    + "WHERE MALO = ? "
                    + "AND MAHH = ?";//:))
        try{
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setFloat(1, sp.getSlkho());
            pr.setString(2, sp.getMalohang());
            pr.setString(3, sp.getMahh());
            if (pr.executeUpdate() >= 1){
                flag = true;
            }
            pr.close();
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.SanPhamDAL.SuaSanPham()"+ex);
        }
        return flag;
    }
    public static boolean ThemSanPham(SanPham sp){
        boolean flag = false;
        String sql="INSERT INTO DSTHEOLO VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = DALDuLieu.con.prepareStatement(sql);
            ps.setString(1,sp.getMalohang() );
            ps.setString(2,sp.getMahh());
            ps.setInt(3,sp.getGianhap());
            ps.setInt(4,sp.getGiaban());
            ps.setDate(5, sp.getNgaynhap());
            ps.setDate(6, sp.getNsx());
            ps.setDate(7, sp.getHsd());
            ps.setFloat(8, sp.getSlnhap());
            ps.setFloat(9, sp.getSlkho());
            ps.setFloat(10, sp.getSldangban());
            ps.setFloat(11, sp.getSldaban());
            if(ps.executeUpdate()>=1)
            {
            ps.close();
            flag=true;
            }
            ps.close();
        } catch (SQLException ex) {
            System.out.println("MiniSupermarket.DAL.SanPhamDAL.ThemSanPham()"+ex);
        }
        return flag;
    }
}
