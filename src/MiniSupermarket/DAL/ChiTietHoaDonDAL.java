/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.CT1DongHD;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class ChiTietHoaDonDAL {

     public static boolean themCTHoaDon(XuLi.CT1DongHD cthd){
     String url="insert into CTHD values(?,?,?,?,?,?,?)";
	   try {
			PreparedStatement ps=MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
			ps.setString(1, cthd.getMahd());
                        System.out.println("mã hóa đơn : "+cthd.getMahd());
			ps.setString(2, cthd.getMahh());
                        System.out.println("Mã hàng hóa : "+ cthd.getMahh());
			ps.setString(3,cthd.getMalohang());
                        System.out.println("Mã lô hàngu : "+cthd.getMalohang());
			ps.setInt(4, cthd.getSoluong());
                        System.out.println("So luong : "+cthd.getSoluong());
			ps.setLong(5,cthd.getThanhtien());
                        System.out.println("Thanh tièn : "+cthd.getThanhtien());
                        ps.setFloat(6,cthd.getGiamgia());
                         System.out.println("Giarm giá : "+cthd.getGiamgia());
                        long chietkhau=cthd.getTongtien()- cthd.getThanhtien();
                        System.out.println("Chiết khấu : "+chietkhau);
                        ps.setLong(7,chietkhau);
			ps.executeUpdate();
			
			
		}catch(SQLException e)  
		{
                    System.out.print(e);
		}
         return false;
     }
     public static boolean xoaCTHD(String maHD){
        String url="delete CTHD from CTHD where MAHD='"+maHD+"'";
			try {
				PreparedStatement ps=MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
				return ps.executeUpdate()>=0;
			}catch(SQLException e)
			{
                            System.out.print(e);
			}
         return false;
     }
     public static ArrayList<XuLi.CT1DongHD> getDanhSachHD(){
         String url="select * from CTHD";
         ArrayList<XuLi.CT1DongHD> cthdList = new ArrayList<>();
			  try {
				 PreparedStatement ps=MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
				 
				  ResultSet rs=ps.executeQuery();
				  while(rs.next())
				  {
					 XuLi.CT1DongHD cthd=new XuLi.CT1DongHD();
                                         cthd.setMahd(rs.getString("MAHD"));
                                         cthd.setMahh(rs.getString("MAHH"));
                                         cthd.setMalohang(rs.getString("MALO"));
                                         cthd.setSoluong(rs.getInt("SOLONG"));
                                         cthd.setThanhtien(rs.getInt("THANHTIEN"));
                                         cthd.setGiamgia(rs.getInt("GIAMGIA"));
                                         cthdList.add(cthd);
                                         
				  } 
			  }catch(SQLException e)
			  {
				 System.out.print(e);;
			  }
			  return cthdList;
     }
     public static ArrayList<XuLi.CT1DongHD> timKiem(String maHD) {
			
			ArrayList<XuLi.CT1DongHD> cthdList= new ArrayList<>();
			
				try {
		            Statement st = MiniSupermarket.DAL.DALDuLieu.con.createStatement();
					ResultSet rs = st.executeQuery("Select * from CTHD where MAHD like N'%"+maHD+"%'");
					
		            while(rs.next()) {
		            	XuLi.CT1DongHD cthd = new CT1DongHD();
                                cthd.setMahd(rs.getString("MAHD"));
                                cthd.setMahh(rs.getString("MAHH"));
                                cthd.setMalohang(rs.getString("MALO"));
                                cthd.setSoluong(rs.getInt("SOLONG"));
                                cthd.setThanhtien(rs.getInt("THANHTIEN"));
                                cthd.setGiamgia(rs.getInt("GIAMGIA"));	
                                cthdList.add(cthd);
		            }
		            return cthdList;
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			
			
			return null;
		}
     
}
