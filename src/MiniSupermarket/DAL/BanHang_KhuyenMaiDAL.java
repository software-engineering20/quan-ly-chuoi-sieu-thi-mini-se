/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

/**
 *
 * @author Admin
 */
import MiniSupermarket.BLL.KhoBLL;
import XuLi.DanhSachTang;
import XuLi.GiamGia;
import XuLi.KMSanPham;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.Statement;

public class BanHang_KhuyenMaiDAL {

    public static XuLi.SanPham maSpHopLe(String malo, String maHH) {

        XuLi.SanPham sp = new XuLi.SanPham();
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from DSTHEOLO where MALO = ? AND MAHH = ? ");
            prep.setString(1, malo);
            prep.setString(2, maHH);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                sp.setMalohang(malo);
                sp.setMahh(maHH);
                sp.setGianhap(rs.getInt(3));
                sp.setGiaban(rs.getInt(4));
                sp.setNgaynhap(rs.getDate(5));
                sp.setNsx(rs.getDate(6));
                sp.setHsd(rs.getDate(7));
                sp.setSlnhap(rs.getInt(8));
                sp.setSlkho(rs.getInt(9));
                sp.setSldangban(rs.getInt(10));
                sp.setSldaban(rs.getInt(11));
                return sp;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean maNVHopLe(String maNV) {
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from NHANVIEN where MANV = ? ");
            prep.setString(1, maNV);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static long maKHHopLe(String maKH) {
        long diem = 0;
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from KHACHHANG where MAKH = ? ");
            prep.setString(1, maKH);

            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                diem = rs.getLong(5);
                return diem;
            }
            return -1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static XuLi.HangHoa tenSanPham(String maHH) {

        try {
            XuLi.HangHoa hh = new XuLi.HangHoa();
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from HANGHOA where MAHH = ?");
            prep.setString(1, maHH);//(1, maSP);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                hh.setMahanghoa(maHH);
                hh.setTenhanghoa(rs.getString(2));
                hh.setNuocsx(rs.getString(3));
                KhoBLL k = new KhoBLL();
                hh.setTrangthai(k.TTTrangThai(rs.getString(4)));
                return hh;
            }
            return null;

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return null;

    }

    public static XuLi.DanhSachTang coKhuyenMai(String masp) {
        XuLi.DanhSachTang km = new DanhSachTang();

        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from DANHSACHTANG where MAHHTANG = ?");
            prep.setString(2, masp);//(1, maSP);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                km.setMaKM(rs.getString(1));
                km.setMahhtang(masp);
                km.setMalotang(rs.getString(3));
                km.setSoluongtang(rs.getInt(4));
                return km;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static XuLi.GiamGia coGiamGia(String masp) {
        XuLi.GiamGia gg = new GiamGia();
        try {
            PreparedStatement prep = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement("Select * from GIAMGIA where MAHH = ?");
            prep.setString(1, masp);
            ResultSet rs = prep.executeQuery();
            if (rs.next()) {
                gg.setMakhuyenmai(rs.getString(1));
                gg.setMahh(masp);
                gg.setMalh(rs.getString(3));
                gg.setPhantramgg(rs.getFloat(4));
                return gg;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean tichDiem(String maKH, long diem) {
        boolean flag = false;
        String url = "UPDATE KHACHHANG SET DIEM=? where MAKH=? ";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setLong(1, diem);
            ps.setString(2, maKH);
            if (ps.executeUpdate() >= 0) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static boolean capNhatDsTheoLo(String mahh, int sldangban, int sldaban) {
        boolean flag = false;
        String url = "UPDATE DSTHEOLO SET SLDANGBAN=?, SLDABAN=? where MAHH=? ";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setInt(1, sldangban);
            ps.setInt(2, sldaban);
            ps.setString(3, mahh);
            if (ps.executeUpdate() >= 0) {
                flag = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static boolean themKhuyenMai(XuLi.KhuyenMai km) {
        String url = "insert into KHUYENMAI values(?,?,?,?)";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, km.getMakhuyenmai());
            ps.setString(2, km.getTenkhuyenmai());
            ps.setDate(3, (Date) km.getNgaybd());
            ps.setDate(4, (Date) km.getNgaykt());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static boolean suaKM(XuLi.KMSanPham km) {
        boolean flag = false;
        String url = "UPDATE KHUYENMAI SET TENKM=?, NGAYBD=?,NGAYKT=? where MAKM=? ";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, km.getTenkhuyenmai());
            ps.setDate(2, (Date) km.getNgaybd());
            ps.setDate(3, (Date) km.getNgaykt());
            ps.setString(4, km.getMakhuyenmai());
            ps.executeUpdate();
            if (ps.executeUpdate() >= 0) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static boolean xoaKm(String maKM) {
        String url = "delete from KHUYENMAI where MAKM='" + maKM + "'";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static boolean themDsTang(XuLi.DanhSachTang dstang) {
        String url = "insert into DANHSACHTANG values(?,?,?,?)";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, dstang.getMaKM());
            ps.setString(2, dstang.getMahhtang());
            ps.setString(3, dstang.getMalotang());
            ps.setInt(4, dstang.getSoluongtang());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static boolean suaDsTang(XuLi.DanhSachTang dstang) {
        boolean flag = false;
        String url = "UPDATE DANHSACHTANG SET MAHHTANG=?, MALOTANG=? where MAKM=? ";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, dstang.getMahhtang());
            ps.setString(2, dstang.getMalotang());
            ps.setString(3, dstang.getMaKM());
            ps.executeUpdate();
            if (ps.executeUpdate() >= 0) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static boolean xoaDsTang(String maKM) {
        String url = "delete from DANHSACHTANG where MAKM='" + maKM + "'";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static boolean themDsMua(XuLi.DSMua dsmua) {
        String url = "insert into DANHSACHMUA values(?,?,?,?)";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, dsmua.getMakhuyenmai());
            ps.setString(2, dsmua.getMahh());
            ps.setString(3, dsmua.getMalh());
            ps.setInt(4, dsmua.getSoluong());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static boolean suaDsMua(XuLi.DSMua dsmua) {
        boolean flag = false;
        String url = "UPDATE DANHSACHMUA SET MAHHMUA=?, MALOMUA=? where MAKM=? ";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            ps.setString(1, dsmua.getMahh());
            ps.setString(2, dsmua.getMalh());
            ps.setString(3, dsmua.getMakhuyenmai());
            ps.executeUpdate();
            if (ps.executeUpdate() >= 0) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public static boolean xoaDsMua(String maKM) {
        String url = "delete from DANHSACHMUA where MAKM='" + maKM + "'";
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public static ArrayList<XuLi.KMSanPham> getDanhSachKm() {
        String url = "select * from KHUYENMAI,DANHSACHTANG,DANHSACHMUA WHERE KHUYENMAI.MAKM = DANHSACHTANG.MAKM AND KHUYENMAI.MAKM= DANHSACHMUA.MAKM ";

        ArrayList<XuLi.KMSanPham> kmList = new ArrayList<>();
        try {
            PreparedStatement ps = MiniSupermarket.DAL.DALDuLieu.con.prepareStatement(url);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                KMSanPham km1 = new KMSanPham();
                km1.setMakhuyenmai(rs.getString("MAKM"));
                km1.setTenkhuyenmai(rs.getString("TENKM"));
                km1.setNgaybd(rs.getDate("NGBD"));
                km1.setNgaykt(rs.getDate("NGKT"));
                km1.setMahh(rs.getString("MAHHMUA"));
                km1.setMalh(rs.getString("MALOMUA"));
                km1.setMahh(rs.getString("MAHHTANG"));
                km1.setMalh(rs.getString("MALOTANG"));
                kmList.add(km1);
            }

        } catch (SQLException e) {

            System.out.println();;
        }
        return kmList;

    }

    public static XuLi.KMSanPham timKiem(String maKM) {

        XuLi.KMSanPham km = new KMSanPham();
        try {
            Statement st = MiniSupermarket.DAL.DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery("Select * from KHUYENMAI where MAKM like N'%" + maKM + "%'");
            if (rs.next()) {
                km.setMakhuyenmai(rs.getString(1));
                km.setTenkhuyenmai(rs.getString(2));
                km.setNgaybd(rs.getDate(3));
                km.setNgaykt(rs.getDate(4));
                return km;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
