/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.CTDatHang;
import XuLi.CTPhieuNhap;
import XuLi.CTPhieuXuat;
import XuLi.CTYCHangHoa;
import XuLi.ChiNhanh;
import XuLi.HangCungCap;
import XuLi.HangHoa;
import XuLi.Kho;
import XuLi.LoHang;
import XuLi.PhieuNhap;
import XuLi.PhieuXuat;
import XuLi.SanPham;
import XuLi.TrangThai;
import XuLi.YCDatHang;
import XuLi.YCHangHoa;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author admin
 */
public class KhoDAL {
    public Vector <Kho> LayTTKho() {
        Vector <Kho> ttkho = new Vector<>();
            try {
                String sql = "SELECT * "
                        + "FROM KHO";
                Statement st = DALDuLieu.con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {                
                    Kho kho = new Kho();
                    kho.setMaKho(rs.getString("MAKHO"));
                    kho.setDiachiKho(rs.getNString("DIACHI"));
                    ChiNhanh cn = new ChiNhanh();
                    cn.setMaCN(rs.getString("MACN"));
                    kho.setChinhanhql(cn);
                    kho.setTrangthai(LayTTTT(new TrangThai(
                            rs.getString("MATT"), 
                            rs.getString("MATT")))
                        );
                    ttkho.add(kho);
                }
                rs.close();
                for (int i = 0; i < ttkho.size(); i++) {
                    sql = "SELECT * "
                            + "FROM KHO_SDT "
                            + "WHERE MAKHO = '" + ttkho.get(i).getMaKho() + "'";
                    rs = st.executeQuery(sql);
                    ArrayList <String> sdt = new ArrayList<>();
                    while (rs.next()) {
                        sdt.add(rs.getString("SDT"));
                    }
                    ttkho.get(i).setSdtKho(sdt);
                }
                rs.close();
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTKho() " + e);
            } finally {
            }
        return ttkho;
    }
    public Vector <Kho> TimKho(String s) {
        Vector <Kho> ttkho = new Vector<>();
            try {
                ttkho = SDT_Kho(s);
                int n = ttkho.size();
                for (int i = 0; i < n;i++){
                    String sql = "SELECT * "
                            + "FROM KHO "
                            + "WHERE MAKHO = '" + ttkho.get(i).getMaKho() + "'";
                    Statement st = DALDuLieu.con.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    while (rs.next()) {    
                        Kho k = ttkho.get(i);
                        k.setMaKho(rs.getString("MAKHO"));
                        k.setDiachiKho(rs.getNString("DIACHI"));
                        k.setChinhanhql(LayTTChiNhanh(rs.getString("MACN")));
                        k.setTrangthai(LayTTTT(new TrangThai(
                                rs.getString("MATT"), 
                                rs.getString("MATT")))
                            );
                    }
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.KhoDAL.TimKho() " + e);
            } finally {
            }
        return ttkho;
    }
    public TrangThai LayTTTT(TrangThai tt) {
        TrangThai trangthai = new TrangThai();
        try {
            String sql = "SELECT * "
                    + "FROM TRANGTHAI "
                    + "WHERE MATT = '" + tt.getMatrangthai() + "' "
                    + "OR TENTT = N'" + tt.getTentrangthai() + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()){
                trangthai.setMatrangthai(rs.getString("MATT"));
                trangthai.setTentrangthai(rs.getString("TENTT"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTTT() " + e);
        } finally {
        }
        return trangthai;
    }
    public boolean ThemTTKho(Kho kho) {
        boolean result = false;
        try {
            String sql = "INSERT INTO KHO VALUES (?, ?, ?, ?)";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, kho.getMaKho());
            pre.setString(2, kho.getDiachi());
            TrangThai trangThai = LayTTTT(kho.getTrangthai());
            pre.setString(3, trangThai.getMatrangthai());
            pre.setString(4, kho.getChinhanhql().getMaCN());
            sql = "INSERT INTO KHO_SDT VALUES (?, ?)";
            PreparedStatement pre0 = DALDuLieu.con.prepareStatement(sql);
            pre0.setString(1, kho.getMaKho());
            pre0.setString(2, kho.getSdtKho().get(0));
            if (pre.executeUpdate() >= 1 && pre0.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
            pre0.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.ThemTTKho() " + e);
        } finally {
        }
        return result;
    }
    public boolean KTKho(String MaKho) {
        boolean result = false;
        try {
            DALDuLieu.connectDB();
            String sql = "SELECT * "
                    + "FROM KHO "
                    + "WHERE MAKHO = '" + MaKho + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()){
                result = true;
            }
            rs.close();
        } catch (Exception e){
            System.out.println("MiniSupermarket.DAL.KhoDAL.KTKho() " + e);
        } finally {
            System.out.println("Kiểm Tra Kho " + result);
        }
        return result;
    }
    public String KTChiNhanh(String MaCN) {
        String result = "KHÔNG";
        try {
            String sql = "SELECT * "
                    + "FROM CHINHANH "
                    + "WHERE MACN = '" + MaCN + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()){
                TrangThai trangthai =  LayTTTT(new TrangThai(rs.getString("MATT"), rs.getString("MATT")));
                result = trangthai.getTentrangthai();
            }
        } catch (Exception e){
            System.out.println("MiniSupermarket.DAL.KhoDAL.KTChiNhanh() " + e);
        } finally {
            System.out.println("Kiểm Tra Chi Nhánh " + result);
        }
        return result;
    }
    public ChiNhanh LayTTChiNhanh(String macn) {
        ChiNhanh chinhanh = new ChiNhanh();
        try {
            String sql = "SELECT * "
                    + "FROM CHINHANH "
                    + "WHERE MACN = '" + macn + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                chinhanh.setDiachiCN(rs.getNString("DIACHI"));
                chinhanh.setMaCN(rs.getString("MACN"));
                chinhanh.setTenchinhanh(rs.getNString("TENCN"));
                TrangThai trangthai = LayTTTT(new TrangThai(rs.getString("MATT"), rs.getString("MATT")));
                chinhanh.setTrangThai(trangthai);
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTChiNhanh() " + e);
        } finally {
        }
        return chinhanh;
    }
    private boolean LaMaKho(String s) {
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM KHO_SDT "
                    + "WHERE MAKHO = '" + s + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()){
                result = true;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LaMaKho() " + e);
        } finally {
        }
        return result;
    }
    public Vector <Kho> SDT_Kho(String str) {
        Vector <Kho> sdtkho = new Vector<>();
        try {
            if (LaMaKho(str)){
                String sql = "SELECT * "
                        + "FROM KHO_SDT "
                        + "WHERE MAKHO = '" + str + "'";
                Statement st = DALDuLieu.con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                ArrayList <String> a = new ArrayList<>();
                while (rs.next()) {                    
                    a.add(rs.getString("SDT"));
                }
                rs.close();
                Kho k = new Kho();
                k.setMaKho(str);
                k.setSdtKho(a);
                sdtkho.add(k);
            } else {
                String sql = "SELECT * "
                        + "FROM KHO_SDT "
                        + "WHERE SDT = '" + str + "'";
                Statement st = DALDuLieu.con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                ArrayList <String> s = new ArrayList<>();
                while (rs.next()) {                    
                    if (!s.contains(rs.getString("MAKHO"))){
                        s.add(rs.getString("MAKHO"));
                    }
                }
                rs.close();
                for (int i = 0 ;i < s.size();i++){
                    String sql1 = "SELECT * "
                            + "FROM KHO_SDT "
                            + "WHERE MAKHO = '" + s.get(i) + "'";
                    Statement st1 = DALDuLieu.con.createStatement();
                    ResultSet rs1 = st1.executeQuery(sql1);
                    ArrayList <String> a = new ArrayList<>();
                    while (rs1.next()) {                    
                        a.add(rs1.getString("SDT"));
                    }
                    Kho k = new Kho();
                    k.setSdtKho(a);
                    k.setMaKho(s.get(i));
                    sdtkho.add(k);
                    rs.close();
                }
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.SDT_Kho() " + e);
        } finally {
        }
        return sdtkho;
    }
    public boolean CapNhatTT(Kho kho) {
        boolean result = false;
        try {
            String sql = "UPDATE KHO "
                    + "SET DIACHI = ?, "
                    + "MATT = ?, "
                    + "MACN = ? "
                    + "WHERE MAKHO = ?";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, kho.getDiachiKho());
            pre.setString(2, kho.getTrangthai().getMatrangthai());
            pre.setString(3, kho.getChinhanhql().getMaCN());
            System.out.println("MiniSupermarket.DAL.KhoDAL.CapNhatTT() " + kho.getMaKho());
            pre.setString(4, kho.getMaKho());
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.CapNhatTT() " + e);
        } finally {
        }
        return result;
    }
    public Vector <SanPham> LayTTKhoHT(String makho){
        Vector <SanPham> khoht = new Vector<>();
        HangHoa hanghoa = new HangHoa();
        try {
            LoHang lh = LayTTLoHang(makho);
            String sql = "SELECT * "
                    + "FROM DSTHEOLO "
                    + "WHERE MALO = '" + lh.getMalohang() + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {                
                SanPham sp = new SanPham();
                sp.setMahh(rs.getString("MAHH"));
                sp.setMalohang(rs.getString("MALO"));
                sp.setGianhap(rs.getInt("GIANHAP"));
                sp.setNgaynhap(rs.getDate("NGAYNHAP"));
                sp.setHsd(rs.getDate("HSD"));
                sp.setSlkho(rs.getFloat("SLKHO"));
                sp.setGianhap(rs.getInt("GIANHAP"));
                khoht.add(sp);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTKhoHT() " + e);
        } finally {
        }
        return khoht;
    }
    public LoHang LayTTLoHang(String MaKho) {
        LoHang lh = new LoHang();
        try {
            String sql = "SELECT * "
                    + "FROM LOHANG "
                    + "WHERE MAKHO = '" + MaKho + "'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()){
                lh.setMakho(MaKho);
                lh.setMadoitac(rs.getString("MADOITAC"));
                lh.setMalohang(rs.getString("MALO"));
                lh.setTenlohang(rs.getNString("TENLO"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTLoHang() " + e);
        } finally {
        }
        return lh;
    }
    public boolean ThemSDTKho(String makho, String sdt){
        boolean result = false;
        try {
            String sql = "INSERT INTO KHO_SDT VALUES (?, ?)";
            PreparedStatement pre = DALDuLieu.con.prepareCall(sql);
            pre.setString(1, makho);
            pre.setString(2, sdt);
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.ThemSDTKho() " + e);
        }
        return result;
    }
    public boolean KTSDT(String makho, String sdt){
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM KHO_SDT "
                    + "WHERE MAKHO = '" + makho + "' "
                    + "AND SDT = '" + sdt + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            if (rs.next()){
                result = true;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.KTSDT() " + e);
        }
        return result;
    }
    public boolean XoaSDT(String makho, String sdt){
        boolean result = false;
        try {
            String sql = "DELETE KHO_SDT WHERE MAKHO = ? AND SDT = ?";
            PreparedStatement pre = DALDuLieu.con.prepareCall(sql);
            pre.setString(1, makho);
            pre.setString(2, sdt);
            if (pre.executeUpdate() >= 1){
                result = true;
            }
            pre.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.XoaSDT() " + e);
        }
        return result;
    }
    public Vector <LoHang> LayTTLoHang() {
        Vector <LoHang> lh = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM LOHANG ";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()){
                LoHang lo = new LoHang();
                lo.setMakho(rs.getString("MAKHO"));
                lo.setMadoitac(rs.getString("MADOITAC"));
                lo.setMalohang(rs.getString("MALO"));
                lo.setTenlohang(rs.getString("TENLO"));
                lh.add(lo);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayTTLoHang() " + e);
        } finally {
        }
        return lh;
    }
    public Vector <SanPham> TTSPKho(String str){
        Vector <SanPham> sp = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM DSTHEOLO "
                    + "WHERE MALO = '" + str + "' "
                    + "OR MAHH = '" + str + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while (rs.next()) {                
                SanPham sanpham = new SanPham();
                sanpham.setMalohang(rs.getString("MALO"));
                sanpham.setMahh(rs.getString("MAHH"));
                sanpham.setNgaynhap(rs.getDate("NGAYNHAP"));
                sanpham.setHsd(rs.getDate("HSD"));
                sanpham.setSlkho(rs.getFloat("SLKHO"));
                sanpham.setGianhap(rs.getInt("GIANHAP"));
                sanpham.setNsx(rs.getDate("NSX"));
                sp.add(sanpham);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TTSPKho() " + e);
        }
        return sp;
    }
    public Vector <LoHang> TimTTLoHang(String str) {
        Vector <LoHang> lohang = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM LOHANG "
                    + "WHERE MAKHO = '" + str + "' "
                    + "OR MALO = '" + str + "' "
                    + "OR MADOITAC = '" + str + "' "
                    + "OR TENLO LIKE N'%" + str + "%'";
            Statement st = DALDuLieu.con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()){
                LoHang lh = new LoHang();
                lh.setMakho(rs.getString("MAKHO"));
                lh.setMadoitac(rs.getString("MADOITAC"));
                lh.setMalohang(rs.getString("MALO"));
                lh.setTenlohang(rs.getNString("TENLO"));
                lohang.add(lh);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TimTTLoHang() " + e);
        } finally {
        }
        return lohang;
    }
    public ArrayList<CTDatHang> LayDuLieuCTDH(String madh) {
        ArrayList <CTDatHang> dsctdh=new ArrayList<CTDatHang>();
        try {
            String sql = "SELECT * "
                    + "FROM CTDATHANG "
                    + "WHERE MADH = '" + madh + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                CTDatHang ctdh = new CTDatHang();
                ctdh.setMayeucau(rs.getString("MADH"));
                ctdh.setMahh(rs.getString("MAHH"));
                ctdh.setSoluong(rs.getFloat("SOLUONG"));
                ctdh.setGiadenghi(rs.getInt("GIADENGHI"));
                dsctdh.add(ctdh);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuCTDH() " + e);
        }
        return dsctdh;
    }
    public CTDatHang LayDuLieuCTDH(String madh, String mahh) {
        CTDatHang ctdh = new CTDatHang();
        try {
            String sql = "SELECT * "
                    + "FROM CTDATHANG "
                    + "WHERE MADH = '" + madh + "' "
                    + "AND MAHH = '" + mahh + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                ctdh.setMayeucau(rs.getString("MADH"));
                ctdh.setMahh(rs.getString("MAHH"));
                ctdh.setSoluong(rs.getFloat("SOLUONG"));
                ctdh.setGiadenghi(rs.getInt("GIADENGHI"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuCTDH() " + e);
        }
        return ctdh;
    }
    public ArrayList<YCDatHang> LayDuLieuDH(String makho) {
        ArrayList <YCDatHang> dsdh = new ArrayList<YCDatHang>();
        try {
            String sql = "SELECT * "
                    + "FROM DATHANG "
                    + "WHERE MAKHO = '" + makho + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                YCDatHang dh = new YCDatHang();
                dh.setMayeucauDH(rs.getString("MADH"));
                dh.setManhanvienYCDH(rs.getString("MANV"));
                dh.setManguonchuyenYCDH(rs.getString("MADT"));
                dh.setSomnhatYCDH(rs.getDate("SOMNHAT"));
                dh.setTrenhatYCDH(rs.getDate("TRENHAT"));
                dsdh.add(dh);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuDH() " + e);
        }
        return dsdh;
    }
    public ArrayList<YCDatHang> TimDH(String str) {
        ArrayList <YCDatHang> dsdh=new ArrayList<YCDatHang>();
        try {
            String sql = "SELECT * "
                    + "FROM DATHANG "
                    + "WHERE MAKHO = '" + str + "' "
                    + "OR MADH = '" + str + "' "
                    + "OR MANV = '" + str + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                YCDatHang dh = new YCDatHang();
                dh.setMayeucauDH(rs.getString("MADH"));
                dh.setManhanvienYCDH(rs.getString("MANV"));
                dh.setManguonchuyenYCDH(rs.getString("MADT"));
                dh.setSomnhatYCDH(rs.getDate("SOMNHAT"));
                dh.setTrenhatYCDH(rs.getDate("TRENHAT"));
                dsdh.add(dh);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuDH() " + e);
        }
        return dsdh;
    }
    public ArrayList <PhieuXuat> LayDuLieuXuatHang() {
        ArrayList<PhieuXuat> dspx = new ArrayList<>();
        try {
            String sql = "SELECT * "
                    + "FROM XUATHANG ";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                PhieuXuat px = new PhieuXuat();
                px.setMaphieu(rs.getString("MAXUAT"));
                px.setManv(rs.getString("MANV"));
                px.setNgayphieu(rs.getDate("NGAYXUAT"));
                dspx.add(px); 
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuXuatHang() " + e);
        }
        return dspx;
    }
    public ArrayList <PhieuXuat> TimTTXuatHang(String string) {
        ArrayList<PhieuXuat> dspx = new ArrayList<>();
        try {
            String sql = "SELECT * "
                    + "FROM XUATHANG "
                    + "WHERE MAXUAT = '" + string + "' "
                    + "OR MANV = '" + string + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                PhieuXuat px = new PhieuXuat();
                px.setMaphieu(rs.getString("MAXUAT"));
                px.setManv(rs.getString("MANV"));
                px.setNgayphieu(rs.getDate("NGAYXUAT"));
                dspx.add(px); 
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TimTTXuatHang() " + e);
        }
        return dspx;
    }
    public ArrayList <CTPhieuXuat> LayDuLieuCTXuatHang(String maxh) {
        ArrayList <CTPhieuXuat> ctpx = new ArrayList<>();
        try {
            String sql = "SELECT * "
                + "FROM CTXUATHANG "
                + "WHERE MAXUAT = '"+maxh+"'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                CTPhieuXuat ctx = new CTPhieuXuat();
                ctx.setMalh(rs.getString("MALO"));
                ctx.setMahh(rs.getString("MAHH"));
                ctx.setSoluong(rs.getFloat("SOLUONG"));
                ctpx.add(ctx); 
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuCTXuatHang() " + e);
        }
        return ctpx;
    }
    public ArrayList <CTYCHangHoa> LayDuLieuCTYCHH(String mayc) {
        ArrayList <CTYCHangHoa> CTYC = new ArrayList<CTYCHangHoa>();
        try {
            String sql = "SELECT * "
                    + "FROM CTYEUCAUHH "
                    + "WHERE MAYC = '"+ mayc + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                CTYCHangHoa ctyc = new CTYCHangHoa();
                ctyc.setMayeucau(rs.getString("MAYC"));
                ctyc.setMahh(rs.getString("MAHH"));
                ctyc.setSoluong(rs.getFloat("SOLUONG"));
                CTYC.add(ctyc);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuCTYCHH() " + e);
        }
        return CTYC;
    }
    public HangHoa TimHH(String mahh) {
        HangHoa hh= new HangHoa();
        try {
            String sql = "SELECT * "
                + "FROM HANGHOA "
                + "WHERE MAHH = '"+mahh+"'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            if (rs.next()){
                hh.setMahanghoa(rs.getString("MAHH"));
                hh.setTenhanghoa(rs.getNString("TENHANG"));
                hh.setNuocsx(rs.getNString("NUOCSX"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TimHH() " + e);
        }
        return hh;
    }
    public ArrayList <YCHangHoa> LayDuLieuYCHH() {
        ArrayList <YCHangHoa> YCHH = new ArrayList <YCHangHoa>();
        try {
            String sql = "SELECT * "
                    + "FROM YEUCAUHH";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                YCHangHoa yc = new YCHangHoa();
                yc.setMayeucauNB(rs.getString("MAYC"));
                yc.setManguonchuyenNB(rs.getString("MAKHO"));
                yc.setManguonycNB(rs.getString("MACN"));
                yc.setManhanvienYCNB(rs.getString("MANV"));
                yc.setManvxacnhan(rs.getString("MANVXACNHAN"));
                yc.setManvhoanthanh(rs.getString("MANVHOANTHANH"));
                yc.setSomnhatYCNB(rs.getDate("SOMNHAT"));
                yc.setTrenhatYCNB(rs.getDate("TRENHAT"));
                YCHH.add(yc);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuYCHH() " + e);
        }
        return YCHH;
    }
    public ArrayList <YCHangHoa> TimYCHH(String str) {
        ArrayList <YCHangHoa> YCHH = new ArrayList <YCHangHoa>();
        try {
            String sql = "SELECT * "
                    + "FROM YEUCAUHH "
                    + "WHERE MAYC = '" + str + "' "
                    + "OR MANVXACNHAN = '" + str + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                YCHangHoa yc = new YCHangHoa();
                yc.setMayeucauNB(rs.getString("MAYC"));
                yc.setManvxacnhan(rs.getString("MANVXACNHAN"));
                yc.setSomnhatYCNB(rs.getDate("SOMNHAT"));
                yc.setTrenhatYCNB(rs.getDate("TRENHAT"));
                YCHH.add(yc);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuYCHH() " + e);
        }
        return YCHH;
    }
    public File ChonFileNhap() {
        JFileChooser chooser = new JFileChooser();
        int i = chooser.showOpenDialog(chooser);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                return file;
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.DALDuLieu.ChonFileXuat(): "+e); 
            }
        }
        return null;
    }
    public File ChonFileXuat() {
        JFileChooser chooser = new JFileChooser();
        int i = chooser.showSaveDialog(chooser);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                boolean check = file.getPath().contains(".xls") || file.getPath().contains(".");
                File fileSave = new File(check ? file+"" : file + ".xls");
                fileSave.getParentFile().mkdirs();
                return fileSave;
            } catch (Exception e) {
                System.out.println("MiniSupermarket.DAL.DALDuLieu.ChonFileXuat(): "+e); 
            }
        }
        return null;
    }
    public String XuatExcel(JTable table, String tenbang) {
        try {
            File file = ChonFileXuat();
            String excelFilePath = file.getPath();
            if (excelFilePath.endsWith("xlsx")) {
                return XuatExcelXLSX(file, table, tenbang);
	    } else 
                if (excelFilePath.endsWith("xls")) {
                    return XuatExcelXLS(file, table, tenbang);
                } else {
                    return ("Bạn chọn sai file rồi!");
                }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcel(): " + e);
            return "Lỗi khi xuất!";
        }
    }
    public String XuatExcelXLSX(File file, JTable table, String tenbang) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(tenbang);
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        Cell cell;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int n = model.getColumnCount();
        int m = model.getRowCount();
        int soDong = 0;
        Row row = sheet.createRow(soDong);
        for (int i = 0; i < n; i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        for (int i = 0; i < m; i++) {
            soDong++;
            row = sheet.createRow(soDong);
            for (int j = 0; j < n; j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i,j).toString());
            }
        }
        try {
            OutputStream stream = new FileOutputStream(file);
            workbook.write(stream);
            stream.close();
            workbook.close();
            return "Xuất thành công! Địa chỉ file: "+file.getPath();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcelXLSX(): "+e);
        }
        return "";
    }
    public String XuatExcelXLS(File file,  JTable table, String tenbang) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(tenbang);
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        Cell cell;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int n = model.getColumnCount();
        int m = model.getRowCount();
        int soDong = 0;
        Row row = sheet.createRow(soDong);
        for (int i = 0; i < n; i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        for (int i = 0; i < m; i++) {
            soDong++;
            row = sheet.createRow(soDong);
            for (int j = 0; j < n; j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i,j).toString());
            }
        }
        try {
            OutputStream stream = new FileOutputStream(file);
            workbook.write(stream);
            stream.close();
            workbook.close();
            return "Xuất thành công! Địa chỉ file: "+file.getPath();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.XuatExcelXLS(): "+e);
        }
        return "";
    }
    private Object getCellValue(Cell cell) {
        CellType ct = cell.getCellType();
        if (ct == CellType.STRING) 
            return cell.getStringCellValue();
        if (ct == CellType.BOOLEAN) 
            return cell.getBooleanCellValue();
        if (ct == CellType.NUMERIC) 
            return cell.getNumericCellValue();

        return null;
    }
    public ArrayList<ArrayList<String>> NhapExcel() throws IOException {
        ArrayList<ArrayList<String>> NoiDung = new ArrayList<>();
        try {
            File file = ChonFileNhap();
            String excelFilePath = file.getPath();
            FileInputStream inputstream = new FileInputStream(file);
            if (excelFilePath.endsWith("xlsx") || excelFilePath.endsWith("xls")) {
                Workbook workbook = getWorkbook(inputstream,file.getPath());
                Sheet firstSheet = workbook.getSheetAt(0);
                Iterator<Row> iterator = firstSheet.iterator();
                Row nextRow = iterator.next();
                while (iterator.hasNext()) {
                    nextRow = iterator.next();
                    Iterator<Cell> cellIterator = nextRow.cellIterator();
                    ArrayList<String> NoiDung1Dong = new ArrayList<>();
                    while (cellIterator.hasNext()) {
                        Cell nextCell = cellIterator.next();
                        int columnIndex = nextCell.getColumnIndex();
                        NoiDung1Dong.add(getCellValue(nextCell).toString());
                        System.out.println(getCellValue(nextCell).toString());
                    }
                    NoiDung.add(NoiDung1Dong);
                }
                workbook.close();
                return NoiDung;
            }
	    else {
	        return null;
	    }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.DALDuLieu.NhapExcel(): "+e);
            return null;
        }
        
    }
    private Workbook getWorkbook(FileInputStream inputstream,String excelFilePath)
            throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputstream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputstream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }
    public ArrayList <LoHang> LayDuLieuLoHang() {
        ArrayList <LoHang> arlh = new ArrayList<>();
        try {
            String sql = "SELECT * "
                    + "FROM LOHANG";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rs.next()){
                LoHang lh = new LoHang();
                lh.setMadoitac(rs.getString("MADOITAC"));
                lh.setMalohang(rs.getString("MALO"));
                lh.setTenlohang(rs.getNString("TENLO"));
                lh.setMakho(rs.getString("MAKHO"));
                arlh.add(lh); 
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuLoHang() " + e);
        }
        return arlh;
    }
    public ArrayList <PhieuNhap> LayDuLieuNH2() {
        ArrayList <PhieuNhap> dsnh = new ArrayList<>();
        try {
            String sql = "SELECT * "
                    + "FROM PHIEUNHAP";
            ResultSet rsnh = DALDuLieu.con.createStatement().executeQuery(sql);
            while(rsnh.next()){
                PhieuNhap nh = new PhieuNhap();
                nh.setMaphieu(rsnh.getString("MANHAP"));
                nh.setMalo(rsnh.getString("MALO"));
                nh.setManv(rsnh.getString("MANV"));
                nh.setNgayphieu(rsnh.getDate("NGAYNHAP"));
                dsnh.add(nh);
            }
            rsnh.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayDuLieuNH2() " + e);
        }
        return dsnh;
    }
    public boolean ThemLoHang(LoHang lh) {
        boolean result = false;
        String sql= "INSERT INTO LOHANG VALUES (?,?,?,?)";
        try{
            PreparedStatement ps = DALDuLieu.con.prepareStatement(sql);
            ps.setString(1,lh.getMalohang());
            ps.setNString(2,lh.getTenlohang());
            ps.setString(3,lh.getMadoitac());
            ps.setString(4,lh.getMakho());
            if(ps.executeUpdate() >= 1){
                result = true;
            }
            ps.close();
        } catch (Exception ex){
            System.out.println("MiniSupermarket.DAL.LoHangDAL.ThemLoHang() " + ex);
        }
        return result ;
    }
    public boolean ThemPhieuNhap(PhieuNhap nh){
        boolean result = false;
        String sql = "INSERT INTO PHIEUNHAP VALUES (?, ?, ?, ?)";
        try{
            PreparedStatement ps = DALDuLieu.con.prepareStatement(sql);
            ps.setString(1,nh.getMaphieu());
            ps.setString(2, nh.getManv());
            ps.setString(3,nh.getMalo());
            ps.setDate(4, (Date) nh.getNgayphieu());
            if(ps.executeUpdate() >= 1){
                result = true;
            }
            ps.close();
        }catch(Exception ex){
            System.out.println("MiniSupermarket.DAL.NhapHangDAL.ThemPhieuNhap() " + ex);
        }
        return result ;
    }
    public boolean KiemTraCCHH(HangCungCap Cungc) {
        boolean result = false;
        try {
            String sql = "SELECT * "
                    + "FROM DANHSACHCUNGCAP "
                    + "WHERE MADOITAC = '" + Cungc.getMadoitac() + "' "
                    + "AND MAHH = '" + Cungc.getMahh() + "'";
            ResultSet ds = DALDuLieu.con.createStatement().executeQuery(sql);
            result = ds.next();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.DaCoCungCap() " + e);
        }
      return result;      
    }
    public boolean ThemCTPhieuNhap(CTPhieuNhap nh){
        boolean result = false;
        try{
            String sql = "INSERT INTO CTPHIEUNHAP VALUES (?, ?, ?, ?, ?)";
            PreparedStatement ps = DALDuLieu.con.prepareStatement(sql);
            ps.setString(1,nh.getMaphieu());
            ps.setString(2, nh.getMahh());
            ps.setFloat(3,nh.getSoluong());
            ps.setInt(4, nh.getGianhap());
            ps.setInt(5, nh.getGiadukien());
            if(ps.executeUpdate() >= 1){
                result = true;
            }
            ps.close();
        }catch(Exception ex){
            System.out.println("MiniSupermarket.DAL.NhapHangDAL.ThemPhieuNhap() " + ex);
        }
        return result ;
    }      
    public boolean ThemSanPham(SanPham sp){
        boolean result = false;
        try {
            String sql = "INSERT INTO DSTHEOLO ( VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.ThemSanPham() " + e);
        }
        return result;
    }
    public boolean XoaDN(PhieuNhap pn){
        boolean result = false;
        try {
            String sql = "DELETE CTPHIEUNHAP "
                    + "WHERE MANHAP = ?";
            PreparedStatement pre = DALDuLieu.con.prepareStatement(sql);
            pre.setString(1, pn.getMaphieu());
            sql = "DELETE PHIEUNHAP "
                    + "WHERE MALO = ? "
                    + "AND MANHAP = ?";
            PreparedStatement pre0 = DALDuLieu.con.prepareStatement(sql);
            pre0.setString(1, pn.getMalo());
            pre0.setString(2, pn.getMaphieu());
            sql = "DELETE DSTHEOLO "
                    + "WHERE MALO = ?";
            PreparedStatement pre1 = DALDuLieu.con.prepareStatement(sql);
            pre1.setString(1, pn.getMalo());
            sql = "DELETE LOHANG "
                    + "WHERE MALO = ?";
            PreparedStatement pre2 = DALDuLieu.con.prepareStatement(sql);
            pre2.setString(1, pn.getMalo());
            if (pre.executeUpdate() >= 1 && 
                    pre0.executeUpdate()>= 1 && 
                    pre1.executeUpdate()>= 1 && 
                    pre2.executeUpdate()>= 1){
                result = true;
            }
            pre.close();
            pre0.close();
            pre1.close();
            pre2.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.XoaDN() " + e);
        }
        return result;
    }
    public Vector <PhieuNhap> TimDN(String str){
        Vector <PhieuNhap> pnpn = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM PHIEUNHAP "
                    + "WHERE MANHAP = '" + str + "' "
                    + "OR MALO = '" + str + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while (rs.next()){
                PhieuNhap p = new PhieuNhap();
                p.setMalo(rs.getString("MALO"));
                p.setMaphieu(rs.getString("MANHAP"));
                p.setManv(rs.getString("MANV"));
                p.setNgayphieu(rs.getDate("NGAYNHAP"));
                pnpn.add(p);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TimDN() " + e);
        }
        return pnpn;
    }
    public SanPham LaySanPham(SanPham sp){
        try {
            String sql = "SELECT TOP 1 * "
                + "FROM DSTHEOLO "
                + "WHERE MAHH = '"+ sp.getMahh() + "' "
                + "AND HSD > GETDATE() "
                + "AND SLKHO > 0 "
                + " ORDER BY HSD";
            ResultSet dssp = DALDuLieu.con.createStatement().executeQuery(sql);
            if (dssp.next()){
                sp.setMalohang(dssp.getString("MALO"));
                sp.setSlkho(dssp.getFloat("SLKHO"));
            }
            dssp.close();
        } catch (Exception e){
            System.out.println("MiniSupermarket.DAL.KhoDAL.LaySanPham() " + e);
        }
        return sp;
   }
    public boolean ThemPhieuXuat(PhieuXuat px){
        boolean result = false;
        String sql = "INSERT INTO XUATHANG VALUES (?, ?, ?)";
        try{
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, px.getMaphieu());
            pr.setString(2, px.getManv());
            pr.setDate(3, (Date) px.getNgayphieu());
            if(pr.executeUpdate() >= 1){
                result = true;
            }
            pr.close();
        }catch(Exception ex){
            System.out.println("MiniSupermarket.DAL.XuatHangDAL.ThemPhieuXuat() " + ex);
        }
        return result;
    }
    public boolean ThemCTPhieuXuat(CTPhieuXuat ctpx){
        boolean result = false;
        String sql = "INSERT INTO CTXUATHANG VALUES (?, ?, ?, ?)";
        try{
            PreparedStatement pr= DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, ctpx.getMaphieu());
            pr.setString(2, ctpx.getMalh());
            pr.setString(3,ctpx.getMahh());
            pr.setFloat(4, ctpx.getSoluong());
            if(pr.executeUpdate() >= 1){
                result = true;
            }
            pr.close();
        }catch(Exception e){
            System.out.println("MiniSupermarket.DAL.KhoDAL.ThemCTPhieuXuat() " + e);
        }
        return result;
    }
    public Vector <CTPhieuNhap> LayCTPhieuNhap(String manhap){
        Vector <CTPhieuNhap> ctpn = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM CTPHIEUNHAP "
                    + "WHERE MANHAP = '" + manhap + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while (rs.next()) {                
                CTPhieuNhap c = new CTPhieuNhap();
                c.setSoluong(rs.getFloat("SOLUONG"));
                c.setGianhap(rs.getInt("GIANHAP"));
                c.setMahh(rs.getString("MAHH"));
                ctpn.add(c);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.LayCTPhieuNhap() " + e);
        }
        return ctpn;
    }
    public Vector <CTPhieuNhap> TimCTPhieuNhap(String str, String maphieu){
        Vector <CTPhieuNhap> ct = new Vector<>();
        try {
            String sql = "SELECT * "
                    + "FROM CTPHIEUNHAP "
                    + "WHERE MAHH = '" + str + "' "
                    + "AND MANHAP = '" + maphieu + "'";
            ResultSet rs = DALDuLieu.con.createStatement().executeQuery(sql);
            while (rs.next()) {                
                CTPhieuNhap c = new CTPhieuNhap();
                c.setMahh(rs.getString("MAHH"));
                c.setGianhap(rs.getInt("GIANHAP"));
                c.setSoluong(rs.getFloat("SOLUONG"));
                ct.add(c);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.DAL.KhoDAL.TimCTPhieuXuat() " + e);
        }
        return ct;
    }
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    public boolean ThemDH (YCDatHang dh){
        boolean result = false;
        try{
            String sql = "INSERT INTO DATHANG VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, dh.getMayeucauDH());
            pr.setString(2, dh.getManhanvienYCDH());
            pr.setString(3, dh.getManguonycDH());
            pr.setString(4, dh.getManguonchuyenYCDH());
            pr.setDate(5, (java.sql.Date) dh.getSomnhatYCDH());
            pr.setDate(6, (java.sql.Date) dh.getTrenhatYCDH());
            if(pr.executeUpdate() >= 1){
                result = true;
            }
            pr.close();
        } catch (Exception ex){
            System.out.println("MiniSupermarket.DAL.KhoDAL.ThemDH() " + ex);
        }    
        return result;
    }
    public boolean ThemCTDH(CTDatHang ctdh) {
        boolean result = false;
        try{    
            String sql = "INSERT INTO CTDATHANG VALUES (?, ?, ?, ?)";
            PreparedStatement pr = DALDuLieu.con.prepareStatement(sql);
            pr.setString(1, ctdh.getMayeucau());
            pr.setString(2, ctdh.getMahh());
            pr.setFloat(3, ctdh.getSoluong());
            pr.setInt(4,ctdh.getGiadenghi());
            if (pr.executeUpdate() >= 1){
                result = true;
            }
            pr.close();
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.DAL.DatHangDAL.ThemCTYCHH()" + ex);
        }    
        return result;
    }
}
