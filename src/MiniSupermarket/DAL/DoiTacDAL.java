/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.DAL;

import XuLi.DoiTac;
import XuLi.TrangThai;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class DoiTacDAL{ 
    public static boolean ThemDoiTac(DoiTac dt) throws SQLException{
        boolean flag = false;
        String sql=" Insert into DOITAC values (?,?,?,?,?,?,?)";
        PreparedStatement stDoiTac= DALDuLieu.con.prepareStatement(sql);
        stDoiTac.setString(1, dt.getMaDT());
        stDoiTac.setString(2, dt.getTenDT());
        stDoiTac.setNString(3, dt.getDiachiDT());
        stDoiTac.setDate(4,  dt.getNghoptac());
        stDoiTac.setDate(5,  dt.getHanhd());
        stDoiTac.setString(6, dt.getEmail());
        DALDuLieu.SoSanhDuLieuTT( dt.getTrangthai());
        stDoiTac.setString(7, dt.getTrangthai().getMatrangthai());
        if(stDoiTac.executeUpdate()>=1)
        {
            stDoiTac.close();
            return flag=true;
        }
        stDoiTac.close();
        return flag;
        
    }
    public static boolean ThemSDTDoiTac(DoiTac dt){
        try{
            String sql="Insert into DOITAC_SDT values (?,?)";
            PreparedStatement stDoiTac_sdt = DALDuLieu.con.prepareStatement(sql);
            stDoiTac_sdt.setString(1, dt.getMaDT());
            stDoiTac_sdt.setString(2,dt.getSdt().get(0
            ));
            if(stDoiTac_sdt.executeUpdate()>=1){
                stDoiTac_sdt.close();
                return true;
            }
            stDoiTac_sdt.close();
        }catch(SQLException ex){
            System.out.println("ThemSDT"+ex);
        }
        return false;
    }
    public static boolean SuaDoiTac(DoiTac dt){
        boolean result = false;
        try {
            String sql = "UPDATE DOITAC "
                    + "SET TENCTYDOITAC = ?, "
                    + "DIACHI = ?, "
                    + "NGAYHOPTAC = ?, "
                    + "HANHOPDONG = ?, "
                    + "EMAILDATHANG = ?, "
                    + "MATT= ? "
                    + "WHERE MADOITAC = ?";
            PreparedStatement stDoiTac = DALDuLieu.con.prepareStatement(sql);
            stDoiTac.setNString(1, dt.getTenDT());
            stDoiTac.setNString(2, dt.getDiachiDT());
            stDoiTac.setDate(3, dt.getNghoptac());
            stDoiTac.setDate(4, dt.getHanhd());
            stDoiTac.setString(5, dt.getEmail());
            DALDuLieu.SoSanhDuLieuTT( dt.getTrangthai());
            stDoiTac.setString(6, dt.getTrangthai().getMatrangthai());
            stDoiTac.setString(7, dt.getMaDT());
            sql = "UPDATE DOITAC_SDT "
                    + "SET SODT = ? "
                    + "WHERE MADOITAC = ? ";
            PreparedStatement pre0 = DALDuLieu.con.prepareStatement(sql);
            pre0.setString(1, dt.getSdt().get(0));
            pre0.setString(2,  dt.getMaDT());
            if (stDoiTac.executeUpdate() >= 1 && pre0.executeUpdate() >= 1){
                result = true;
            }
            stDoiTac.close();
            pre0.close();
        } catch (Exception e ) {
            System.out.println(e);
        }
        return result;
 }
    
    public static boolean DaCoDoiTac(String ma){
        boolean flag = false;
        try{        
            String sql ="SELECT * FROM DOITAC WHERE MADOITAC = '"+ma+"'";
            Statement stDoiTac =DALDuLieu.con.createStatement();
            ResultSet ds = stDoiTac.executeQuery(sql);
            flag = ds.next();
            stDoiTac.close();
      } catch (SQLException ex) {
            System.out.println(ex);
        } 
        return flag; 
    }      
}
