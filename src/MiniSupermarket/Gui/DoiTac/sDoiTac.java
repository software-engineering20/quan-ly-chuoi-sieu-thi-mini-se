/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.DoiTac;

import MiniSupermarket.BLL.DoiTacBLL;
import MiniSupermarket.Gui.sMain;
import MiniSupermarket.Gui.BanHang.sBanHang;
import MiniSupermarket.Gui.ChiNhanh.sChiNhanh;
import MiniSupermarket.Gui.HangHoa.sHangHoa;
import MiniSupermarket.Gui.HoaDon.sHoaDon;
import MiniSupermarket.Gui.KhachHang.sKhachHang;
import MiniSupermarket.Gui.Kho.sKho;
import MiniSupermarket.Gui.LuanChuyen.sYCLuanChuyen;
import MiniSupermarket.Gui.ThuChi.sThuChi;
import MiniSupermarket.Gui.sNhanVien.sNhanVien;
import XuLi.DoiTac;
import XuLi.TrangThai;
import java.awt.Color;
import java.awt.Font;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author admin
 */
public class sDoiTac extends javax.swing.JFrame {

    
    public sDoiTac() {
        initComponents();
        TaiDuLieu();
    }
    public void TaiDuLieu(){ 
        try{
            DefaultTableModel tb= new DefaultTableModel();
            tb.addColumn("Mã đối tác");
            tb.addColumn("Tên cty đối tác");
            tb.addColumn("Địa chỉ");
            tb.addColumn("Ngày hợp tác");
            tb.addColumn("Hạn hợp đồng");
            tb.addColumn("Số điện thoại");
            tb.addColumn("Email");
            tb.addColumn("Trạng Thái");
            DsDoiTac.setModel(tb);
            DsDoiTac.setFont(new Font("Times New Roman",Font.PLAIN, 10));  
            for(int i=0; i<DoiTacBLL.DuLieuDoiTac().size();i++)
            {
                DoiTac Dtac = DoiTacBLL.DuLieuDoiTac().get(i);
                String ma= Dtac.getMaDT();
                String ten= Dtac.getTenDT();
                String diachi=Dtac.getDiachiDT();                
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String ngayht  = formatter.format(Dtac.getNghoptac());
                String hanhd = formatter.format(Dtac.getHanhd());
                String sdt = Dtac.getSdt().size() > 0 ? Dtac.getSdt().get(0) : "";
                String email= Dtac.getEmail();
                String trangthai= Dtac.getTrangthai().getTentrangthai();
                Object[] row={ma,ten,diachi,ngayht,hanhd,sdt,email,trangthai};
                tb.addRow(row);
            }
        }catch (Exception e) {
            System.out.println(e);}
                
        
    }
    public boolean HopLeSDT(String s){ 
        Pattern p = Pattern.compile("([+]{1}[8]{1}[4]{1}[0-9]{9})|([0]{1}[0-9]{9})");
        Matcher m = p.matcher(s);
        return (m.matches());
    }
    public boolean HopLeEmail(String email){
        {                              
        Pattern pat = Pattern.compile("^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$");
        if (email == null)
            return false;
        return pat.matcher(email).matches();
        }
    }
    private void TimKiem() throws SQLException{
        setTable(jtTimKiem.getText());
    }
    private void setTable(String text) throws SQLException {
        DsDoiTac.setModel(DoiTacBLL.TaiBangTimKiemDT(text));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jtTimKiem = new JTextField();
        jtTimKiem.setBorder(new LineBorder(Color.white, 10, true));
        jButton11 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        DsDoiTac = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jtMaDT = new javax.swing.JTextField();
        jtTenDT = new javax.swing.JTextField();
        jtDiaChi = new javax.swing.JTextField();
        jtNgayHT = new javax.swing.JTextField();
        jtHanHD = new javax.swing.JTextField();
        jtSDT = new javax.swing.JTextField();
        jBAdd = new javax.swing.JButton();
        jBUpdate = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jtEmail = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jcbTT = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(100, 0));
        setResizable(false);
        setSize(new java.awt.Dimension(1080, 768));

        jPanel1.setBackground(new java.awt.Color(141, 238, 221));
        jPanel1.setPreferredSize(new java.awt.Dimension(1080, 768));

        jPanel2.setBackground(new java.awt.Color(146, 215, 220));
        jPanel2.setPreferredSize(new java.awt.Dimension(1080, 98));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnBanHang.png"))); // NOI18N
        jButton1.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnHangHoa.png"))); // NOI18N
        jButton2.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnHoaDon.png"))); // NOI18N
        jButton3.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnKho.png"))); // NOI18N
        jButton4.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnKhachHang.png"))); // NOI18N
        jButton5.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnNhanVien.png"))); // NOI18N
        jButton6.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnLuanChuyen.png"))); // NOI18N
        jButton7.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnThuChi.png"))); // NOI18N
        jButton8.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnChiNhanh.png"))); // NOI18N
        jButton9.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnDoiTac (1).png"))); // NOI18N
        jButton10.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel3.setBackground(new java.awt.Color(141, 238, 221));
        jPanel3.setPreferredSize(new java.awt.Dimension(1045, 65));

        jtTimKiem.setForeground(new java.awt.Color(204, 204, 204));
        jtTimKiem.setPreferredSize(new java.awt.Dimension(400, 36));
        jtTimKiem.setRequestFocusEnabled(true);
        jtTimKiem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtTimKiemMouseClicked(evt);
            }
        });

        jButton11.setBackground(new java.awt.Color(124, 205, 189));
        jButton11.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton11.setText("Tìm Kiếm");
        jButton11.setPreferredSize(new java.awt.Dimension(95, 55));
        jButton11.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton11.setBackground(new Color(156, 221, 229));
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton14.setBackground(new java.awt.Color(124, 205, 189));
        jButton14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton14.setText("Tải lại");
        jButton14.setPreferredSize(new java.awt.Dimension(95, 55));
        jButton14.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton14.setBackground(new Color(156, 221, 229));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jtTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        DsDoiTac.setFont(new java.awt.Font("Songti TC", 0, 5)); // NOI18N
        DsDoiTac.setPreferredSize(new java.awt.Dimension(644, 550));
        DsDoiTac.getTableHeader().setReorderingAllowed(false);
        DsDoiTac.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DsDoiTacMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(DsDoiTac);
        if (DsDoiTac.getColumnModel().getColumnCount() > 0) {
            DsDoiTac.getColumnModel().getColumn(0).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(1).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(1).setPreferredWidth(100);
            DsDoiTac.getColumnModel().getColumn(2).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(2).setPreferredWidth(60);
            DsDoiTac.getColumnModel().getColumn(3).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(3).setPreferredWidth(100);
            DsDoiTac.getColumnModel().getColumn(4).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(4).setPreferredWidth(100);
            DsDoiTac.getColumnModel().getColumn(5).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(6).setResizable(false);
            DsDoiTac.getColumnModel().getColumn(6).setPreferredWidth(60);
            DsDoiTac.getColumnModel().getColumn(7).setResizable(false);
        }

        jPanel5.setBackground(new java.awt.Color(207, 250, 253));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Mã Đối Tác");
        jLabel2.setPreferredSize(new java.awt.Dimension(130, 30));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setText("Tên Đối Tác");
        jLabel3.setPreferredSize(new java.awt.Dimension(130, 30));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setText("Địa Chỉ");
        jLabel4.setPreferredSize(new java.awt.Dimension(130, 30));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setText("Ngày Hợp Tác");
        jLabel5.setPreferredSize(new java.awt.Dimension(130, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("Hạn Hợp Đồng");
        jLabel6.setPreferredSize(new java.awt.Dimension(130, 30));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Số Điện Thoại");
        jLabel7.setPreferredSize(new java.awt.Dimension(130, 30));

        jtMaDT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtMaDT.setPreferredSize(new java.awt.Dimension(150, 30));
        jtMaDT.setBorder(new LineBorder(Color.white, 8, true));
        jtMaDT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtMaDTActionPerformed(evt);
            }
        });

        jtTenDT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtTenDT.setPreferredSize(new java.awt.Dimension(150, 30));
        jtTenDT.setBorder(new LineBorder(Color.white, 8, true));

        jtDiaChi.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtDiaChi.setPreferredSize(new java.awt.Dimension(150, 30));
        jtDiaChi.setBorder(new LineBorder(Color.white, 8, true));

        jtNgayHT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtNgayHT.setPreferredSize(new java.awt.Dimension(150, 30));
        jtNgayHT.setBorder(new LineBorder(Color.white, 8, true));

        jtHanHD.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtHanHD.setPreferredSize(new java.awt.Dimension(150, 30));
        jtHanHD.setBorder(new LineBorder(Color.white, 8, true));
        jtHanHD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtHanHDActionPerformed(evt);
            }
        });

        jtSDT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtSDT.setPreferredSize(new java.awt.Dimension(150, 30));
        jtSDT.setBorder(new LineBorder(Color.white, 8, true));

        jBAdd.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBAdd.setText("Thêm");
        jBAdd.setPreferredSize(new java.awt.Dimension(100, 40));
        jBAdd.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jBAdd.setBackground(new Color(156, 221, 229));
        jBAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAddActionPerformed(evt);
            }
        });

        jBUpdate.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBUpdate.setText("Cập Nhật");
        jBUpdate.setPreferredSize(new java.awt.Dimension(100, 40));
        jBUpdate.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jBUpdate.setBackground(new Color(156, 221, 229));
        jBUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBUpdateActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel8.setText("Email");
        jLabel8.setPreferredSize(new java.awt.Dimension(130, 30));

        jtEmail.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtEmail.setPreferredSize(new java.awt.Dimension(150, 30));
        jtEmail.setBorder(new LineBorder(Color.white, 8, true));
        jtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtEmailActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("Trạng Thái");
        jLabel9.setPreferredSize(new java.awt.Dimension(130, 30));

        jcbTT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jcbTT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Có", "Không" }));
        jcbTT.setPreferredSize(new java.awt.Dimension(100, 30));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jBAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(70, 70, 70)
                        .addComponent(jBUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jcbTT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(10, 10, 10)
                                    .addComponent(jtTenDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtHanHD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtNgayHT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(10, 10, 10)
                                    .addComponent(jtMaDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtSDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtMaDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtTenDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtNgayHT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtHanHD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtSDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbTT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(207, 250, 253));

        jLabel1.setBackground(new java.awt.Color(207, 250, 253));
        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("ĐỐI TÁC");

        jButton12.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton12.setText("Xuất Exel");
        jButton12.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton12.setBackground(new Color(156, 221, 229));
        jButton12.setPreferredSize(new java.awt.Dimension(100, 50));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton13.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton13.setText("Chi tiết SĐT");
        jButton13.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton13.setBackground(new Color(156, 221, 229));
        jButton13.setActionCommand("Chi Ti?t SÐT");
        jButton13.setPreferredSize(new java.awt.Dimension(130, 50));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jComboBox1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Đối Tác", "Hàng Hoá Cung Cấp" }));
        jComboBox1.setPreferredSize(new java.awt.Dimension(200, 50));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 769, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        
    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sThuChi().setVisible(true);
        this.dispose();
    }                                        

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        try {
            TimKiem();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }                                         

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sBanHang().setVisible(true);
        this.dispose();
    }                                        

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sHangHoa().setVisible(true);
        this.dispose();
    }                                        

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sYCLuanChuyen().setVisible(true);
        this.dispose();
    }                                        

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        new sKhachHang().setVisible(true);
        this.dispose();
    }                                        

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sKho().setVisible(true);
        this.dispose();
    }                                        

    private void jtEmailActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
    }                                       

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
        if (jComboBox1.getSelectedIndex() == 1){
            new sHHCungCap().setVisible(true);
            this.dispose();
        }
    }                                          

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sChiNhanh().setVisible(true);
        this.dispose();
    }                                        

    private boolean first = false;
    private void jtTimKiemMouseClicked(java.awt.event.MouseEvent evt) {                                       
        // TODO add your handling code here:
        if (!first){
            first = true;
            jtTimKiem.setText("");
            jtTimKiem.setForeground(Color.black);
        }
    }                                      

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
        new sMain().setVisible(true);
        this.dispose();
    }                                         

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sHoaDon().setVisible(true);
        this.dispose();
    }                                        

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sNhanVien().setVisible(true);
        this.dispose();
    }                                        

    private void jBAddActionPerformed(java.awt.event.ActionEvent evt) {                                      
        try{
            int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn thêm thông tin này không?");
            if (rs == 0){
                if(jtMaDT.getText().trim().equals("")||jtTenDT.getText().trim().equals("")||
                        jtDiaChi.getText().trim().equals("")||jtEmail.getText().trim().equals("")||
                        jtHanHD.getText().trim().equals("")||jtNgayHT.getText().trim().equals("")||jtSDT.getText().trim().equals("")){
                    JOptionPane.showMessageDialog(this,"Co cho bo trong");
                }
                else 
                    if(HopLeSDT(jtSDT.getText())==false)
                        {
                         JOptionPane.showMessageDialog(this,"SDT Khong hop le");   
                        }
                    else{
                        DoiTac tdt = new DoiTac();
                        tdt.setMaDT(jtMaDT.getText());
                        tdt.setTenDT(jtTenDT.getText());
                        tdt.setDiachiDT(jtDiaChi.getText());
                        tdt.setEmail(jtEmail.getText());
                        String[] s =jtNgayHT.getText().split("/");
                        tdt.setNghoptac(new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0])));
                        String[] s2 =jtHanHD.getText().split("/");
                        tdt.setHanhd(new Date(Integer.parseInt(s2[2]) - 1900, Integer.parseInt(s2[1]) - 1, Integer.parseInt(s2[0])));
                        ArrayList<String> arrsdt= new ArrayList<String>();
                        arrsdt.add(jtSDT.getText());
                        tdt.setSdt(arrsdt);
                        TrangThai tt = new TrangThai();
                        tt.setTentrangthai(jcbTT.getItemAt(jcbTT.getSelectedIndex()));
                        tdt.setTrangthai(tt);

                        try { 
                            JOptionPane.showMessageDialog(this,DoiTacBLL.ThemDoiTac(tdt));
                        } catch (SQLException ex) {
                            System.out.println(ex);
                        }finally{
                            TaiDuLieu();
                        }

                }
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "thong tin khong hop le"); 
    }                                     
 }
    private void jBUpdateActionPerformed(java.awt.event.ActionEvent evt) {                                         
        try{
            int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn sửa thông tin này không?");
            if (rs == 0){
                if(jtMaDT.getText().trim().equals("")||jtTenDT.getText().trim().equals("")||
                        jtDiaChi.getText().trim().equals("")||jtEmail.getText().trim().equals("")||
                        jtHanHD.getText().trim().equals("")||jtNgayHT.getText().trim().equals("")||jtSDT.getText().trim().equals("")){
                    JOptionPane.showMessageDialog(this,"Co cho bo trong");
                }
                else if(HopLeSDT(jtSDT.getText())==false){
                    JOptionPane.showMessageDialog(this,"SDT Khong hop le");
                }
                    else
                    {
                        DoiTac tdt = new DoiTac();
                        tdt.setMaDT(jtMaDT.getText());
                        tdt.setTenDT(jtTenDT.getText());
                        tdt.setDiachiDT(jtDiaChi.getText());
                        tdt.setEmail(jtEmail.getText());
                        String[] s =jtNgayHT.getText().split("/");
                        tdt.setNghoptac(new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0])));
                        String[] s2 =jtHanHD.getText().split("/");
                        tdt.setHanhd(new Date(Integer.parseInt(s2[2]) - 1900, Integer.parseInt(s2[1]) - 1, Integer.parseInt(s2[0])));
                        ArrayList<String> arrsdt= new ArrayList<String>();
                        arrsdt.add(jtSDT.getText());
                        tdt.setSdt(arrsdt);
                        TrangThai tt = new TrangThai();
                        tt.setTentrangthai(jcbTT.getItemAt(jcbTT.getSelectedIndex()));
                        tdt.setTrangthai(tt);

                        try { 
                            JOptionPane.showMessageDialog(this,DoiTacBLL.SuaDoiTacBLL(tdt));
                        } catch (SQLException ex) {
                            System.out.println(ex);
                        }finally{
                            TaiDuLieu();
                        }

                    }
                 }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "thong tin khong hop le"); 
                }
            
                   
    }                                        

    private void DsDoiTacMouseClicked(java.awt.event.MouseEvent evt) {                                      
        // TODO add your handling code here:
        jtMaDT.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 0).toString());
        jtTenDT.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 1).toString());
        jtDiaChi.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 2).toString());
        jtNgayHT.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 3).toString());
        jtHanHD.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 4).toString());
        jtSDT.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 5).toString());
        jtEmail.setText(DsDoiTac.getValueAt(DsDoiTac.getSelectedRow(), 6).toString());
        
    }                                     

    private void jtMaDTActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
    }                                      

    private void jtHanHDActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
    }                                       

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
    }                                         

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        DoiTacBLL.XuatExcel(DsDoiTac);
    }                                         

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        TaiDuLieu();
    }                                         

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify                     
    private javax.swing.JTable DsDoiTac;
    private javax.swing.JButton jBAdd;
    private javax.swing.JButton jBUpdate;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcbTT;
    private javax.swing.JTextField jtDiaChi;
    private javax.swing.JTextField jtEmail;
    private javax.swing.JTextField jtHanHD;
    private javax.swing.JTextField jtMaDT;
    private javax.swing.JTextField jtNgayHT;
    private javax.swing.JTextField jtSDT;
    private javax.swing.JTextField jtTenDT;
    private javax.swing.JTextField jtTimKiem;
    // End of variables declaration                   

}
