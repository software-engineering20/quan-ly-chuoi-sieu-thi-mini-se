/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.HangHoa;

import MiniSupermarket.BLL.HangHoaBLL;
import MiniSupermarket.Gui.sMain;
import MiniSupermarket.Gui.BanHang.sBanHang;
import MiniSupermarket.Gui.ChiNhanh.sChiNhanh;
import MiniSupermarket.Gui.DoiTac.sDoiTac;
import MiniSupermarket.Gui.HoaDon.sHoaDon;
import MiniSupermarket.Gui.KhachHang.sKhachHang;
import MiniSupermarket.Gui.Kho.sKho;
import MiniSupermarket.Gui.LuanChuyen.sYCLuanChuyen;
import MiniSupermarket.Gui.ThuChi.sThuChi;
import MiniSupermarket.Gui.sNhanVien.sNhanVien;
import XuLi.HangHoa;
import XuLi.TrangThai;
import java.awt.Color;
import java.awt.Font;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author admin
 */
public class sHangHoa extends javax.swing.JFrame {

    public sHangHoa() {
        initComponents();
        TaiDuLieu();
    }
    public void TailaiText(){
        jTTenHH.setText("");
        jTHH.setText("");
        jTNSX.setText("");
    }
    public void TaiDuLieu(){
        try{
            DefaultTableModel tb= new DefaultTableModel();
            tb.addColumn("Mã hàng hóa");
            tb.addColumn("Tên hàng hóa");
            tb.addColumn("Nước sản xuất");
            tb.addColumn("Trạng thái");
            DsHH.setModel(tb);
            DsHH.setFont(new Font("Times New Roman",Font.PLAIN, 15));
            for(int i=0; i<HangHoaBLL.DuLieuHH().size();i++)
            {
                HangHoa HH = HangHoaBLL.DuLieuHH().get(i);
                String ma= HH.getMahanghoa();
                String ten= HH.getTenhanghoa();
                String nsx=HH.getNuocsx();                
                String trangthai= HH.getTrangthai().getTentrangthai();
                Object[] row={ma,ten,nsx,trangthai};
                tb.addRow(row);
            }
        }catch (Exception e) {
            System.out.println(e);}
}
    private void TimKiem() throws SQLException{
        setTable(jtTimKiem.getText());
    }
    private void setTable(String text) throws SQLException {
    DsHH.setModel(HangHoaBLL.TaiBangTimKiemHH(text));
    }    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jtTimKiem = new JTextField();
        jtTimKiem.setBorder(new LineBorder(Color.white, 10, true));
        jbTimKiem = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        DsHH = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTHH = new javax.swing.JTextField();
        jTTenHH = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jBThem = new javax.swing.JButton();
        jbSua = new javax.swing.JButton();
        jTNSX = new javax.swing.JTextField();
        jButton12 = new javax.swing.JButton();
        jcbTT = new javax.swing.JComboBox<>();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(100, 0));
        setResizable(false);
        setSize(new java.awt.Dimension(1080, 768));

        jPanel1.setBackground(new java.awt.Color(141, 238, 221));
        jPanel1.setPreferredSize(new java.awt.Dimension(1080, 768));

        jPanel2.setBackground(new java.awt.Color(146, 215, 220));
        jPanel2.setPreferredSize(new java.awt.Dimension(1080, 98));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnBanHang.png"))); // NOI18N
        jButton1.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnHangHoa (1).png"))); // NOI18N
        jButton2.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnHoaDon.png"))); // NOI18N
        jButton3.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnKho.png"))); // NOI18N
        jButton4.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnKhachHang.png"))); // NOI18N
        jButton5.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnNhanVien.png"))); // NOI18N
        jButton6.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnLuanChuyen.png"))); // NOI18N
        jButton7.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnThuChi.png"))); // NOI18N
        jButton8.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnChiNhanh.png"))); // NOI18N
        jButton9.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.packages/btnDoiTac.png"))); // NOI18N
        jButton10.setPreferredSize(new java.awt.Dimension(98, 98));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel3.setBackground(new java.awt.Color(207, 250, 253));
        jPanel3.setPreferredSize(new java.awt.Dimension(1045, 65));

        jtTimKiem.setForeground(new java.awt.Color(204, 204, 204));
        jtTimKiem.setText("Mã, Tên, NSX, Giá, Trạng Thái, ...");
        jtTimKiem.setPreferredSize(new java.awt.Dimension(470, 36));
        jtTimKiem.setRequestFocusEnabled(true);
        jtTimKiem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtTimKiemMouseClicked(evt);
            }
        });

        jbTimKiem.setBackground(new java.awt.Color(124, 205, 189));
        jbTimKiem.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jbTimKiem.setText("Tìm Kiếm");
        jbTimKiem.setPreferredSize(new java.awt.Dimension(95, 45));
        jbTimKiem.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jbTimKiem.setBackground(new Color(156, 221, 229));
        jbTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbTimKiemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jtTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addComponent(jbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        DsHH.setFont(new java.awt.Font("Songti TC", 0, 5)); // NOI18N
        DsHH.setPreferredSize(new java.awt.Dimension(644, 550));
        DsHH.getTableHeader().setReorderingAllowed(false);
        DsHH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DsHHMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(DsHH);
        if (DsHH.getColumnModel().getColumnCount() > 0) {
            DsHH.getColumnModel().getColumn(0).setResizable(false);
            DsHH.getColumnModel().getColumn(0).setPreferredWidth(100);
            DsHH.getColumnModel().getColumn(1).setResizable(false);
            DsHH.getColumnModel().getColumn(1).setPreferredWidth(110);
            DsHH.getColumnModel().getColumn(2).setResizable(false);
            DsHH.getColumnModel().getColumn(2).setPreferredWidth(120);
            DsHH.getColumnModel().getColumn(3).setResizable(false);
            DsHH.getColumnModel().getColumn(3).setPreferredWidth(64);
            DsHH.getColumnModel().getColumn(4).setResizable(false);
            DsHH.getColumnModel().getColumn(4).setPreferredWidth(100);
        }

        jPanel5.setBackground(new java.awt.Color(207, 250, 253));
        jPanel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(207, 250, 253), 1, true));
        jPanel5.setPreferredSize(new java.awt.Dimension(383, 458));
        //jPanel5.setBorder(new LineBorder(new Color(207, 250, 253), 15, true));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setText("Mã Hàng Hoá");
        jLabel3.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setText("Tên Hàng");
        jLabel4.setMaximumSize(new java.awt.Dimension(107, 26));
        jLabel4.setPreferredSize(new java.awt.Dimension(120, 30));

        jTHH.setBorder(new LineBorder(Color.white, 10, true));
        jTHH.setPreferredSize(new java.awt.Dimension(236, 38));
        jTHH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTHHActionPerformed(evt);
            }
        });

        jTTenHH.setBorder(new LineBorder(Color.white, 10, true));
        jTTenHH.setPreferredSize(new java.awt.Dimension(236, 38));
        jTTenHH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTTenHHActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Nước Sản Xuất");
        jLabel7.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("Trạng Thái");
        jLabel9.setPreferredSize(new java.awt.Dimension(120, 30));

        jBThem.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBThem.setText("Thêm");
        jBThem.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jBThem.setBackground(new Color(156, 221, 229));
        jBThem.setPreferredSize(new java.awt.Dimension(130, 50));
        jBThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBThemActionPerformed(evt);
            }
        });

        jbSua.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jbSua.setText("Cập Nhật");
        jbSua.setPreferredSize(new java.awt.Dimension(130, 50));
        jbSua.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jbSua.setBackground(new Color(156, 221, 229));
        jbSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSuaActionPerformed(evt);
            }
        });

        jTNSX.setBorder(new LineBorder(Color.white, 10, true));
        jTNSX.setPreferredSize(new java.awt.Dimension(236, 38));

        jButton12.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton12.setText("Yêu Cầu Hàng Hoá");
        jButton12.setPreferredSize(new java.awt.Dimension(165, 50));
        jButton12.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton12.setBackground(new Color(156, 221, 229));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jcbTT.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jcbTT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Đang bán ", "Ngừng bán" }));
        jcbTT.setPreferredSize(new java.awt.Dimension(120, 40));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(101, 101, 101)
                        .addComponent(jbSua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65)
                        .addComponent(jcbTT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(110, 110, 110))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTTenHH, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTHH, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTNSX, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jTHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jTTenHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jTNSX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbTT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbSua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jComboBox1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Thông Tin Hàng Hoá", "Thông Tin Hàng Hoá Theo Lô", "Lịch Sử Yêu Cầu" }));
        jComboBox1.setPreferredSize(new java.awt.Dimension(250, 50));
        jComboBox1.setVerifyInputWhenFocusTarget(false);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sThuChi().setVisible(true);
        this.dispose();
    }                                        

    private void jbTimKiemActionPerformed(java.awt.event.ActionEvent evt) {                                          
        try {
            TimKiem();
        } catch (SQLException ex) {
            System.out.println("Tim kiem"+ ex);
        }
    }                                         

    private void jTHHActionPerformed(java.awt.event.ActionEvent evt) {                                     
        // TODO add your handling code here:
    }                                    

    private void jTTenHHActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
    }                                       

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sBanHang().setVisible(true);
        this.dispose();
    }                                        

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
        new sYCHangHoa().setVisible(true);

    }                                         

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
        if (jComboBox1.getSelectedIndex() == 2){
            new sLSYeuCau().setVisible(true);
            this.setVisible(false);
        }if (jComboBox1.getSelectedIndex() == 1){
            new sHHTheoLo().setVisible(true);
            this.setVisible(false);
        }
    }                                          

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sKho().setVisible(true);
        this.dispose();
    }                                        

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sYCLuanChuyen().setVisible(true);
        this.dispose();
    }                                        

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sKhachHang().setVisible(true);
        this.dispose();
    }                                        

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {                                          
        new sDoiTac().setVisible(true);
        this.dispose();
    }                                         

    private boolean first = false;
    private void jtTimKiemMouseClicked(java.awt.event.MouseEvent evt) {                                       
        // TODO add your handling code here:
        if (!first){
            first = true;
            jtTimKiem.setText("");
            jtTimKiem.setForeground(Color.black);
        }
    }                                      

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sMain().setVisible(true);
        this.dispose();
    }                                        

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sChiNhanh().setVisible(true);
        this.dispose();
    }                                        

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sHoaDon().setVisible(true);
        this.dispose();
    }                                        

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        new sNhanVien().setVisible(true);
        this.dispose();
    }                                        

    private void jBThemActionPerformed(java.awt.event.ActionEvent evt) {                                       
        try{
            int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn thêm thông tin này không?");
            if (rs == 0){
                if(jTHH.getText().trim().equals("")||jTTenHH.getText().trim().equals("")||
                        jTNSX.getText().trim().equals("")){
                    JOptionPane.showMessageDialog(this,"Co cho bo trong");
                }
                    else{
                        HangHoa HH = new HangHoa();
                        HH.setMahanghoa(jTHH.getText());
                        HH.setTenhanghoa(jTTenHH.getText());
                        HH.setNuocsx(jTNSX.getText());
                        TrangThai tt = new TrangThai();
                        tt.setTentrangthai(jcbTT.getItemAt(jcbTT.getSelectedIndex()));
                        HH.setTrangthai(tt);

                        try { 
                            JOptionPane.showMessageDialog(this,HangHoaBLL.ThemHangHoa(HH));
                        } catch (SQLException  ex) {
                            System.out.println("GUI"+ex);
                        }finally{
                            TaiDuLieu();
                            TailaiText();
                        }

                }
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "thong tin khong hop le");
        }
    }                                      

    private void jbSuaActionPerformed(java.awt.event.ActionEvent evt) {                                      
        try{
            int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn sửa thông tin này không?");
            if (rs == 0){
                if(jTHH.getText().trim().equals("")||jTTenHH.getText().trim().equals("")||
                        jTNSX.getText().trim().equals("")){
                    JOptionPane.showMessageDialog(this,"Co cho bo trong");
                }
                    else{
                        HangHoa HH = new HangHoa();
                        HH.setMahanghoa(jTHH.getText());
                        HH.setTenhanghoa(jTTenHH.getText());
                        HH.setNuocsx(jTNSX.getText());
                        TrangThai tt = new TrangThai();
                        tt.setTentrangthai(jcbTT.getItemAt(jcbTT.getSelectedIndex()));
                        HH.setTrangthai(tt);
                        JOptionPane.showMessageDialog(this,HangHoaBLL.SuaHangHoa(HH));
                        TaiDuLieu();
                        TailaiText();

                }
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "thong tin khong hop le");
        }
    }                                     

    private void DsHHMouseClicked(java.awt.event.MouseEvent evt) {                                  
        jTHH.setText(DsHH.getValueAt(DsHH.getSelectedRow(), 0).toString());
        jTTenHH.setText(DsHH.getValueAt(DsHH.getSelectedRow(), 1).toString());
        jTNSX.setText(DsHH.getValueAt(DsHH.getSelectedRow(), 2).toString());

    }                                 

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify                     
    private javax.swing.JTable DsHH;
    private javax.swing.JButton jBThem;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTHH;
    private javax.swing.JTextField jTNSX;
    private javax.swing.JTextField jTTenHH;
    private javax.swing.JButton jbSua;
    private javax.swing.JButton jbTimKiem;
    private javax.swing.JComboBox<String> jcbTT;
    private javax.swing.JTextField jtTimKiem;
    // End of variables declaration                   
}
