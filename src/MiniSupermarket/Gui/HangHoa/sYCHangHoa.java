/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.HangHoa;

import MiniSupermarket.BLL.DoiTacBLL;
import MiniSupermarket.BLL.HangHoaBLL;
import MiniSupermarket.BLL.YeuCauHHBLL;
import XuLi.CTYCHangHoa;
import XuLi.CTYeuCau;
import XuLi.HangHoa;
import XuLi.QuanLy.DangNhap;
import XuLi.YCHangHoa;
import java.awt.Color;
import java.awt.Font;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author admin
 */
public class sYCHangHoa extends javax.swing.JFrame {
    DefaultTableModel tb;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    static String t;
    public sYCHangHoa() {
        initComponents();
        getContentPane().setBackground(new Color(141, 238, 221));
        TaoBang();
    }
    public void TaoBang(){
        tb=new DefaultTableModel();
        tb.addColumn("Mã hàng hóa");
        tb.addColumn("Tên Hàng Hóa");
        tb.addColumn("Nước sản xuất");
        tb.addColumn("Số lượng");
        DsYC.setModel(tb);
        DsYC.setFont(new Font("Times New Roman",Font.PLAIN, 15));
    }
    public void ThemVaoBang(){
        try{
            if(!jTMaHH.getText().trim().equals("")&&!jTSoL.getText().trim().equals("")){    
                String mahh = jTMaHH.getText();
                String sl= jTSoL.getText();
                Float Solg = Float.parseFloat(sl);
                HangHoa hh= HangHoaBLL.TimHH(mahh);
                int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn thêm "+ hh.getTenhanghoa()+" với số lượng "+sl+ " ?");
                if (rs == 0){
                    if(Float.parseFloat(sl)<=0)
                        JOptionPane.showMessageDialog(this,"Số lượng không hợp lệ");
                    else{
                                        
                            String tenhh= hh.getTenhanghoa();
                            String nsx=hh.getNuocsx();
                            Object[] row={mahh, tenhh, nsx, sl};
                            tb.addRow(row);
                        }                           
                    }
            }
            else{
                JOptionPane.showMessageDialog(this,"Có chỗ bỏ trống");
            }
        }catch(SQLException ex){
            System.out.println("MiniSupermarket.Gui.HangHoa.sYCHangHoa.ThemVaoBang()"+ ex);
            JOptionPane.showMessageDialog(this,"Mã hàng không tồn tại");
        }    
}
    public void ThemYC(){
        try {
            if(!jtSomNhat.getText().trim().equals("")&&!jtTreNhat.getText().trim().equals("")&&DsYC.getModel().getRowCount()>0){
                String[] s =jtSomNhat.getText().split("/");
                String[] s2 =jtTreNhat.getText().split("/");
                long millis=System.currentTimeMillis();   java.sql.Date date =new java.sql.Date(millis);
                String[] s3 =format.format(date).split("/");
                Date somnhat = new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0]));
                Date trenhat = new Date(Integer.parseInt(s2[2]) - 1900, Integer.parseInt(s2[1]) - 1, Integer.parseInt(s2[0]));
                Date hientai= new Date(Integer.parseInt(s3[2]) - 1900, Integer.parseInt(s3[1]) - 1, Integer.parseInt(s3[0]));
                System.out.println(hientai);
                if(somnhat.compareTo(trenhat)>=0||somnhat.compareTo(hientai)<=0)
                    JOptionPane.showMessageDialog(this,"Thời gian trễ nhất và sớm nhất không hop le  ");
                else{   
                    int k= YeuCauHHBLL.LayDuLieu().size()+1;
                    YCHangHoa yc = new YCHangHoa();
                    yc.setMayeucauNB("YC0"+(k));
                    yc.setManguonchuyenNB(DangNhap.MAKHO);
                    yc.setManguonycNB(DangNhap.MACN);
                    yc.setManhanvienYCNB(DangNhap.MA);
                    yc.setSomnhatYCNB(somnhat);
                    yc.setTrenhatYCNB(trenhat);
                    int n= DsYC.getModel().getRowCount();
                    ArrayList<CTYCHangHoa> arrctyc= new ArrayList<CTYCHangHoa>();
                    for(int i=0;i<n;i++){
                        CTYCHangHoa ctyc= new CTYCHangHoa() ;
                        ctyc.setMayeucau(yc.getMayeucauNB());
                        ctyc.setMahh(DsYC.getValueAt(i,0).toString());
                        String sl= DsYC.getValueAt(i,3).toString();
                        ctyc.setSoluong(Float.parseFloat(sl));
                        arrctyc.add(ctyc);
                    }
                    try { 
                    JOptionPane.showMessageDialog(this,YeuCauHHBLL.ThemYeuCau(yc));
                    }catch(SQLException ex) {
                                System.out.println("sYCHH "+ex);
                    }   
                    try{
                    int m= arrctyc.size();
                    for(int i=0;i<m;i++)
                        t= YeuCauHHBLL.ThemCTYeuCau(arrctyc.get(i));
                    JOptionPane.showMessageDialog(this,t);
                    }catch(SQLException e){
                            System.out.println("Them CTYC"+ e);
                    }
                }
            }
            else{
                JOptionPane.showMessageDialog(this,"Có chỗ bỏ trống");
            }
        } catch (SQLException ex) {
                    System.out.println("MiniSupermarket.Gui.HangHoa.sYCHangHoa.ThemVaoBang()"+ex);
        }
}
    private Font f = new Font("Times new roman", 0, 15);

    @Override
    public void setFont(Font f) {
        super.setFont(f); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTMaHH = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTSoL = new javax.swing.JTextField();
        jBThem = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        DsYC = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jtSomNhat = new javax.swing.JTextField();
        jtTreNhat = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jBXoa = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(141, 238, 221));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Yêu Cầu Hàng Hoá");
        jLabel1.setName(""); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(165, 40));

        jPanel1.setBackground(new java.awt.Color(207, 250, 253));
        jPanel1.setPreferredSize(new java.awt.Dimension(397, 105));

        jLabel4.setText("Mã Hàng Hoá");
        jLabel4.setFont(f);

        jTMaHH.setPreferredSize(new java.awt.Dimension(150, 35));
        jTMaHH.setBorder(new LineBorder(Color.white, 10, true));

        jLabel6.setText("Số Lượng");
        jLabel6.setFont(f);

        jTSoL.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jTSoL.setPreferredSize(new java.awt.Dimension(150, 35));
        jTSoL.setBorder(new LineBorder(Color.white, 10, true));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel4)
                .addGap(24, 24, 24)
                .addComponent(jTMaHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(jTSoL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTMaHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jTSoL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jBThem.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBThem.setText("Thêm");
        jBThem.setPreferredSize(new java.awt.Dimension(100, 40));
        jBThem.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jBThem.setBackground(new Color(156, 221, 229));
        jBThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBThemActionPerformed(evt);
            }
        });

        DsYC.setPreferredSize(new java.awt.Dimension(397, 300));
        DsYC.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(DsYC);
        if (DsYC.getColumnModel().getColumnCount() > 0) {
            DsYC.getColumnModel().getColumn(0).setResizable(false);
            DsYC.getColumnModel().getColumn(0).setPreferredWidth(165);
            DsYC.getColumnModel().getColumn(1).setResizable(false);
            DsYC.getColumnModel().getColumn(1).setPreferredWidth(165);
            DsYC.getColumnModel().getColumn(2).setResizable(false);
            DsYC.getColumnModel().getColumn(2).setPreferredWidth(165);
            DsYC.getColumnModel().getColumn(3).setResizable(false);
            DsYC.getColumnModel().getColumn(3).setPreferredWidth(165);
        }

        jLabel2.setText("Sớm Nhất");
        jLabel2.setPreferredSize(new java.awt.Dimension(70, 20));
        jLabel2.setFont(f);

        jLabel3.setText("Trễ Nhất");
        jLabel3.setPreferredSize(new java.awt.Dimension(70, 20));

        jtSomNhat.setPreferredSize(new java.awt.Dimension(100, 40));
        jtSomNhat.setBorder(new LineBorder(Color.white, 15, true));

        jtTreNhat.setPreferredSize(new java.awt.Dimension(100, 40));
        jtTreNhat.setBorder(new LineBorder(Color.white, 15, true));
        jtTreNhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtTreNhatActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton2.setText("Gửi Yêu Cầu");
        jButton2.setPreferredSize(new java.awt.Dimension(120, 75));
        jButton2.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jButton2.setBackground(new Color(156, 221, 229));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jBXoa.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBXoa.setText("Xóa");
        jBXoa.setPreferredSize(new java.awt.Dimension(100, 40));
        jBXoa.setBorder(new MiniSupermarket.Button(new Color(124, 218, 201)));
        jBXoa.setBackground(new Color(156, 221, 229));
        jBXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBXoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(jtSomNhat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(jtTreNhat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 304, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBXoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(140, 140, 140))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBXoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtSomNhat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtTreNhat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41))))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        ThemYC();
    }                                        

    private void jtTreNhatActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
    }                                         

    private void jBThemActionPerformed(java.awt.event.ActionEvent evt) {                                       
        ThemVaoBang();
    }                                      

    private void jBXoaActionPerformed(java.awt.event.ActionEvent evt) {                                      
        tb.removeRow(DsYC.getSelectedRow());
    }                                     

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify                     
    private javax.swing.JTable DsYC;
    private javax.swing.JButton jBThem;
    private javax.swing.JButton jBXoa;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTMaHH;
    private javax.swing.JTextField jTSoL;
    private javax.swing.JTextField jtSomNhat;
    private javax.swing.JTextField jtTreNhat;
    // End of variables declaration                   
}
