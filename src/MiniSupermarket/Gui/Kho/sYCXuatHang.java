/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.Kho;

import MiniSupermarket.BLL.KhoBLL;
import MiniSupermarket.BLL.SanPhamBLL;
import MiniSupermarket.BLL.YeuCauHHBLL;
import MiniSupermarket.Button;
import XuLi.CTPhieuXuat;
import XuLi.PhieuXuat;
import XuLi.QuanLy.DangNhap;
import XuLi.SanPham;
import XuLi.YCHangHoa;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author admin
 */
public class sYCXuatHang extends javax.swing.JFrame {

    /**
     * Creates new form sYCHangHoa
     */
    public sYCXuatHang() {
        initComponents();
        getContentPane().setBackground(new Color(141, 238, 221));
        TaoBang();
        TaiDuLieu();
        try {
            yc = YeuCauHHBLL.TimYCHH(sYCXuatHang.MAXH);
        } catch (SQLException ex) {
            Logger.getLogger(sYCXuatHang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static YCHangHoa yc;
    public void TaoBang(){
        df = new DefaultTableModel();
        df.addColumn("Mã yêu cầu");
        df.addColumn("Sớm nhất");
        df.addColumn("Trễ nhất");
        df.addColumn("Xác nhận kho");
        jTable1.setModel(df);
        
    }
    KhoBLL khoBLL = new KhoBLL();
    public void TaiDuLieu(){
        try {
            int n = khoBLL.LayDuLieuYCHH().size();
            for(int i=0; i<n; i++){
                YCHangHoa yc = khoBLL.LayDuLieuYCHH().get(i);
                String mayc = yc.getMayeucauNB();
                String somnhat  = format.format(yc.getSomnhatYCNB());
                String trenhat = format.format(yc.getTrenhatYCNB());
                String xacnhan = yc.getManvxacnhan();
                Object [] row={mayc, somnhat, trenhat, xacnhan};
                df.addRow(row);
            }
        } catch (Exception ex) {
            System.out.println("MiniSupermarket.Gui.Kho.sYCXuatHang.TaiDuLieu()"+ex);
        }   
    }
    public void TimDuLieu(){
        try {
            ArrayList <YCHangHoa> yc = khoBLL.TimYCHH(jTextField3.getText());
            for(int i=0; i < yc.size(); i++){
                df.addRow(new Object[]{
                    yc.get(i).getMayeucauNB(),
                    format.format(yc.get(i).getSomnhatYCNB()),
                    format.format(yc.get(i).getTrenhatYCNB()),
                    yc.get(i).getManvxacnhan()
                });
            }
        } catch (Exception ex) {
            System.out.println("MiniSupermarket.Gui.Kho.sYCXuatHang.TaiDuLieu()"+ex);
        }   
    }
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    DefaultTableModel df;
    static String MAXH = "";
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jTextField3 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(141, 238, 221));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("YÊU CẦU XUẤT HÀNG");
        jLabel1.setName(""); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(165, 40));

        jPanel1.setBackground(new java.awt.Color(207, 250, 253));
        jPanel1.setPreferredSize(new java.awt.Dimension(397, 70));

        jTextField3.setPreferredSize(new java.awt.Dimension(150, 35));
        jTextField3.setBorder(new LineBorder(Color.white, 10, true));

        jButton3.setBackground(new Color(156, 221, 229));
        jButton3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton3.setText("Tìm Kiếm");
        jButton3.setBorder(new Button(new Color(124, 218, 201)));
        jButton3.setPreferredSize(new java.awt.Dimension(100, 40));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Yêu Cầu", "Sớm Nhất", "Trễ Nhất", "Trạng thái"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setPreferredSize(new java.awt.Dimension(397, 39));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(165);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(165);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(165);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(165);
        }

        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton2.setText("Chi Tiết");
        jButton2.setPreferredSize(new java.awt.Dimension(150, 55));
        jButton2.setBorder(new Button(new Color(124, 218, 201)));
        jButton2.setBackground(new Color(156, 221, 229));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(243, 243, 243))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(30, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jScrollPane1)
                .addGap(18, 18, 18)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    static String mayc = "";
    static  String manv = "";
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        if(MAXH.equals("") && jTable1.getSelectedRow() < 0 )
            JOptionPane.showMessageDialog(this,"Vui Lòng Chọn Mã Để Xem Chi Tiết!");
        else{
            mayc = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString();
            manv = jTable1.getValueAt(jTable1.getSelectedRow(), 3).toString();
            new sCTYCXuatHang().setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        MAXH = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString(); 
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        if (jTextField3.getText().equals("")){
            JOptionPane.showMessageDialog(rootPane, "Vui lòng nhập thông tin cần tìm kiếm!");
        } else {
            TaoBang();
            TimDuLieu();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables
}
