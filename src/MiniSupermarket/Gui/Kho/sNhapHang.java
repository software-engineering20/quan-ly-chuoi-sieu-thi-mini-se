/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.Kho;

import MiniSupermarket.BLL.KhoBLL;
import MiniSupermarket.BLL.SanPhamBLL;
import MiniSupermarket.Button;
import XuLi.CTPhieuNhap;
import XuLi.HangCungCap;
import XuLi.LoHang;
import XuLi.PhieuNhap;
import XuLi.QuanLy.DangNhap;
import XuLi.SanPham;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;


public class sNhapHang extends javax.swing.JFrame {

    DefaultTableModel df;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private LoHang lh = new LoHang();
    private PhieuNhap nh = new PhieuNhap();
    private ArrayList <CTPhieuNhap> ctpn = new ArrayList<>();
    private ArrayList <SanPham> arrsp =new ArrayList<>();
    
    public sNhapHang() {
        initComponents();
        getContentPane().setBackground(new Color(141, 238, 221));
        TaoBang();
    }

    private void TaoBang(){
        df = new DefaultTableModel();
        df.addColumn("Mã hàng hóa");
        df.addColumn("Số lượng");
        df.addColumn("NSX");
        df.addColumn("HSD");
        df.addColumn("Giá nhập");
        df.addColumn("Giá dự kiến");
        DsNhap.setModel(df);
        DsNhap.setFont(new Font("Times New Roman",Font.PLAIN, 15));
    }
    
    KhoBLL khoBLL = new KhoBLL();
    
    private void ThemVaoBang(){
        if(!jtTenLo.getText().equals("") &&
                !jtMaDT.getText().equals("") &&!
                jtMahh.getText().equals("") &&
                !jtSoLuong.getText().equals("") &&
                !jtNSX.getText().equals("") &&
                !jtHSD.getText().equals("") &&
                !jtGiaDK.getText().equals("") &&
                !jtGiaNhap.getText().equals("")){
            try{
                int k = khoBLL.LayDuLieu().size()+1;
                int j = khoBLL.LayDuLieuNH2().size()+1;
                String malo;
                if (k < 10){
                    malo = "LO0" + k;
                } else {
                    malo = "LO" + k;
                }
                String manhap;
                if (j < 10){
                    manhap = "NH0" + k;
                } else {
                    manhap = "NH" + k;
                }
                String madt = jtMaDT.getText().toUpperCase();
                String tenlo = jtTenLo.getText();
                String mahh = jtMahh.getText().toUpperCase();
                String sl = jtSoLuong.getText();
                Float slg = Float.parseFloat(sl);
                String nsx=jtNSX.getText();
                String hsd=jtHSD.getText();
                String giadk = jtGiaDK.getText();
                String gianhap = jtGiaNhap.getText();
                String[] s =nsx.split("/");
                String[] s2 =hsd.split("/");
                Date  date1 = new Date(Integer.parseInt(s[2]) - 1900, 
                        Integer.parseInt(s[1]) - 1, 
                        Integer.parseInt(s[0]));
                Date  date2 = new Date(Integer.parseInt(s2[2]) - 1900, 
                        Integer.parseInt(s2[1]) - 1, 
                        Integer.parseInt(s2[0]));
                lh.setTenlohang(tenlo);
                lh.setMakho(DangNhap.MAKHO);
                lh.setMadoitac(madt);
                lh.setMalohang(malo);
                nh.setMalo(malo);
                nh.setMaphieu(manhap);
                nh.setManv(DangNhap.MA);
                HangCungCap hh = new HangCungCap();
                hh.setMadoitac(madt);
                hh.setMahh(mahh);
                if(!khoBLL.KiemTraCungCap(hh))
                    JOptionPane.showMessageDialog(rootPane,"Hàng hóa hoặc đối tác không hợp lệ");
                else{
                    if(Float.parseFloat(sl)<=0)
                       JOptionPane.showMessageDialog(rootPane,"Hàng hóa hoặc đối tác không hợp lệ"); 
                    else{
                        if(date1.compareTo(date2) >= 0)
                            JOptionPane.showMessageDialog(rootPane,"Thời gian NSX và HSD không hợp lệ"); 
                        else{
                            Object[] row = {
                                mahh,
                                slg,
                                nsx,
                                hsd,
                                gianhap,
                                giadk
                            };
                            df.addRow(row);
                            jtGiaDK.setText("");
                            jtGiaNhap.setText("");
                            jtMahh.setText("");
                            jtSoLuong.setText("");
                            jtHSD.setText("");
                            jtNSX.setText("");
                        }
                    }
                }
            } catch (Exception ex){
                System.out.println("MiniSupermarket.Gui.Kho.sNhapHang.ThemVaoBang()"+ex);
            }
        }
        else
          JOptionPane.showMessageDialog(rootPane,"Có ô trống");  
    }
    private void ThemYC() throws ParseException{
        if (DsNhap.getModel().getRowCount() > 0){
            long millis=System.currentTimeMillis();   
            java.sql.Date date = new java.sql.Date(millis);
            Date hientai = date;
            nh.setNgayphieu(hientai);
            int n = DsNhap.getRowCount();
            for(int i = 0; i<n; i++){
                SanPham sp = new SanPham();
                CTPhieuNhap pn = new CTPhieuNhap();
                sp.setMalohang(lh.getMalohang());
                sp.setMahh(DsNhap.getValueAt(i, 0).toString());
                sp.setSlnhap(Float.parseFloat((DsNhap.getValueAt(i, 1).toString())));
                sp.setSlkho(Float.parseFloat((DsNhap.getValueAt(i, 1).toString())));
                String[] s = DsNhap.getValueAt(i, 2).toString().split("/"); 
                sp.setNsx(new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0])));
                s = DsNhap.getValueAt(i, 3).toString().split("/");
                sp.setHsd(new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0])));
                sp.setGianhap(Integer.parseInt((DsNhap.getValueAt(i, 4).toString())));
                sp.setGiaban(Integer.parseInt((DsNhap.getValueAt(i, 5).toString())));
                sp.setNgaynhap(hientai);
                pn.setMahh(DsNhap.getValueAt(i, 0).toString());
                pn.setMaphieu(nh.getMaphieu());
                pn.setSoluong(Float.parseFloat((DsNhap.getValueAt(i, 1).toString())));
                pn.setGianhap(Integer.parseInt((DsNhap.getValueAt(i, 4).toString())));
                pn.setGiadukien(Integer.parseInt((DsNhap.getValueAt(i, 5).toString())));
                arrsp.add(sp);
                ctpn.add(pn);
            }
            try {
                JOptionPane.showMessageDialog(rootPane,khoBLL.ThemLH(lh));
                JOptionPane.showMessageDialog(rootPane,khoBLL.ThemPhieuNhap(nh));           
                int m = arrsp.size();
                for(int i=0;i<m;i++){
                    JOptionPane.showMessageDialog(rootPane,SanPhamBLL.ThemSanPham(arrsp.get(i)));
                    JOptionPane.showMessageDialog(this,khoBLL.ThemCTPhieuNhap(ctpn.get(i)));
                }
            }catch(HeadlessException e){
                System.out.println("MiniSupermarket.Gui.Kho.sNhapHang.ThemYC()"+e);
            }
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jBXoa = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        DsNhap = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jtTenLo = new javax.swing.JTextField();
        jtMahh = new javax.swing.JTextField();
        jtMaDT = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jtSoLuong = new javax.swing.JTextField();
        jtNSX = new javax.swing.JTextField();
        jtHSD = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jtGiaNhap = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jtGiaDK = new javax.swing.JTextField();
        jBThem = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(141, 238, 221));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("NHẬP HÀNG");
        jLabel1.setName(""); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(165, 40));

        jBXoa.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBXoa.setText("Xóa");
        jBXoa.setPreferredSize(new java.awt.Dimension(100, 40));
        jBXoa.setBorder(new Button(new Color(124, 218, 201)));
        jBXoa.setBackground(new Color(156, 221, 229));
        jBXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBXoaActionPerformed(evt);
            }
        });

        DsNhap.setPreferredSize(new java.awt.Dimension(397, 400));
        DsNhap.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(DsNhap);
        if (DsNhap.getColumnModel().getColumnCount() > 0) {
            DsNhap.getColumnModel().getColumn(0).setResizable(false);
            DsNhap.getColumnModel().getColumn(0).setPreferredWidth(165);
            DsNhap.getColumnModel().getColumn(1).setResizable(false);
            DsNhap.getColumnModel().getColumn(1).setPreferredWidth(165);
            DsNhap.getColumnModel().getColumn(2).setResizable(false);
            DsNhap.getColumnModel().getColumn(2).setPreferredWidth(165);
            DsNhap.getColumnModel().getColumn(3).setResizable(false);
            DsNhap.getColumnModel().getColumn(3).setPreferredWidth(165);
        }

        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton2.setText("Xác Nhận");
        jButton2.setPreferredSize(new java.awt.Dimension(120, 55));
        jButton2.setBorder(new Button(new Color(124, 218, 201)));
        jButton2.setBackground(new Color(156, 221, 229));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(207, 250, 253));
        jPanel2.setPreferredSize(new java.awt.Dimension(600, 170));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel4.setText("Tên Lô");
        jLabel4.setPreferredSize(new java.awt.Dimension(90, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel5.setText("Mã Hàng Hoá");
        jLabel5.setPreferredSize(new java.awt.Dimension(90, 20));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel6.setText("Mã Đối Tác");
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 20));

        jtTenLo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtTenLo.setPreferredSize(new java.awt.Dimension(90, 35));
        jtTenLo.setBorder(new LineBorder(Color.white, 8, true));

        jtMahh.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtMahh.setPreferredSize(new java.awt.Dimension(90, 35));
        jtMahh.setBorder(new LineBorder(Color.white, 8, true));

        jtMaDT.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtMaDT.setPreferredSize(new java.awt.Dimension(90, 35));
        jtMaDT.setBorder(new LineBorder(Color.white, 8, true));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel7.setText("Số Lượng");
        jLabel7.setPreferredSize(new java.awt.Dimension(70, 20));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel9.setText("NSX");
        jLabel9.setPreferredSize(new java.awt.Dimension(70, 20));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel10.setText("HSD");
        jLabel10.setPreferredSize(new java.awt.Dimension(70, 20));

        jtSoLuong.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtSoLuong.setPreferredSize(new java.awt.Dimension(90, 35));
        jtSoLuong.setBorder(new LineBorder(Color.white, 8, true));

        jtNSX.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtNSX.setPreferredSize(new java.awt.Dimension(90, 35));
        jtNSX.setBorder(new LineBorder(Color.white, 8, true));

        jtHSD.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtHSD.setPreferredSize(new java.awt.Dimension(90, 35));
        jtHSD.setBorder(new LineBorder(Color.white, 8, true));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel11.setText("Giá Nhập");
        jLabel11.setPreferredSize(new java.awt.Dimension(90, 20));

        jtGiaNhap.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtGiaNhap.setPreferredSize(new java.awt.Dimension(90, 35));
        jtGiaNhap.setBorder(new LineBorder(Color.white, 8, true));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jLabel12.setText("Giá Dự Kiến");
        jLabel12.setPreferredSize(new java.awt.Dimension(90, 20));

        jtGiaDK.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtGiaDK.setPreferredSize(new java.awt.Dimension(90, 35));
        jtGiaDK.setBorder(new LineBorder(Color.white, 8, true));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jtMaDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(jtTenLo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(jtMahh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtHSD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtSoLuong, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtNSX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtGiaNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtGiaDK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(9, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtTenLo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtGiaNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtMahh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtNSX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtGiaDK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtMaDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtHSD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jBThem.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jBThem.setText("Thêm");
        jBThem.setPreferredSize(new java.awt.Dimension(100, 40));
        jBThem.setBorder(new Button(new Color(124, 218, 201)));
        jBThem.setBackground(new Color(156, 221, 229));
        jBThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBThemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBXoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(150, 150, 150))
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(90, 90, 90)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(233, 233, 233))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 599, Short.MAX_VALUE)))
                .addGap(0, 23, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jBXoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jBThem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int rs = JOptionPane.showConfirmDialog(rootPane, "Bạn có muốn thêm yêu cầu này?");
        if (rs == 0){
            try {
                ThemYC();
            } catch (ParseException ex) {
                Logger.getLogger(sNhapHang.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.setVisible(false);
        }    
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jBThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBThemActionPerformed
       int rs = JOptionPane.showConfirmDialog(rootPane, "Bạn có muốn thêm thông tin này ?");
       if (rs == 0){
            ThemVaoBang();
       }
    }//GEN-LAST:event_jBThemActionPerformed

    private void jBXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBXoaActionPerformed
        if (DsNhap.getSelectedRow() >= 0){
            df.removeRow(DsNhap.getSelectedRow());
        }
    }//GEN-LAST:event_jBXoaActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable DsNhap;
    private javax.swing.JButton jBThem;
    private javax.swing.JButton jBXoa;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jtGiaDK;
    private javax.swing.JTextField jtGiaNhap;
    private javax.swing.JTextField jtHSD;
    private javax.swing.JTextField jtMaDT;
    private javax.swing.JTextField jtMahh;
    private javax.swing.JTextField jtNSX;
    private javax.swing.JTextField jtSoLuong;
    private javax.swing.JTextField jtTenLo;
    // End of variables declaration//GEN-END:variables
}
