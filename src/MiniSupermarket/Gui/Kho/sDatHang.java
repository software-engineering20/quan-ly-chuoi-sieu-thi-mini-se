/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package MiniSupermarket.Gui.Kho;

import MiniSupermarket.BLL.DatHangBLL;
import MiniSupermarket.BLL.KhoBLL;
import MiniSupermarket.Button;
import XuLi.CTDatHang;
import XuLi.HangCungCap;
import XuLi.QuanLy.DangNhap;
import XuLi.YCDatHang;
import java.awt.Color;
import java.awt.Font;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import send_email.SwingEmailSender;

/**
 *
 * @author admin
 */
public class sDatHang extends javax.swing.JFrame {

    DefaultTableModel tb;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    static String t;
    public sDatHang() {
        initComponents();
        getContentPane().setBackground(new Color(141, 238, 221));
        TaoBang();
    }
        public void TaoBang(){
        tb=new DefaultTableModel();
        tb.addColumn("Mã đối tác");
        tb.addColumn("Mã hàng hóa");
        tb.addColumn("Số lượng");
        tb.addColumn("Giá đề nghị");
        DsDH.setModel(tb);
        DsDH.setFont(new Font("Times New Roman",Font.PLAIN, 15));
    }
        KhoBLL khoBLL = new KhoBLL();
    public void ThemVaoBang(){
        try{
            if(!jtHH.getText().trim().equals("")&&!jtSL.getText().trim().equals("")
                &&!jtGia.getText().trim().equals("")&&!jtDT.getText().trim().equals("")){
                HangCungCap hh= new HangCungCap();
                String mahh = jtHH.getText();
                String madt= jtDT.getText();
                hh.setMadoitac(madt);
                hh.setMahh(mahh);
                int rs = JOptionPane.showConfirmDialog(null, "Bạn có muốn thêm thông tin này ?");
                if (rs == 0){
                    if(!khoBLL.KiemTraCungCap(hh))
                        JOptionPane.showMessageDialog(this,"Cung cấp này không tồn tại");
                    else{
                                        
                            Float sl= Float.parseFloat(jtSL.getText());
                            String giadenghi=jtGia.getText();
                            Object[] row={madt,mahh,sl,giadenghi};
                            tb.addRow(row);
                        }                           
                    }
            }
            else{
                JOptionPane.showMessageDialog(this,"Có chỗ bỏ trống");
            }
        } catch (Exception ex){
            System.out.println("MiniSupermarket.Gui.HangHoa.sYCHangHoa.ThemVaoBang()"+ ex);
        }    
}
    public void ThemYC(){
        try {
            if(!jtSn.getText().trim().equals("")&&!jtTn.getText().trim().equals("")&&DsDH.getModel().getRowCount()>0){
                String[] s =jtSn.getText().split("/");
                String[] s2 =jtTn.getText().split("/");
                long millis=System.currentTimeMillis();   java.sql.Date date =new java.sql.Date(millis);
                String[] s3 =format.format(date).split("/");
                Date somnhat = new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[0]));
                Date trenhat = new Date(Integer.parseInt(s2[2]) - 1900, Integer.parseInt(s2[1]) - 1, Integer.parseInt(s2[0]));
                Date hientai= new Date(Integer.parseInt(s3[2]) - 1900, Integer.parseInt(s3[1]) - 1, Integer.parseInt(s3[0]));
                System.out.println(hientai);
                if(somnhat.compareTo(trenhat)>=0||somnhat.compareTo(hientai)<=0)
                    JOptionPane.showMessageDialog(this,"Thời gian trễ nhất và sớm nhất không hợp lệ !");
                else{   
                    int k= DatHangBLL.LayDuLieuDH2().size()+1;
                    YCDatHang dh = new YCDatHang();
                    dh.setMayeucauDH("DH0"+k);
                    dh.setManguonycDH(DangNhap.MAKHO);
                    dh.setManhanvienYCDH(DangNhap.MA);
                    dh.setManguonchuyenYCDH(DsDH.getValueAt(0,0).toString());
                    dh.setSomnhatYCDH(somnhat);
                    dh.setTrenhatYCDH(trenhat);
                    int n= DsDH.getModel().getRowCount();
                    ArrayList<CTDatHang> arrctdh= new ArrayList<CTDatHang>();
                    for(int i=0;i<n;i++){
                        CTDatHang ctdh= new CTDatHang();
                        ctdh.setMayeucau(dh.getMayeucauDH());
                        ctdh.setMahh(DsDH.getValueAt(i,1).toString());
                        String sl= DsDH.getValueAt(i,2).toString();
                        ctdh.setSoluong(Float.parseFloat(sl));
                        String gia = DsDH.getValueAt(i, 3).toString();
                        ctdh.setGiadenghi(Integer.parseInt(gia));
                        arrctdh.add(ctdh);
                    }
                    try { 
                    JOptionPane.showMessageDialog(this,DatHangBLL.ThemDH(dh));
                    }catch(SQLException ex) {
                                System.out.println("DatHangBLL.Themdh "+ex);
                    }   
                    try{
                    int m= arrctdh.size();
                    for(int i=0;i<m;i++)
                        t= DatHangBLL.ThemCTDH(arrctdh.get(i));
                    JOptionPane.showMessageDialog(this,t);
                        new SwingEmailSender().setVisible(true);
                    }catch(SQLException e){
                            System.out.println("Them CTYC"+ e);
                    }
                }
            }
            else{
                JOptionPane.showMessageDialog(this,"Có chỗ bỏ trống");
            }
        } catch (SQLException ex) {
                    System.out.println("MiniSupermarket.Gui.Kho.sDatHang.ThemYC()"+ex);
        }
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jtHH = new javax.swing.JTextField();
        jtDT = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jtSL = new javax.swing.JTextField();
        jtGia = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        DsDH = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jtSn = new javax.swing.JTextField();
        jtTn = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(141, 238, 221));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ĐẶT HÀNG");
        jLabel1.setName(""); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(165, 40));

        jPanel1.setBackground(new java.awt.Color(207, 250, 253));
        jPanel1.setPreferredSize(new java.awt.Dimension(397, 105));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setText("Mã Hàng Hoá");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setText("Mã Đối Tác");

        jtHH.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtHH.setPreferredSize(new java.awt.Dimension(150, 35));
        jtHH.setBorder(new LineBorder(Color.white, 10, true));

        jtDT.setPreferredSize(new java.awt.Dimension(150, 35));
        jtDT.setBorder(new LineBorder(Color.white, 10, true));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("Số Lượng");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Giá Đề Nghị");

        jtSL.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jtSL.setPreferredSize(new java.awt.Dimension(150, 35));
        jtSL.setBorder(new LineBorder(Color.white, 10, true));

        jtGia.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jtGia.setPreferredSize(new java.awt.Dimension(150, 35));
        jtGia.setBorder(new LineBorder(Color.white, 10, true));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtSL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jtHH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jtSL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jtDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jtGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton1.setText("Thêm");
        jButton1.setPreferredSize(new java.awt.Dimension(100, 40));
        jButton1.setBorder(new Button(new Color(124, 218, 201)));
        jButton1.setBackground(new Color(156, 221, 229));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        DsDH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        DsDH.setPreferredSize(new java.awt.Dimension(397, 400));
        DsDH.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(DsDH);
        if (DsDH.getColumnModel().getColumnCount() > 0) {
            DsDH.getColumnModel().getColumn(0).setResizable(false);
            DsDH.getColumnModel().getColumn(0).setPreferredWidth(165);
            DsDH.getColumnModel().getColumn(1).setResizable(false);
            DsDH.getColumnModel().getColumn(1).setPreferredWidth(165);
            DsDH.getColumnModel().getColumn(2).setResizable(false);
            DsDH.getColumnModel().getColumn(2).setPreferredWidth(165);
            DsDH.getColumnModel().getColumn(3).setResizable(false);
            DsDH.getColumnModel().getColumn(3).setPreferredWidth(165);
        }

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Sớm Nhất");
        jLabel2.setPreferredSize(new java.awt.Dimension(70, 20));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setText("Trễ Nhất");
        jLabel3.setPreferredSize(new java.awt.Dimension(70, 20));

        jtSn.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtSn.setPreferredSize(new java.awt.Dimension(100, 40));
        jtSn.setBorder(new LineBorder(Color.white, 8, true));

        jtTn.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jtTn.setPreferredSize(new java.awt.Dimension(100, 40));
        jtTn.setBorder(new LineBorder(Color.white, 8, true));

        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton2.setText("Xác Nhận Đặt");
        jButton2.setPreferredSize(new java.awt.Dimension(150, 55));
        jButton2.setBorder(new Button(new Color(124, 218, 201)));
        jButton2.setBackground(new Color(156, 221, 229));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton3.setText("Xóa");
        jButton3.setPreferredSize(new java.awt.Dimension(100, 40));
        jButton3.setBorder(new Button(new Color(124, 218, 201)));
        jButton3.setBackground(new Color(156, 221, 229));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 617, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtSn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(jtTn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(233, 233, 233)))
                .addGap(0, 24, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(150, 150, 150))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtSn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtTn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(30, 30, 30))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        ThemYC();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ThemVaoBang();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        tb.removeRow(DsDH.getSelectedRow());
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable DsDH;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jtDT;
    private javax.swing.JTextField jtGia;
    private javax.swing.JTextField jtHH;
    private javax.swing.JTextField jtSL;
    private javax.swing.JTextField jtSn;
    private javax.swing.JTextField jtTn;
    // End of variables declaration//GEN-END:variables
}
