/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import XuLi.CTPhieuXuat;
import XuLi.PhieuXuat;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class XuatHangBLL {
    public static ArrayList<PhieuXuat> LayDuLieuXuatHang() throws SQLException{
        DALDuLieu.connectDB();
        try {
            ArrayList<PhieuXuat> listPX = DALDuLieu.LayDuLieuXuatHang();
            DALDuLieu.con.close();
            return listPX;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.XuatHangBLL.LayDuLieuXuatHang(): "+e);
            return null;
        }
    }
    public static ArrayList<CTPhieuXuat> LayDuLieuCTXuatHang(String maxh) throws SQLException{
        DALDuLieu.connectDB();
        try {
            ArrayList<CTPhieuXuat> listCTPX = DALDuLieu.LayDuLieuCTXuatHang(maxh);
            DALDuLieu.con.close();
            return listCTPX;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.XuatHangBLL.LayDuLieuCTXuatHang(): "+e);
            return null;
        }
    }
}
