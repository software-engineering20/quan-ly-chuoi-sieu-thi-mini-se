/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import XuLi.GiamGia;
import XuLi.KMSanPham;
import javax.swing.JOptionPane;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class BanHang_KhuyenMaiBLL {

    private XuLi.KMSanPham kmsp = new KMSanPham();
    private static int ma_KM = 0;
    private static String maKM = null;
    public static ArrayList<XuLi.KMSanPham> kmList = new ArrayList<>();

    public static XuLi.SanPham themHangBan(String malo, String mahh) {
        XuLi.SanPham sp;
        sp = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.maSpHopLe(malo, mahh);

        if (sp != null) {
            String maspnhapvao = mahh + malo;
            String maspdulieu = sp.getMahh() + sp.getMalohang();
            if (maspnhapvao.equals(maspdulieu)) {
                return sp;
            }
        } else {
            JOptionPane.showMessageDialog(MiniSupermarket.Gui.sMain.getFrames()[0],
                    "Không tìm thấy mã sản phẩm",
                    "Thông báo!!!",
                    JOptionPane.WARNING_MESSAGE);
            return null;
        }

        return null;
    }

    public static String tenSP(String maHH) {
        XuLi.HangHoa hh;
        String ma;
        try {
            ma = maHH;
        } catch (NumberFormatException exx) {
            return null;
        }
        hh = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.tenSanPham(maHH);
        return hh.getTenhanghoa();
    }

    public static XuLi.GiamGia giamGia(String mahh) {
        XuLi.GiamGia gg = new GiamGia();
        String ma = null;
        try {
            ma = mahh;
        } catch (NumberFormatException exx) {
            return null;
        }
        gg = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.coGiamGia(ma);
        return gg;
    }

    public static XuLi.DanhSachTang Tang(String mahh) {
        XuLi.DanhSachTang dstang;
        String ma = null;
        try {
            ma = mahh;
        } catch (NumberFormatException exx) {
            return null;
        }
        dstang = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.coKhuyenMai(ma);
        return dstang;
    }

    public ArrayList<XuLi.KMSanPham> getKMListt() {
        try {
            System.out.println("");
            if (kmList.size() > 1) {
                maKM = kmList.get(kmList.size() - 1).getMakhuyenmai();
                System.out.println("Ma khuyen mai trong list NÀY LÀ " + maKM);
                String[] IDma = maKM.split("KM");
                String id = IDma[1];
                ma_KM = Integer.parseInt(id);
                System.out.println("ID MA KM trong try " + ma_KM);
            }
        } catch (Exception ex) {
            ma_KM = 0;
        }
       // System.out.println("ID MA KM " + ma_KM);
        return kmList;
    }

    public static void docDsKM() {
        kmList = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.getDanhSachKm();
    }

    public static void themKM(XuLi.KhuyenMai km, XuLi.DSMua dsmua, XuLi.DanhSachTang dstang) {
        //  HoaDonBAL hoadondal=new HoaDonBAL();
        docDsKM();
        BanHang_KhuyenMaiBLL bhkm = new BanHang_KhuyenMaiBLL();
        kmList = bhkm.getKMListt();
        for (XuLi.KMSanPham kmsp : kmList) {
            System.out.println("Mã khuyến mãi là đcmmmm " + kmsp.getMakhuyenmai());
        }
        ma_KM++;
        System.out.println("Ma khuyen mai sau khi cộng ");
        if (ma_KM > 9) {
            km.setMakhuyenmai("KM" + ma_KM);
        } else {
            km.setMakhuyenmai("KM0" + ma_KM);
        }
        System.out.println("Mã khuyến mãi là " + ma_KM);
        dsmua.setMakhuyenmai(km.getMakhuyenmai());
        dstang.setMaKM(km.getMakhuyenmai());
       if(MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.themKhuyenMai(km)){
        MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.themDsMua(dsmua);
        MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.themDsTang(dstang);
        }
    }

    public static boolean xoaKM(String makm) {
        if (MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.xoaDsMua(makm)) 
            if(MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.xoaDsTang(makm)){
                MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.xoaKm(makm);
                return true;
            
            }
     return false;
    }

    public static boolean suaKM(XuLi.KMSanPham km, XuLi.DSMua dsmua, XuLi.DanhSachTang dstang){
    if(MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.suaKM(km))
        if(MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.suaDsMua(dsmua))
            if(MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.suaDsTang(dstang))
                return true;
    return false;
    }
    public static XuLi.KMSanPham timKiem(String maKM) {
        XuLi.KMSanPham km ;
        String ma_kmm = null;
        try {
            ma_kmm = maKM;
        } catch (NumberFormatException exx) {
            return null;
        }
        km = MiniSupermarket.DAL.BanHang_KhuyenMaiDAL.timKiem(maKM);
        return km;

    }
   

}
