/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.ChiNhanhDAL;
import XuLi.ChiNhanh;
import XuLi.ChucVu;
import XuLi.NhVienNgDung;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author mac
 */
public class ChiNhanhBLL {
    public static String KiemTraTextFieldThem(ArrayList<String> NoiDung) {
        String result = "";
        for (String s : NoiDung) 
            if (s.replaceAll(" ", "").equals("")) {
                result = "Tất cả các ô không được bỏ trống";
                break;
            }
        if (result == "") {
            result = KiemTraSDT(NoiDung.get(3));
        }
        return result;
    }
    public static String KiemTraNgaySinh(String NgaySinh) {
        try {
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setLenient(false);
            System.out.println(sdf.parse(NgaySinh));         
        } catch (Exception e) {
            System.out.println(e);
            return "Ngày sinh không hợp lệ!";
        }
        return "";
    }
    public static String KiemTraSDT(String SDT) {
        String SDTPartern = "([+]{1}[8]{1}[4]{1}[1-9]{9}$)|([0]{1}[1-9]{9}$)";
        if (Pattern.matches(SDTPartern, SDT)) return "";
        return "Số điện thoại không đúng định dạng!";
    }
    public static String ThemChiNhanh(ArrayList<String> ThongTinChiNhanh) {
        try {  
            DALDuLieu.connectDB();
            String result = KiemTraTextFieldThem(ThongTinChiNhanh);
            if (result == "") {
                result = ChiNhanhDAL.ThemChiNhanh(ThongTinChiNhanh);
            }DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.ThemChiNhanh(): "+e);
            return "Thêm không thành công!";
        }
    }
    public static String CapNhatChiNhanh (ArrayList<String> ThongTinChiNhanh) {
        try {
            DALDuLieu.connectDB();
            String result = KiemTraTextFieldThem(ThongTinChiNhanh);
            if (KiemTraTextFieldThem(ThongTinChiNhanh) == "") {
                result = ChiNhanhDAL.CapNhatChiNhanh(ThongTinChiNhanh);
            }
            DALDuLieu.con.close();
            return result;  
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.CapNhatChiNhanh(): "+e);
            return "Cập nhật không thành công!";
        }
    }
    public static DefaultTableModel ModelTableChiNhanh(String TimKiem) {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Chi nhánh", "Tên Chi nhánh",  "Địa chỉ", "Số điện thoại"};
            ArrayList<ChiNhanh> data = null;
            data = DALDuLieu.LayThongTinChiNhanh();
            TimKiem = TimKiem.trim().toLowerCase();
            if (!TimKiem.equals("")) 
                for (int i = 0; i < data.size(); i++) 
                    if (!data.get(i).getMaCN().toLowerCase().contains(TimKiem) && !data.get(i).getTenchinhanh().toLowerCase().contains(TimKiem)) {
                        boolean checkSDT = false;
                        for (String s : data.get(i).getSdtCN())
                            if (s.contains(TimKiem)) {
                                checkSDT = true;
                                break;
                            }
                        if (!checkSDT)
                            data.remove(i--) ;
                    }                      
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiNhanh ct : data) {
                dataModel[i][0] = ct.getMaCN();
                dataModel[i][1] = ct.getTenchinhanh();
                dataModel[i][2] = ct.getDiachiCN();
                dataModel[i][3] = ct.getSdtCN().size() == 0 ? "" : ct.getSdt().get(0);
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.ModelTableChiNhanh(): "+e);
            return null;
        }
    }
    public static String ThemSDT(String MANV, ArrayList<String> SDT) {
        try {
            DALDuLieu.connectDB();
            String result = ChiNhanhDAL.ThemNhieuSoDienThoai(MANV, SDT);
            DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.ThemSDT(): "+e);
            return null;
        }
    }

    public static String XoaSDT(String maNV, String SDT) {
        try {
            DALDuLieu.connectDB();
            String result = ChiNhanhDAL.XoaSDT(maNV, SDT);
            DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.XoaSDT(): "+e);
            return null;
        }
    }

    public static String XuatExcel(JTable jTable2) {
        return DALDuLieu.XuatExcel(jTable2);
    }
    public static String NhapExcel() {
        String s = "";
        String result = "Nhập excel thành công!";
        try {
            ArrayList<ArrayList<String>> NoiDung = DALDuLieu.NhapExcel();
            for (ArrayList Dong : NoiDung) {
                for (int i = 0; i < Dong.size() - 4; i++)
                    Dong.add("");
                s = ThemChiNhanh(Dong);
                if (s.toLowerCase().contains("không thành công") || s.toLowerCase().contains("lỗi"))
                    result = "Nhập excel không thành công hoàn toàn!";
            }
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.NhapExcel(): "+e);
            return "Nhập excel không thành công!";
        }
    } 
    public static String DongCuaChiNhanh(String MACN) {
        try {
            DALDuLieu.connectDB();
            boolean rs = ChiNhanhDAL.DongCuaChiNhanh(MACN);
            DALDuLieu.con.close();
            if (rs) 
                return "Đóng cửa thành công!";
            else 
                return "Đóng của không thành công!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ChiNhanhBLL.DongCuaChiNhanh(): "+e);
            return "Lỗi!";
        }
        
    }
}
