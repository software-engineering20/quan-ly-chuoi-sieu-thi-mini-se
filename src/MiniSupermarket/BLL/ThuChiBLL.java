/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import XuLi.ChiTietThuChiCNSP;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import MiniSupermarket.DAL.*;
import XuLi.ChiNhanh;
import XuLi.ChiTietNVKH;
import XuLi.HangHoa;
import XuLi.KhachHang;
import XuLi.NhVienNgDung;
import XuLi.QuanLy.QLKhachHang;
import XuLi.SaveData;
import java.sql.SQLException;
import java.time.LocalDateTime;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author mac
 */
public class ThuChiBLL {
    public static JFreeChart ThongKeTongQuatChart(LocalDateTime NgayBD, LocalDateTime NgayKT) throws SQLException {
        DALDuLieu.connectDB();
        LocalDateTime[] NgBDKT = {NgayBD, NgayKT};
        ArrayList<ChiNhanh> CacChiNhanh = DALDuLieu.LayThongTinChiNhanh();
        LocalDateTime NgBD = NgBDKT[0];
        ArrayList<String> TheLoai = new ArrayList<>();
        ArrayList<Long> TongThu = new ArrayList<>();
        ArrayList<Long> TongChi = new ArrayList<>();
        ArrayList<ChiTietThuChiCNSP> TongChiTiet = ThuChiDAL.ThongKeThuChiCacChiNhanhDaSapXep(NgayBD, NgayKT);
        for (ChiTietThuChiCNSP ct : TongChiTiet) {
            TheLoai.add(ct.getMaCN());
            TongThu.add(ct.getTongThu());
            TongChi.add(ct.getTongChi());
        }
        DALDuLieu.con.close();
        return SoDoThongKe("CÁC CHI NHÁNH", "VND","CHI NHÁNH","THU", "CHI", TongThu, TongChi, TheLoai);     
    }
    public static JFreeChart ThongKeChiNhanhChart(String MACN,LocalDateTime NgayBD, LocalDateTime NgayKT, ChiNhanh ChiNhanh, int kindTime) throws SQLException {
        DALDuLieu.connectDB();
        LocalDateTime[] NgBDKT = {NgayBD, NgayKT};
        LocalDateTime NgBD = NgBDKT[0];
        ArrayList<String> TheLoai = new ArrayList<>();
        ArrayList<Long> TongThu = new ArrayList<>();
        ArrayList<Long> TongChi = new ArrayList<>();
        ArrayList<ChiTietThuChiCNSP> TongChiTiet = ThuChiDAL.ThongKeThuChiChiNhanhTheoThoiGian(NgayBD, NgayKT, ChiNhanh, kindTime);
        int i = 0;
        for (ChiTietThuChiCNSP ct : TongChiTiet) {
            TheLoai.add(NgayBD.plusDays(i++).toString());
            TongThu.add(ct.getTongThu());
            TongChi.add(ct.getTongChi());
        }
        DALDuLieu.con.close();
        return SoDoThongKe(MACN, "VND","CHI NHÁNH","THU", "CHI", TongThu, TongChi, TheLoai);
    }
    public static JFreeChart SoDoThongKe(String title, String CotDoc, String CotNgang,String Cot1, String Cot2, ArrayList<Long> data1, ArrayList<Long> data2, ArrayList<String> TheLoai) {
        final CategoryDataset dataset1;
        if (!Cot2.equals(""))
            dataset1 = ThongKe2Cot(Cot1, Cot2, data1, data2, TheLoai);
        else
            dataset1 = ThongKe1Cot(Cot1,data1, TheLoai);
        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
                title, // chart title
                CotNgang, // domain axis label
                CotDoc, // range axis label
                dataset1, // data
                PlotOrientation.VERTICAL,
                true, // include legend
                true, // tooltips?
                false // URL generator?  Not required...
        );

        chart.setBackgroundPaint(Color.white);

        final CategoryPlot plot = chart.getCategoryPlot();
        
        plot.setBackgroundPaint(new Color(0xFF, 0xFF, 0xFF));
        plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);

        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);

        final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
        renderer2.setToolTipGenerator(new StandardCategoryToolTipGenerator());
        plot.setRenderer(1, renderer2);
        plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
        

        // add the chart to a panel...

        BarRenderer r = (BarRenderer) chart.getCategoryPlot().getRenderer();
        r.setSeriesPaint(0, Color.decode("#89D865"));
        r.setSeriesPaint(1, Color.red);
        plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
        return chart;
    }
    public static DefaultTableModel ModelTableThuChiTongQuat() {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Chi Nhánh", "Từ",  "Đến", "Tổng Thu", "Tổng Chi", "Lợi Nhuận", "Phần Trăm Lợi Nhuận"};
            ArrayList<ChiTietThuChiCNSP> data = null;
            try {
                data = ThuChiDAL.ThongKeThuChiCacChiNhanhDaSapXep(SaveData.Tu, SaveData.Den);
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiTietThuChiCNSP ct : data) {
                dataModel[i][0] = ct.getMaCN();
                dataModel[i][1] = ct.getTu();
                dataModel[i][2] = ct.getDen();
                dataModel[i][3] = ct.getTongThu();
                dataModel[i][4] = ct.getTongChi();
                dataModel[i][5] = ct.getLoiNhuan();
                dataModel[i][6] = ct.getPhanTramLoiNhuan();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ModelTableThuChiTongQuat(): "+e);
            return null;
        }
    }
    public static DefaultTableModel ModelTableThuChiChiNhanh(int kindTime) {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Chi Nhánh", "Từ",  "Đến", "Tổng Thu", "Tổng Chi", "Lợi Nhuận", "Phần Trăm Lợi Nhuận"};
            ArrayList<ChiTietThuChiCNSP> data = null;
            try {
                data = ThuChiDAL.ThongKeThuChiChiNhanhTheoThoiGian(SaveData.Tu, SaveData.Den, SaveData.ChiNhanh, kindTime);
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiTietThuChiCNSP ct : data) {
                dataModel[i][0] = ct.getMaCN();
                dataModel[i][1] = ct.getTu();
                dataModel[i][2] = ct.getDen();
                dataModel[i][3] = ct.getTongThu();
                dataModel[i][4] = ct.getTongChi();
                dataModel[i][5] = ct.getLoiNhuan();
                dataModel[i][6] = ct.getPhanTramLoiNhuan();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ModelTableThuChiChiNhanh(): "+e);
            return null;
        }
    }
    public static DefaultTableModel ModelTableThuChiNhanVien() {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Nhân viên", "Từ",  "Đến","Doanh thu"};
            ArrayList<ChiTietNVKH> data = null;
            try {
                data = ThuChiDAL.ThongKeThuChiNhanVienDaSapXep(SaveData.Tu, SaveData.Den);
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiTietNVKH ct : data) {
                dataModel[i][0] = ct.getMaNV();
                dataModel[i][1] = ct.getTu();
                dataModel[i][2] = ct.getDen();
                dataModel[i][3] = ct.getTongThu();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ModelTableThuChiNhanVien(): "+e);
            return null;
        }
    }
    public static DefaultTableModel ModelTableThuChiKhachHang() {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Khách hàng", "Từ",  "Đến","Doanh thu"};
            ArrayList<ChiTietNVKH> data = null;
            try {
                data = ThuChiDAL.ThongKeThuChiKhachHangDaSapXep(SaveData.Tu, SaveData.Den);
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiTietNVKH ct : data) {
                dataModel[i][0] = ct.getMaNV();
                dataModel[i][1] = ct.getTu();
                dataModel[i][2] = ct.getDen();
                dataModel[i][3] = ct.getTongThu();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ModelTableThuChiKhachHang(): "+e);
            return null;
        }
    }
    public static TableModel ModelTableThuChiSanPham() {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã Sản phẩm", "Từ",  "Đến", "Tổng Thu", "Tổng Chi", "Lợi Nhuận", "Phần Trăm Lợi Nhuận"};
            ArrayList<ChiTietThuChiCNSP> data = null;

            data = ThuChiDAL.ThongKeThuChiSanPhamDaSapXep(SaveData.Tu, SaveData.Den);
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (ChiTietThuChiCNSP ct : data) {
                dataModel[i][0] = ct.getMaCN();
                dataModel[i][1] = ct.getTu();
                dataModel[i][2] = ct.getDen();
                dataModel[i][3] = ct.getTongThu();
                dataModel[i][4] = ct.getTongChi();
                dataModel[i][5] = ct.getLoiNhuan();
                dataModel[i][6] = ct.getPhanTramLoiNhuan();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        } 
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ModelTableThuChiSanPham(): "+e);
            return null;
        }
    }
    public static int KiemTraTuVaDen(String NgBD, String NgKT) {
        if (NgBD.equals("") || NgKT.equals("")) 
            return 1;
        else {
            String[] dateBD = NgBD.split("/");
            String[] dateKT = NgKT.split("/");
            if (dateBD.length != 3 || dateKT.length != 3) 
                return 0;               
            else {
                int ngay;
                int thang;
                int nam;
                try {
                    ngay = Integer.parseInt(dateBD[0]);
                    thang = Integer.parseInt(dateBD[1]);
                    nam = Integer.parseInt(dateBD[2]);
                    LocalDateTime NgBDTime = LocalDateTime.of(nam,thang,ngay,0,0,0); 
                    ngay = Integer.parseInt(dateKT[0]);
                    thang = Integer.parseInt(dateKT[1]);
                    nam = Integer.parseInt(dateKT[2]);
                    LocalDateTime NgKTTime = LocalDateTime.of(nam, thang,ngay,0,0,0); 
                    SaveData.Tu = NgBDTime;
                    SaveData.Den = NgKTTime;
                    return 1;
                }
                catch (Exception e) {
                    return -1;
                }
            }
        }        
    }
     private static CategoryDataset ThongKe1Cot(String Cot1, ArrayList<Long> data1, ArrayList<String> TheLoai) {
       int n = data1.size();
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < n; i++)
            dataset.addValue(data1.get(i), Cot1, TheLoai.get(i));
        return dataset;
    }
    private static CategoryDataset ThongKe2Cot(String Cot1, String Cot2, ArrayList<Long> data1, ArrayList<Long> data2, ArrayList<String> TheLoai) {
        int n = data1.size();
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < n; i++) {
            dataset.addValue(data1.get(i), Cot1, TheLoai.get(i));
            dataset.addValue(data2.get(i), Cot2, TheLoai.get(i));
        }
        return dataset;

    }

    public static JFreeChart ThongKeNhanVienChart(LocalDateTime NgayBD, LocalDateTime NgayKT) {
        ArrayList<String> TheLoai = new ArrayList<>();
        ArrayList<Long> TongThu = new ArrayList<>();
        try {
            DALDuLieu.connectDB();
            LocalDateTime[] NgBDKT = {NgayBD, NgayKT};
            ArrayList<NhVienNgDung> DSNhanVien = DALDuLieu.LayThongTinNhanVien();
            LocalDateTime NgBD = NgBDKT[0];
            
            ArrayList<ChiTietNVKH> TongChiTiet = ThuChiDAL.ThongKeThuChiNhanVienDaSapXep(NgayBD, NgayKT);
            for (ChiTietNVKH ct : TongChiTiet) {
                TheLoai.add(ct.getMaNV());
                TongThu.add(ct.getTongThu());
            }
            DALDuLieu.con.close();
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ThongKeNhanVienChart(): "+e);
        }
        return SoDoThongKe("CÁC NHÂN VIÊN", "VND","MÃ NHÂN VIÊN","THU", "", TongThu, null, TheLoai);  
    }

    public static JFreeChart ThongKeKhachHangChart(LocalDateTime NgayBD, LocalDateTime NgayKT) {
        ArrayList<String> TheLoai = new ArrayList<>();
        ArrayList<Long> TongThu = new ArrayList<>();
        try {
            DALDuLieu.connectDB();
            LocalDateTime[] NgBDKT = {NgayBD, NgayKT};
            ArrayList<KhachHang> DSKhachHang = DALDuLieu.LayDuLieuKhachHang();
            LocalDateTime NgBD = NgBDKT[0];
            
            ArrayList<ChiTietNVKH> TongChiTiet = ThuChiDAL.ThongKeThuChiKhachHangDaSapXep(NgayBD, NgayKT);
            for (ChiTietNVKH ct : TongChiTiet) {
                TheLoai.add(ct.getMaNV());
                TongThu.add(ct.getTongThu());
            }
            DALDuLieu.con.close();
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.ThuChiBLL.ThongKekhachHangChart(): "+e);
        }
        return SoDoThongKe("CÁC KHÁCH HÀNG", "VND","MÃ KHÁCH HÀNG","THU", "", TongThu, null, TheLoai);
    }

    public static JFreeChart ThongKeSanPhamChart(LocalDateTime NgayBD, LocalDateTime NgayKT) throws SQLException{
        DALDuLieu.connectDB();
        LocalDateTime[] NgBDKT = {NgayBD, NgayKT};
        ArrayList<HangHoa> DSHangHoa = DALDuLieu.LayDuLieuHangHoa();
        LocalDateTime NgBD = NgBDKT[0];
        ArrayList<String> TheLoai = new ArrayList<>();
        ArrayList<Long> TongThu = new ArrayList<>();
        ArrayList<Long> TongChi = new ArrayList<>();
        ArrayList<ChiTietThuChiCNSP> TongChiTiet = ThuChiDAL.ThongKeThuChiSanPhamDaSapXep(NgayBD, NgayKT);
        for (ChiTietThuChiCNSP ct : TongChiTiet) {
            TheLoai.add(ct.getMaCN());
            TongThu.add(ct.getTongThu());
            TongChi.add(ct.getTongChi());
        }
        DALDuLieu.con.close();
        return SoDoThongKe("SẢN PHẨM", "VND","MÃ SẢN PHẨM","THU", "CHI", TongThu, TongChi, TheLoai);
    }

    public static String XuatExcel(JTable jTable1) {
        return DALDuLieu.XuatExcel(jTable1);
    }

    

   
}
