/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.DatHangDAL;
import XuLi.CTDatHang;
import XuLi.YCDatHang;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class DatHangBLL {
    public static ArrayList<YCDatHang> LayDuLieuDH2() throws SQLException{
        DALDuLieu.connectDB();
        return DatHangDAL.LayDuLieuDH();
    }
    public static String ThemDH(YCDatHang dh) throws SQLException{
        if(DatHangDAL.ThemDH(dh)){
            return "Thêm yêu cầu đặt hàng thành công!";
        }
        return "Thêm yêu cầu đặt hàng thất bại!";
    }
    public static String ThemCTDH(CTDatHang ctdh) throws SQLException{
        if(DatHangDAL.ThemCTDH(ctdh))
            return "Thêm chi tiết yêu cầu đặt hàng thành công!";
        return"Thêm chi tiết yêu cầu đặt hàng thất bại!";
    }
        
}
