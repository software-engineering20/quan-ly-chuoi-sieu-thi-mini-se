/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.HangHoaDAL;
import MiniSupermarket.DAL.LuanChuyenDAL;
import XuLi.CTYCLuanChuyen;
import XuLi.NhVienNgDung;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author mac
 */
public class LuanChuyenBLL {
    public static String YeuCauLuanChuyen(ArrayList<String> ThongTinLuanChuyen, ArrayList<CTYCLuanChuyen> listCTYC) {
        try {
            DALDuLieu.connectDB();
            String MAYC = "YCLC"+LuanChuyenDAL.LayThongTinYCLC().size()+1;
            if (LuanChuyenDAL.YeuCauLuanChuyen(MAYC, ThongTinLuanChuyen) && LuanChuyenDAL.ThemChiTietYeuCau(MAYC ,listCTYC)) {
                DALDuLieu.con.close();
                return "Yêu cầu thành công!";
            }
                
            else {
                DALDuLieu.con.close();
                return "Yêu cầu thất bại!";
            }
            
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.LuanChuyenBLL.YeuCauLuanChuyen(): "+e);
            return "Lỗi!";
        }
        
    }
    public static DefaultTableModel ModelTableYeuCauLuanChuyen(String TimKiem, DefaultTableModel model, ArrayList<String> DuLieuThem, ArrayList<CTYCLuanChuyen> DanhSachDat) {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã hàng hóa", "Tên hàng hóa",  "Nước sản xuất", "Số lượng"};
            String MAHH = DuLieuThem.get(0);
            String SOLUONG = DuLieuThem.get(1);
            XuLi.HangHoa HH = HangHoaDAL.TimKiemHH(MAHH);
            String HANGHOA = HH.getTenhanghoa();
            String NUOCSX = HH.getNuocsx();
            CTYCLuanChuyen ctyc = new CTYCLuanChuyen();
            ctyc.setMahh(MAHH);
            ctyc.setSoluong(Float.parseFloat(SOLUONG));
            DanhSachDat.add(ctyc);
            model.addRow(new Object[]{MAHH, HANGHOA, NUOCSX, SOLUONG});
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ModelTableNhanVien(): "+e);
            return null;
        }
        
    }
    public static DefaultTableModel ModelTableYeuCauLuanChuyen(String TimKiem) {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã hàng hóa", "Tên hàng hóa",  "Nước sản xuất", "Số lượng"};
            DefaultTableModel model = new DefaultTableModel(TieuDe, 0);
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ModelTableNhanVien(): "+e);
            return null;
        }
        
    }
}
