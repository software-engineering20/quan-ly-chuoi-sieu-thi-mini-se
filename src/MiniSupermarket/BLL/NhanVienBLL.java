/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.NhanVienDAL;
import MiniSupermarket.DAL.ThuChiDAL;
import XuLi.ChiTietThuChiCNSP;
import XuLi.ChucVu;
import XuLi.NhVienNgDung;
import XuLi.SaveData;
import com.orsoncharts.util.json.parser.ParseException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author mac
 */
public class NhanVienBLL {
    public static String KiemTraTextFieldThem(ArrayList<String> NoiDung) {
        String result = "";
        for (String s : NoiDung) 
            if (s.replaceAll(" ", "").equals("")) {
                result = "Tất cả các ô không được bỏ trống";
                break;
            }
        if (result == "") {
            result = KiemTraNgaySinh(NoiDung.get(3));
        }
        if (result == "") {
            result = KiemTraSDT(NoiDung.get(2));
        }
        return result;
    }
    public static String KiemTraNgaySinh(String NgaySinh) {
        try {
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setLenient(false);
            System.out.println(sdf.parse(NgaySinh));         
        } catch (Exception e) {
            System.out.println(e);
            return "Ngày sinh không hợp lệ!";
        }
        return "";
    }
    public static String KiemTraSDT(String SDT) {
        String SDTPartern = "([+]{1}[8]{1}[4]{1}[1-9]{9}$)|([0]{1}[1-9]{9}$)";
        if (Pattern.matches(SDTPartern, SDT)) return "";
        return "Số điện thoại không đúng định dạng!";
    }
    public static String ThemNhanVien(ArrayList<String> ThongTinNhanVien) {
        try {
            DALDuLieu.connectDB();
            String result = KiemTraTextFieldThem(ThongTinNhanVien);
            if (result == "") {
                ArrayList<NhVienNgDung> listNguoiDung = DALDuLieu.LayThongTinNhanVien();
                result = NhanVienDAL.ThemNhanVien(ThongTinNhanVien);
            }
            DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ThemNhanVien(): "+e);
            return "Thêm không thành công!";
        }
    }
    public static String CapNhatNhanVien (ArrayList<String> ThongTinNhanVien) {
        try {
            DALDuLieu.connectDB();
            String result = KiemTraTextFieldThem(ThongTinNhanVien);
            if (KiemTraTextFieldThem(ThongTinNhanVien) == "") {
                result = NhanVienDAL.CapNhatNhanVien(ThongTinNhanVien);
            }
            DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.CapNhatNhanVien(): "+e);
            return "Cập nhật không thành công!";
        }
        
    }
    public static String LuuXetQuyen(NhVienNgDung NhanVien,ArrayList<ChucVu> DSChucVu,ArrayList<Boolean> CheckBanDau, ArrayList<Boolean> CheckSauXetQuyen) {
        String result = "";
        result = XoaQuyen(NhanVien,DSChucVu,CheckBanDau, CheckSauXetQuyen);
        try {
            DALDuLieu.connectDB();
            if (!result.toLowerCase().contains("không thành công") && !result.toLowerCase().contains("lỗi"))
                result = ThemQuyen(NhanVien,DSChucVu,CheckBanDau, CheckSauXetQuyen);
            if (!result.toLowerCase().contains("không thành công") && !result.toLowerCase().contains("lỗi"))
                result = "Xét quyền thành công!";
            DALDuLieu.con.close();
            return result;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.LuuXetQuyen(): "+e);
            return "Xét quyền không thành công!";
        }
        
    }
    private static String XoaQuyen(NhVienNgDung NhanVien, ArrayList<ChucVu> DSChucVu, ArrayList<Boolean> CheckBanDau, ArrayList<Boolean> checkSauXetQuyen) {
        String result = "";
        try {
            DALDuLieu.connectDB();
            int n = CheckBanDau.size();
            for (int i = 0; i < n; i++)
                if (CheckBanDau.get(i) == true && checkSauXetQuyen.get(i) == false) 
                    result = NhanVienDAL.XoaQuyen(NhanVien.getMaNV(),DSChucVu.get(i).getMachucvu());
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.XoaQuyen(): "+e);
            result = "Lỗi xóa quyền";
        }
        return result;
    }
    private static String ThemQuyen(NhVienNgDung NhanVien, ArrayList<ChucVu> DSChucVu, ArrayList<Boolean> CheckBanDau, ArrayList<Boolean> checkSauXetQuyen) {
        String result = "";
        try {
            DALDuLieu.connectDB();
            int n = CheckBanDau.size();
            for (int i = 0; i < n; i++)
                if (CheckBanDau.get(i) == false && checkSauXetQuyen.get(i) == true) 
                    result = NhanVienDAL.ThemQuyen(NhanVien.getMaNV(),DSChucVu.get(i).getMachucvu());
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ThemQuyen(): "+e);
            result = "Lỗi thêm quyền";
        }
        return result;
    }
    public static ArrayList<ChucVu> DSChucVu() {
        return NhanVienDAL.DSChucVu();
    }
    public static DefaultTableModel ModelTableNhanVien(String TimKiem) {
        try {
            String[] TieuDe = new String [] {"Mã nhân viên", "Tên nhân viên",  "Ngày sinh", "Số điện thoại", "Chi nhánh"};
            ArrayList<NhVienNgDung> data = null;
            DALDuLieu.connectDB();
            data = DALDuLieu.LayThongTinNhanVien();
            TimKiem = TimKiem.trim().toLowerCase();
            if (!TimKiem.equals("")) 
                for (int i = 0; i < data.size(); i++) 
                    if (!data.get(i).getMaNV().toLowerCase().contains(TimKiem) && !data.get(i).getTenNV().toLowerCase().contains(TimKiem)) {
                        boolean checkSDT = false;
                        for (String s : data.get(i).getSdtNV())
                            if (s.contains(TimKiem)) {
                                checkSDT = true;
                                break;
                            }
                        if (!checkSDT)
                            data.remove(i--) ;
                    }                      
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            for (NhVienNgDung ct : data) {
                dataModel[i][0] = ct.getMaNV();
                dataModel[i][1] = ct.getTenNV();
                dataModel[i][2] = format.format(ct.getNgsinh());
                dataModel[i][3] = ct.getSdtNV().size() == 0 ? "" : ct.getSdt().get(0);
                dataModel[i][4] = ct.getMachinhanh();
                i++;
            }
            DALDuLieu.con.close();
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ModelTableNhanVien(): "+e);
            return null;
        }
        
    }
    public static String ThemSDT(String MANV, ArrayList<String> SDT) {
        try {
            DALDuLieu.connectDB();
            String rs =  NhanVienDAL.ThemNhieuSoDienThoai(MANV, SDT);
            DALDuLieu.con.close();
            return rs;  
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ThemSDT(): "+e);
            return "Lỗi!";
        }
    }

    public static String XoaSDT(String maNV, String SDT) {
        try {
            DALDuLieu.connectDB();
            String rs =  NhanVienDAL.XoaSDT(maNV, SDT);
            DALDuLieu.con.close();
            return rs;  
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.ThemSDT(): "+e);
            return "Lỗi!";
        }
    }

    public static String XuatExcel(JTable jTable2) {
        return DALDuLieu.XuatExcel(jTable2);
    }
    public static String NhapExcel() {
        String s = "";
        String result = "Nhập excel thành công!";
        try {
            ArrayList<ArrayList<String>> NoiDung = DALDuLieu.NhapExcel();
            for (ArrayList Dong : NoiDung) {
                for (int i = 0; i < Dong.size() - 6; i++)
                    Dong.add("");
                s = ThemNhanVien(Dong);
                if (!s.contains("không thành công"))
                    result = "Nhập excel không thành công hoàn toàn!";
            }
            return result;
        } catch (Exception e) { 
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.NhapExcel(): "+e);
            return "Nhập excel không thành công!";
        }
    } 

    public static String NghiViec(String maNV) {
        try {
            boolean rs = NhanVienDAL.NghiViec(maNV);
            if (rs) 
                return "Cho nhân viên nghỉ việc thành công!";
            else
                return "Cho nhân viên nghỉ việc không thành công!";
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.NghiViec(): "+e);
            return "Lỗi!";
        }
    }
}
