/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.DatHangDAL;
import MiniSupermarket.DAL.KhoDAL;
import XuLi.CTDatHang;
import XuLi.CTPhieuNhap;
import XuLi.CTPhieuXuat;
import XuLi.CTYCHangHoa;
import XuLi.ChiNhanh;
import XuLi.HangCungCap;
import XuLi.HangHoa;
import XuLi.Kho;
import XuLi.LoHang;
import XuLi.PhieuNhap;
import XuLi.PhieuXuat;
import XuLi.QuanLy.DangNhap;
import XuLi.SanPham;
import XuLi.TrangThai;
import XuLi.YCDatHang;
import XuLi.YCHangHoa;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;

/**
 *
 * @author admin
 */
public class KhoBLL {
    KhoDAL khoDAL = new KhoDAL();
    public Vector <Kho> TTKho() throws SQLException{
        DALDuLieu.connectDB();
        Vector <Kho> tmp = khoDAL.LayTTKho();
        return tmp;
    }
    public String ThemTTKho(Kho ttkho) throws SQLException{
        DALDuLieu.connectDB();
        if (khoDAL.KTKho(ttkho.getMaKho())){
            System.out.println(ttkho.getMaKho());
            DALDuLieu.con.close();
            return "Kho Đã Tồn Tại";
        } 
        if (khoDAL.KTChiNhanh(ttkho.getChinhanhql().getMaCN()).equals("KHÔNG")){
            DALDuLieu.con.close();
            return "Không Có Chi Nhánh Này";
        }
        if (khoDAL.ThemTTKho(ttkho)){
            DALDuLieu.con.close();
            return "Thêm Thành Công";
        }
        DALDuLieu.con.close();
        return "Thất Bại";
    }
    public ChiNhanh TTChiNhanh(String macn) throws SQLException{
        DALDuLieu.connectDB();
        ChiNhanh cn = khoDAL.LayTTChiNhanh(macn); 
        DALDuLieu.con.close();
        return cn;
    }
    public TrangThai TTTrangThai(String matt) throws SQLException{
        DALDuLieu.connectDB();
        TrangThai tt = khoDAL.LayTTTT(new TrangThai(matt, matt));
        DALDuLieu.con.close();
        return tt;
    }
    public Vector <Kho> TTKho(String str){
        DALDuLieu.connectDB();
        Vector <Kho> tmp = khoDAL.TimKho(str);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tmp;
    }
    public String CapNhatTTKho(Kho k) throws SQLException{
        DALDuLieu.connectDB();
        if (khoDAL.CapNhatTT(k)){
            DALDuLieu.con.close();
            return "Cập Nhật Thành Công";
        }
        DALDuLieu.con.close();
        return "Cập Nhật Thất Bại";
    }
    public Vector <SanPham> TTKhoHT(String makho){
        DALDuLieu.connectDB();
        Vector <SanPham> sp = khoDAL.LayTTKhoHT(makho);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sp;
    }
    public String ThemSDTKho(String makho, String sdt){
        DALDuLieu.connectDB();
        if (khoDAL.KTSDT(makho, sdt)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Số Điện Thoại Đã Tồn Tại";
        }
        if (khoDAL.ThemSDTKho(makho, sdt)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm Thành Công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm Thất Bại";
    }
    public boolean KTSDT(String makho, String sdt){
        DALDuLieu.connectDB();
        if (khoDAL.KTSDT(makho, sdt)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public String XoaSDT(String makho, String sdt){
        DALDuLieu.connectDB();
        if (khoDAL.XoaSDT(makho, sdt)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Xoá Thành Công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Xoá Thất Bại";
    }
    public Vector <LoHang> TTLoHang(){
        DALDuLieu.connectDB();
        Vector <LoHang> lo = khoDAL.LayTTLoHang();
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lo;
    }
    public boolean KTKho(String makho){
        DALDuLieu.connectDB();
        if (khoDAL.KTKho(makho)){
            return true;
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public Vector <SanPham> TTSPKho(String str){
        DALDuLieu.connectDB();
        Vector <SanPham> sp = khoDAL.TTSPKho(str);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sp;
    }
    public Vector <LoHang> TimLoHang(String str){
        DALDuLieu.connectDB();
        Vector <LoHang> lohang = khoDAL.TimTTLoHang(str);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lohang;
    }
    public String XuatExcel(JTable tb, String tenbang){
        DALDuLieu.connectDB();
        String result = khoDAL.XuatExcel(tb, tenbang);;
        return result;
    }
    public String NhapExcel() {
        try {
            ArrayList<ArrayList<String>> NoiDung = khoDAL.NhapExcel();
            for (ArrayList Dong : NoiDung) {
                DALDuLieu.connectDB();
                Kho k = new Kho();
                k.setMaKho(Dong.get(0).toString());
                k.setDiachiKho(Dong.get(1).toString());
                ArrayList <String> a = new ArrayList<>();
                a.add(Dong.get(2).toString());
                k.setSdtKho(a);
                k.setTrangthai(khoDAL.LayTTTT(new TrangThai(
                        Dong.get(3).toString(), 
                        Dong.get(3).toString()))
                );
                k.setChinhanhql(khoDAL.LayTTChiNhanh(Dong.get(4).toString()));
                ThemTTKho(k);
            }  
            DALDuLieu.con.close();
            return "Nhập excel thành công!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.NhapExcel(): " + e);
            return "Nhập excel không thành công hoàn toàn!";
        }
    } 
    public ArrayList<CTDatHang> LayDuLieuCTDH(String madh) {
        DALDuLieu.connectDB();
        ArrayList <CTDatHang> c = khoDAL.LayDuLieuCTDH(madh);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
    public CTDatHang LayDuLieuCTDH(String madh, String mahh) {
        DALDuLieu.connectDB();
        CTDatHang c = khoDAL.LayDuLieuCTDH(madh, mahh);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
    public ArrayList<YCDatHang> LayDuLieuDH(String makho) {
        DALDuLieu.connectDB();
        ArrayList <YCDatHang> yc = khoDAL.LayDuLieuDH(makho);
        try {
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.KhoBLL.LayDuLieuDH() " + e);
        }
        return yc;
    }
    public ArrayList<YCDatHang> TimDH(String str) {
        DALDuLieu.connectDB();
        ArrayList <YCDatHang> yc = khoDAL.TimDH(str);
        try {
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.KhoBLL.LayDuLieuDH() " + e);
        }
        return yc;
    }
    public ArrayList<PhieuXuat> LayDuLieuXuatHang(){
        DALDuLieu.connectDB();
        ArrayList <PhieuXuat> p = khoDAL.LayDuLieuXuatHang();
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }
    public ArrayList <CTPhieuXuat> LayDuLieuCTXuatHang(String maxh) {
        DALDuLieu.connectDB();
        ArrayList <CTPhieuXuat> p = khoDAL.LayDuLieuCTXuatHang(maxh);
        try {
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.KhoBLL.LayDuLieuCTXuatHang() " + e);
        }
        return p;
    }
    public ArrayList <CTYCHangHoa> CTYCHH(String mayc) {
        DALDuLieu.connectDB();
        ArrayList <CTYCHangHoa> ct = khoDAL.LayDuLieuCTYCHH(mayc);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ct;
    }
    public HangHoa TimHH(String mahh){
        DALDuLieu.connectDB();
        HangHoa hh = khoDAL.TimHH(mahh);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hh;
    }
    public ArrayList <YCHangHoa> LayDuLieuYCHH(){
        DALDuLieu.connectDB();
        ArrayList <YCHangHoa> yc = khoDAL.LayDuLieuYCHH();
        try {
            DALDuLieu.con.clearWarnings();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return yc;
    }
    public ArrayList <YCHangHoa> TimYCHH(String str){
        DALDuLieu.connectDB();
        ArrayList <YCHangHoa> yc = khoDAL.TimYCHH(str);
        try {
            DALDuLieu.con.clearWarnings();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return yc;
    }
    public ArrayList <PhieuXuat> TimTTXuatHang(String string){
        DALDuLieu.connectDB();
        ArrayList <PhieuXuat> px = khoDAL.TimTTXuatHang(string);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return px;
    }
    public ArrayList <LoHang> LayDuLieu () {
        DALDuLieu.connectDB();
        ArrayList <LoHang> lh = khoDAL.LayDuLieuLoHang();
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lh;
    }
    public ArrayList <PhieuNhap> LayDuLieuNH2(){
        DALDuLieu.connectDB();
        ArrayList <PhieuNhap> pn = khoDAL.LayDuLieuNH2();
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pn;
    }
    public String ThemLH(LoHang lh){
        DALDuLieu.connectDB();
        if(khoDAL.ThemLoHang(lh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm thành công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm thất bại";
    }
    public String ThemPhieuNhap (PhieuNhap pn){
        DALDuLieu.connectDB();
        if(khoDAL.ThemPhieuNhap(pn)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm thành công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return"Thêm thất bại";
    }
    public boolean KiemTraCungCap(HangCungCap Cungc){
        DALDuLieu.connectDB();
        if (khoDAL.KiemTraCCHH(Cungc)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public String ThemCTPhieuNhap (CTPhieuNhap ctpn){
        DALDuLieu.connectDB();
        if(khoDAL.ThemCTPhieuNhap(ctpn)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm thành công";
        }
        try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
        return"Thêm thất bại";
    }
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    public String XoaDN(PhieuNhap pn){
        DALDuLieu.connectDB();
        if (khoDAL.XoaDN(pn)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Xoá thành công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Xoá thất bại";
    }
    public Vector <PhieuNhap> TimDN(String str){
        DALDuLieu.connectDB();
        Vector <PhieuNhap> pn = khoDAL.TimDN(str);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pn;
    }
    public SanPham LaySanPham(SanPham sp){
        DALDuLieu.connectDB();
        SanPham spsp = khoDAL.LaySanPham(sp);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return spsp;
    }
     public String ThemXuatHang(PhieuXuat px){
        DALDuLieu.connectDB();
        if(khoDAL.ThemPhieuXuat(px)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm thành công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm thất bại";
    }
    public String ThemCTXuatHang(CTPhieuXuat ctpx){
        DALDuLieu.connectDB();
        if (khoDAL.ThemCTPhieuXuat(ctpx)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm thành công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm thất bại";
    }
    public Vector <CTPhieuNhap> LayCTPhieuNhap(String str){
        DALDuLieu.connectDB();
        Vector <CTPhieuNhap> ct = khoDAL.LayCTPhieuNhap(str);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ct;
    }
    public Vector <CTPhieuNhap> TimCTPhieuNhap(String str, String maphieu){
        DALDuLieu.connectDB();
        Vector <CTPhieuNhap> ct = khoDAL.TimCTPhieuNhap(str, maphieu);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ct;
    }
    public String ThemDH(YCDatHang dh) {
        DALDuLieu.connectDB();
        if (khoDAL.ThemDH(dh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm yêu cầu đặt hàng thành công!";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm yêu cầu đặt hàng thất bại!";
    }
    public String ThemCTDH(CTDatHang ctdh) {
        DALDuLieu.connectDB();
        if (khoDAL.ThemCTDH(ctdh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm chi tiết yêu cầu đặt hàng thành công!";
        }
        try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhoBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
        return"Thêm chi tiết yêu cầu đặt hàng thất bại!";
    }
}
