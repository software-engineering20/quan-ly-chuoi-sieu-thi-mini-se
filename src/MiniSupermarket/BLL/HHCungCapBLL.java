/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DoiTacDAL;
import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.HHCungCapDAL;
import MiniSupermarket.DAL.HangHoaDAL;
import XuLi.HangCungCap;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ADMIN
 */
public class HHCungCapBLL {
    public static ArrayList <HangCungCap> DuLieuCungCap() throws SQLException{
        return DALDuLieu.LayDuLieuCungCap();
    }
    public static String ThemCungCap(HangCungCap Cungc) throws SQLException{
        if(HHCungCapDAL.DaCoCungCap(Cungc)){
            return "Hang hoa cung cap da co";
        }
        if(DoiTacDAL.DaCoDoiTac(Cungc.getMadoitac())&&HangHoaDAL.DaCoHangHoa(Cungc.getMahh())){
            if(HHCungCapDAL.ThemCungCap(Cungc)){
                return "Them thanh cong";
            }        
        }
        else{
             return "Doi tac hoac hang hoa chua ton tai";
        }
        return "Them that bai";
    }
    public static String SuaCungCap(HangCungCap dt)throws SQLException{
        if(HHCungCapDAL.DaCoCungCap(dt)){
            if(HHCungCapDAL.SuaHCungCap(dt)){
                return "Sua thanh cong";
            }
            return "Sua that bai";
        }
        else{
            return " Khong ton tai ";
        }
    }
    public static DefaultTableModel TaiBangTimKimCungC(String TimKiem) throws SQLException {
        String[] TieuDe = new String [] {"Mã đối tác", "Mã hàng hóa", "Trạng Thái"};
        ArrayList<HangCungCap> data = null;
        data = DALDuLieu.LayDuLieuCungCap();
        TimKiem = TimKiem.trim().toLowerCase();
        if (!TimKiem.equals("")) 
            for (int i = 0; i < data.size(); i++) 
                if (!data.get(i).getMadoitac().toLowerCase().contains(TimKiem) && !data.get(i).getMahh().toLowerCase().contains(TimKiem)
                        &&!data.get(i).getTrangThai().getTentrangthai().toLowerCase().contains(TimKiem)){
                            data.remove(i--) ;
                }           
        Object[][] dataModel = new Object[data.size()][TieuDe.length];
        int i = 0;
        for (HangCungCap ct : data) {
            dataModel[i][0] = ct.getMadoitac();
            dataModel[i][1] = ct.getMahh();
            dataModel[i][2] = ct.getTrangThai().getTentrangthai();
            i++;
        }
        DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
        return model;
    }
    public static boolean KiemTraCungCap(HangCungCap Cungc) throws SQLException{
        if(HHCungCapDAL.DaCoCungCap(Cungc))
            return true;
        return false;
    } 
}
