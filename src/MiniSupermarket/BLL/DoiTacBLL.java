/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import XuLi.DoiTac;
import MiniSupermarket.DAL.DoiTacDAL;
import MiniSupermarket.DAL.DALDuLieu;

/**
 *
 * @author ADMIN
 */
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
public class DoiTacBLL {
    public static ArrayList<DoiTac> DuLieuDoiTac() throws SQLException{
        DALDuLieu.connectDB();
        ArrayList<DoiTac> listDT = DALDuLieu.LayDuLieuDoiTac();
        try {
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DoiTacBLL.DuLieuDoiTac(): "+e);
        }
        return listDT;
    }
    public static String ThemDoiTac(DoiTac dt) throws SQLException{
        DALDuLieu.connectDB();
        try {
            if(DoiTacDAL.DaCoDoiTac(dt.getMaDT())){
                DALDuLieu.con.close();
                return "Doi Tac da co";
            }
            if(DoiTacDAL.ThemDoiTac(dt)&&DoiTacDAL.ThemSDTDoiTac(dt)){
                DALDuLieu.con.close();
                return "Them thanh cong";
            }
            DALDuLieu.con.close();
            return "Them that bai";
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DoiTacBLL.ThemDoiTac(): "+e);
            return "Lỗi!";
        }
    }
    public static String SuaDoiTacBLL(DoiTac dt)throws SQLException{
        DALDuLieu.connectDB();
        try {
            if(DoiTacDAL.DaCoDoiTac(dt.getMaDT())){
                if(DoiTacDAL.SuaDoiTac(dt)){
                    return "Sua thanh cong";
                }
                return "Sua that bai";
            }
            else{
                return " Khong ton tai doi tac";
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DoiTacBLL.SuaDoiTacBLL(): "+e);
            return "Lỗi!";
        }
    }
    public static String XuatExcel(JTable tb){
        return DALDuLieu.XuatExcel(tb);
    }
    public static DefaultTableModel TaiBangTimKiemDT(String TimKiem) throws SQLException {
        DALDuLieu.connectDB();
        try {
            String[] TieuDe = new String [] {"Mã đối tác ", "Tên cty đối tác",  "Địa chỉ", "Ngày hợp tác", "Hạn hợp đồng","Số điện thoại","Email","Trạng Thái"};
            ArrayList<DoiTac> data = null;
            data = DALDuLieu.LayDuLieuDoiTac();
            TimKiem = TimKiem.trim().toLowerCase();
            if (!TimKiem.equals("")) 
                for (int i = 0; i < data.size(); i++) 
                    if (!data.get(i).getMaDT().toLowerCase().contains(TimKiem) && !data.get(i).getTenDT().toLowerCase().contains(TimKiem)
                            &&!data.get(i).getTrangthai().getTentrangthai().toLowerCase().contains(TimKiem)
                            &&!data.get(i).getEmail().toLowerCase().contains(TimKiem)
                            &&!data.get(i).getDiachiDT().toLowerCase().contains(TimKiem)){
                            boolean checkSDT = false;
                            for (String s : data.get(i).getSdt())
                                if (s.contains(TimKiem)) {
                                 checkSDT = true;
                                 break;
                            }
                            if (!checkSDT)
                                data.remove(i--) ;
                    }           
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            for (DoiTac ct : data) {
                dataModel[i][0] = ct.getMaDT();
                dataModel[i][1] = ct.getTenDT();
                dataModel[i][2] = ct.getDiachiDT();
                dataModel[i][3] = format.format(ct.getNghoptac());
                dataModel[i][4] = format.format(ct.getHanhd());
                dataModel[i][5] = ct.getSdt().size() == 0 ? "" : ct.getSdt().get(0);
                dataModel[i][6] = ct.getEmail();
                dataModel[i][7] = ct.getTrangthai().getTentrangthai();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DoiTacBLL.TaiBangTimKiemDT(): "+e);
            return null;
        }
        
    }
}