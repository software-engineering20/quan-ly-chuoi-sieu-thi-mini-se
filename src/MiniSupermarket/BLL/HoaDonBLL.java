/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import java.util.ArrayList;
import XuLi.HoaDon;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class HoaDonBLL {

    private XuLi.HoaDon hd = new HoaDon();
    private static int ma_HD = 13;
    private String maHD = null;
    public static ArrayList<XuLi.HoaDon> hdList = new ArrayList<>();

    public ArrayList<XuLi.HoaDon> getHdLít() {
        try {
            if (hdList.size() > 1) {
                maHD = hdList.get(hdList.size() - 1).getMahoadon();
                String[] IDma = maHD.split("HD");
                String id = IDma[1];
                ma_HD = Integer.parseInt(id);
            }
        } catch (Exception ex) {
            ma_HD = 1;
        }
        return hdList;
    }

    public static void docDsHD() {
        hdList = MiniSupermarket.DAL.HoaDonDAL.getDanhSachHD();
    }

    public static void themHD(XuLi.HoaDon hd) {
        HoaDonBLL hoadondal = new HoaDonBLL();
        docDsHD();
        hdList = hoadondal.getHdLít();
        ma_HD++;
        if (ma_HD > 9) {
            hd.setMahoadon("HD" + ma_HD);
        } else {
            hd.setMahoadon("HD0" + ma_HD);
        }
        MiniSupermarket.DAL.HoaDonDAL.themHoaDon(hd);
    }

    public static boolean xoaHD(String mahd) {
        if (MiniSupermarket.DAL.ChiTietHoaDonDAL.xoaCTHD(mahd)) {
            if (MiniSupermarket.DAL.HoaDonDAL.xoaHD(mahd)) {
                return true;
            }
        }
        return false;
    }

    public static XuLi.HoaDon timKiem(String maHD) {
        DALDuLieu.connectDB();
        XuLi.HoaDon hd = new HoaDon();
        String ma_hd = null;
        try {
            ma_hd = maHD;
        } catch (NumberFormatException exx) {
            return null;
        }
        hd = MiniSupermarket.DAL.HoaDonDAL.timKiem(ma_hd);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(HoaDonBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hd;

    }
//    public static String xuatRaExcel(JTable jtable){
//        return MiniSupermarket.DAL.DALDuLieu.XuatExcel(jtable);
//           
//        }

}
