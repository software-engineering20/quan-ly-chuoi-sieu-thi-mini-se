/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.YeuCauHHDAL;
import XuLi.CTYCHangHoa;
import XuLi.YCHangHoa;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ADMIN
 */
public class YeuCauHHBLL {
    public static ArrayList<YCHangHoa> LayDuLieu() throws SQLException{
        DALDuLieu.connectDB();
        try {
            ArrayList<YCHangHoa> YC = DALDuLieu.LayDuLieuYCHH(); 
            DALDuLieu.con.close();
            return YC;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.LayDuLieu(): "+e);
            return null;
        }
    }
    public static ArrayList<CTYCHangHoa> CTYCHH(String mayc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            ArrayList<CTYCHangHoa> CTYC = DALDuLieu.LayDuLieuCTYCHH(mayc);
            DALDuLieu.con.close();
            return CTYC;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.CTYCHH(): "+e);
            return null;
        }
    }
    public static YCHangHoa TimYCHH(String mayc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            YCHangHoa YCHH = YeuCauHHDAL.TimYCHH(mayc);
            DALDuLieu.con.close();
            return YCHH;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.YCHH(): "+e);
            return null;
        }
    }
    public static String ThemYeuCau(YCHangHoa yc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            boolean rs = YeuCauHHDAL.ThemYCHH(yc);
            DALDuLieu.con.close();
            if(rs){
                return "Thêm yêu cầu hàng hóa thành công!";
            }
            return "Thêm yêu cầu hàng hóa thất bại!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.YCHH(): "+e);
            return "Lỗi!";
        }
    }
    public static String ThemCTYeuCau(CTYCHangHoa ctyc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            boolean rs = YeuCauHHDAL.ThemCTYCHH(ctyc);
            DALDuLieu.con.close();
            if(rs){
                return "Thêm chi tiết yêu cầu hàng hóa thành công!";
            }
            return"Thêm chi tiết yêu cầu hàng hóa thất bại!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.ThemCTYeuCau(): "+e);
            return "Lỗi!";
        }
            
    }
    public static String XacNhanYCCN (YCHangHoa yc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            boolean rs = YeuCauHHDAL.XacNhanYCCN(yc);
            DALDuLieu.con.close(); 
            if(rs)
                return "Xác nhận yêu cầu thành công!";
            return"Xác nhận yêu cầu thất bại!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.xacNhanYCCN(): "+e);
            return "Lỗi!";
        }
    }
    public static String XacNhanYCKho (YCHangHoa yc) throws SQLException{
        DALDuLieu.connectDB();
        try {
            boolean rs = YeuCauHHDAL.XacNhanYCKho(yc);
            DALDuLieu.con.close(); 
            if(rs)
                return "Xác nhận yêu cầu thành công!";
            return"Xác nhận yêu cầu thất bại!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.xacNhanYCKho(): "+e);
            return "Lỗi!";
        }
    }
    public static DefaultTableModel TaiBangTimKimYCHH(String TimKiem) throws SQLException {
        DALDuLieu.connectDB();
        try {
            String[] TieuDe = new String [] {"Mã yêu cầu","Mã NV yêu cầu","Sớm nhất","Trễ nhất","Xác nhận của kho","Xác nhận đã nhận"};
            ArrayList<YCHangHoa> data = null;
            data = DALDuLieu.LayDuLieuYCHH();
            TimKiem = TimKiem.trim().toLowerCase();
            if (!TimKiem.equals("")) 
                for (int i = 0; i < data.size(); i++) 
                    if (!data.get(i).getMayeucauNB().toLowerCase().contains(TimKiem) 
                        && !data.get(i).getManhanvienYCNB().toLowerCase().contains(TimKiem)){
                                data.remove(i--) ;
                    }           
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (YCHangHoa yc : data) {
                dataModel[i][0] = yc.getMayeucauNB();
                dataModel[i][1]= yc.getManhanvienYCNB();                
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                dataModel[i][2]  = formatter.format(yc.getSomnhatYCNB());
                dataModel[i][3] = formatter.format(yc.getTrenhatYCNB());
                dataModel[i][4] = yc.getManvxacnhan();
                dataModel[i][5]= yc.getManvhoanthanh();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel,TieuDe);
            DALDuLieu.con.close();
            return model;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.YeuCauHHBLL.TaiBangTimKimYCHH(): "+e);
            return null;
        }
    }

}
