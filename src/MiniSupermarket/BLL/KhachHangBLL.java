/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.KhachHangDAL;
import XuLi.KhachHang;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;

/**
 *
 * @author admin
 */
public class KhachHangBLL {
    KhachHangDAL dskh = new KhachHangDAL();
    public Vector <KhachHang> DSKhachHang() throws SQLException {
        DALDuLieu.connectDB();
        Vector <KhachHang> kh = dskh.LayTTKH();
        DALDuLieu.con.close();
        return kh;
    }
    public String ThemKhachHang(KhachHang kh){
        DALDuLieu.connectDB();
        if (!dskh.HopLeSDT(kh.getSdtKH().get(0))){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Số Điện Thoại Không Hợp Lệ";
        }
        if (dskh.KTMaKH(kh.getMaKH())){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Mã Khách Hàng Đã Tồn Tại";
        }
        if (dskh.ThemKH(kh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm Thành Công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm Thất Bại";
    }
    public String SuaTTKH(KhachHang kh){
        DALDuLieu.connectDB();
        if (dskh.SuaTTKH(kh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Cập Nhật Thông Tin Thành Công";
        }
        try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
        return "Cập Nhật Thất Bại";
    }
    public Vector <KhachHang> TimKH(String kh){
        DALDuLieu.connectDB();
        Vector <KhachHang> k = dskh.TimKH(kh);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return k;
    }
    public KhachHang DSSDTKhachHang(String maKH){
        DALDuLieu.connectDB();
        KhachHang k = dskh.DSSDTKhachHang(maKH);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return k;
    }
    public String ThemSDT(KhachHang kh, int i){
        DALDuLieu.connectDB();
        if (dskh.KTSDT(kh, i)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Số Điện Thoại Đã Tồn Tại";
        }
        if (dskh.ThemSDT(kh, i)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Thêm Thành Công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thêm Thất Bại";
    }
    public String XoaSDT(KhachHang kh){
        DALDuLieu.connectDB();
        if (dskh.XoaSDT(kh)){
            try {
                DALDuLieu.con.close();
            } catch (SQLException ex) {
                Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "Xoá Thành Công";
        }
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Thất Bại";
    }
    public boolean KTSDT(KhachHang kh){
        DALDuLieu.connectDB();
        boolean result = dskh.KTSDT(kh, 0);
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    public String XuatExcel(JTable tb, String tenbang){
        DALDuLieu.connectDB();
        String result = new KhoBLL().XuatExcel(tb, tenbang);
        try {
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.KhachHangBLL.XuatExcel(): " + e);
        }
        return result;
    }
    public String NhapExcel() {
        DALDuLieu.connectDB();
        try {
            ArrayList<ArrayList<String>> NoiDung = DALDuLieu.NhapExcel();
            for (ArrayList Dong : NoiDung) {
                KhachHang k = new KhachHang();
                k.setMaKH(Dong.get(0).toString());
                k.setTenKH(Dong.get(1).toString());
                ArrayList <String> a = new ArrayList<>();
                a.add(Dong.get(2).toString());
                k.setSdtKH(a);
                String[] s = Dong.get(3).toString().split("/");
                k.setNgsinhKH(new Date(Integer.parseInt(s[2]) - 1900, 
                        Integer.parseInt(s[1]) - 1, 
                        Integer.parseInt(s[0]))
                );
                k.setDiachi(Dong.get(4).toString());
                k.setDiem(Integer.parseInt(Dong.get(5).toString()));
                ThemKhachHang(k);
                DALDuLieu.connectDB();
                dskh.ThemSDT(k, 0);
            }  
            DALDuLieu.con.close();
            return "Nhập excel thành công!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.NhanVienBLL.NhapExcel(): " + e);
            return "Nhập excel không thành công hoàn toàn!";
        }
    } 
    public int SDT(String makh){
        DALDuLieu.connectDB();
        int k = dskh.SDT(makh); 
        try {
            DALDuLieu.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(KhachHangBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return k;
    }
}
