/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.HangHoaDAL;
import java.util.ArrayList;
import XuLi.HangHoa;
import XuLi.QuanLy.QLHangHoa;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author ADMIN
 */
public class HangHoaBLL {
    public static  ArrayList<HangHoa> DuLieuHH() throws SQLException{
        try {
            DALDuLieu.connectDB();
            ArrayList<HangHoa> listHH = DALDuLieu.LayDuLieuHangHoa();
            DALDuLieu.con.close();
            return listHH;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.HangHoaBLL.DuLieuHH(): "+e);
            return null;
        }
    }
    public static HangHoa TimHH(String mhh) throws SQLException{
        try {
            DALDuLieu.connectDB();
            HangHoa HH = HangHoaDAL.TimKiemHH(mhh);
            DALDuLieu.con.close();
            return HH;
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.HangHoaBLL.DuLieuHH(): "+e);
            return null;
        }
    }
    public static String ThemHangHoa(HangHoa HH) throws SQLException{         
        try {
            DALDuLieu.connectDB();
            if(HangHoaDAL.DaCoHangHoa(HH.getMahanghoa())){
                DALDuLieu.con.close();
                return "Hang hoa da co";
            }
            if(HangHoaDAL.ThemHangHoa(HH)){
                DALDuLieu.con.close();
                return "Them thanh cong";
            }
            return "Them that bai";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.HangHoaBLL.DuLieuHH(): "+e);
            return "Lỗi!";
        }
        
    }
    public static String SuaHangHoa(HangHoa HH){
        try {
            DALDuLieu.connectDB();
            if(HangHoaDAL.DaCoHangHoa(HH.getMahanghoa())){
                if(HangHoaDAL.SuaHangHoa(HH)) {
                    DALDuLieu.con.close();
                    return "Sua thanh cong ";
                }
                DALDuLieu.con.close();
                return"Sua that bai";
            }
            else{
                DALDuLieu.con.close();
                return " Hang hoa khong ton tai";
            }
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.HangHoaBLL.SuaHangHoa(): "+e);
            return "Lỗi!";
        }
    }
    public static DefaultTableModel TaiBangTimKiemHH(String TimKiem) throws SQLException {
        try {
            DALDuLieu.connectDB();
            String[] TieuDe = new String [] {"Mã hàng hóa","Tên hang hóa", "Nước sản xuất", "Trạng Thái"};
            ArrayList<HangHoa> data = null;
            data = DALDuLieu.LayDuLieuHangHoa();
            TimKiem = TimKiem.trim().toLowerCase();
            if (!TimKiem.equals("")) 
                for (int i = 0; i < data.size(); i++) 
                    if (!data.get(i).getMahanghoa().toLowerCase().contains(TimKiem) && !data.get(i).getTenhanghoa().toLowerCase().contains(TimKiem)
                            && !data.get(i).getNuocsx().toLowerCase().contains(TimKiem)
                            &&!data.get(i).getTrangthai().getTentrangthai().toLowerCase().contains(TimKiem)){
                                data.remove(i--) ;
                    }           
            Object[][] dataModel = new Object[data.size()][TieuDe.length];
            int i = 0;
            for (HangHoa hh : data) {
                dataModel[i][0] = hh.getMahanghoa();
                dataModel[i][1] = hh.getTenhanghoa();
                dataModel[i][2] =hh.getNuocsx();
                dataModel[i][3] = hh.getTrangthai().getTentrangthai();
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
            DALDuLieu.con.close();
            return model;
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.HangHoaBLL.TaiBangTimKiemHH(): "+e);
            return null;
        }
    }
}
