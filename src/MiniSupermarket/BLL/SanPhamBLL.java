/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.SanPhamDAL;
import XuLi.CTPhieuXuat;
import XuLi.SanPham;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ADMIN
 */
public class SanPhamBLL {
    public static ArrayList <SanPham> DuLieuSanPham() throws SQLException{
        return DALDuLieu.LayDuLieuSanpham();
    }
    public static DefaultTableModel TaiBangTimKiemSP(String TimKiem) throws SQLException {
        String[] TieuDe = new String [] {"Mã hàng hóa","Mã lô", "NSX", "HSD", "Giá bán", "SL đang bán","SL đã bán"};
        ArrayList<SanPham> data = null;
        data = DALDuLieu.LayDuLieuSanpham();
        TimKiem = TimKiem.trim().toLowerCase();
        if (!TimKiem.equals("")) 
            for (int i = 0; i < data.size(); i++) 
                if (!data.get(i).getMahh().toLowerCase().contains(TimKiem) && !data.get(i).getMalohang().toLowerCase().contains(TimKiem)){
                            data.remove(i--) ;
                }           
        Object[][] dataModel = new Object[data.size()][TieuDe.length];
        int i = 0;
        for (SanPham sp : data) {
            dataModel[i][0] = sp.getMahh();
            dataModel[i][1] = sp.getMalohang();
            dataModel[i][2] =sp.getNsx();
            dataModel[i][3] = sp.getHsd();
            dataModel[i][4]= sp.getGiaban();
            dataModel[i][5]= sp.getSldangban();
            dataModel[i][6]= sp.getSldaban();
            i++;
        }
        DefaultTableModel model = new DefaultTableModel(dataModel, TieuDe);
        return model;
    }
    public static ArrayList<SanPham> TimSanPham(CTPhieuXuat px) throws SQLException{
        return SanPhamDAL.TimSanPham(px);
    }
    public static String ThemSanPham(SanPham sp){
        if(SanPhamDAL.ThemSanPham(sp))
            return "Thêm thành công";
        return "Thêm thất bại";
    }
    public static SanPham LaySanPham(SanPham sp){
        return SanPhamDAL.LaySanPham(sp);
    }
    public static String SuaSanPham(SanPham sp){
        if(SanPhamDAL.SuaSanPham(sp))
            return "Sửa thành công";
        return "Sửa thất bại";
    }
}
