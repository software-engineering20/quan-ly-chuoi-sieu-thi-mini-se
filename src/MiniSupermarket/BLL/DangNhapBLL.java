/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import MiniSupermarket.DAL.DALDuLieu;
import MiniSupermarket.DAL.DangNhapDAL;

/**
 *
 * @author mac
 */
public class DangNhapBLL {
    public static String DangNhap(String UN, String PW) {
        try {
            DALDuLieu.connectDB();
            boolean rs = DangNhapDAL.DangNhap(UN, PW);
            DALDuLieu.con.close();
            if (rs) 
                return "";
            else {
                return "Đăng nhập không thành công!";
            }
        }
        catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DangNhapBLL.DangNhap(): "+e);
            return "Lỗi!";
        }
    }
    public static String connectDB() {
        boolean check = DALDuLieu.connectDB();
        if (check) return "";
        else return "Kết nối cơ sở dữ liệu không thành công!";
    }

    public static void LayThongTinNguoiDangNhap() {
        try {
            DALDuLieu.connectDB();
            DALDuLieu.LayThongTinNguoiDangNhap(XuLi.QuanLy.DangNhap.MA);
            DALDuLieu.con.close();
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DangNhapBLL.LayTTNDN(): "+e);
        }
    }

    public static String DoiMatKhau(String text, String text0, String text1) {
        try {
            DALDuLieu.connectDB();
            if (!DangNhapDAL.KiemTraMatKhau(text)) {
                DALDuLieu.con.close();
                return "Mật khẩu cũ không chính xác!";
            }
            else 
                if (!text0.equals(text1)) {
                    DALDuLieu.con.close();
                    return "Xác nhận không khớp!";
                }
                else
                    if (!DangNhapDAL.DoiMatKhau(text1)) {
                        DALDuLieu.con.close();
                        return "Lỗi!";
                    }
            return "Đổi mật khẩu thành công!";
        } catch (Exception e) {
            System.out.println("MiniSupermarket.BLL.DangNhapBLL.DoiMatKhau(): "+e);
            return "Lỗi!";
        }
    }
}
