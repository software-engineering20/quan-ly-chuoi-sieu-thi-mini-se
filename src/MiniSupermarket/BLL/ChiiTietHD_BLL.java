/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MiniSupermarket.BLL;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class ChiiTietHD_BLL {

    public static ArrayList<XuLi.CT1DongHD> cthdList = new ArrayList<>();

    public static boolean themCTHD(XuLi.CT1DongHD cthd) {
        if (!MiniSupermarket.DAL.ChiTietHoaDonDAL.themCTHoaDon(cthd)) {
            return true;
        }
        return false;
    }

    public static void docDsHD() {
        cthdList = MiniSupermarket.DAL.ChiTietHoaDonDAL.getDanhSachHD();
    }

    public static ArrayList<XuLi.CT1DongHD> timKiem(String maHD) {
        ArrayList<XuLi.CT1DongHD> cthdList = new ArrayList<>();
        cthdList = MiniSupermarket.DAL.ChiTietHoaDonDAL.timKiem(maHD);//nhahangDAO.timKiemTen(ten.trim().replaceAll("\\s+", " "));

        return cthdList;

    }

}
